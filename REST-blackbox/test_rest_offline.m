%% example code for running simulated online REST
% buffer blocksize = srate/10 without overlapping
% restoredefaultpath
% RESTpath = '/home/yuan/Documents/Tool/REST-git/';
% eeglab % to load any xdf plugins
RESTpath = 'C:\Users\AE\Documents\Bitbucket\mfssvep-eeg-duke\REST-blackbox';
current_path = pwd;
cd(RESTpath);

% Root folder for .xdf
% EEG_DATASET_ROOT = fullfile('..', 'dataset', 'mfssvep-v2', 'sample');    

% EEG_DATASET_ROOT = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal';
% EEG_DATASET_ROOT = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\3. severe';
EEG_DATASET_ROOT = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal - handpick'    
% EEG_DATASET_ROOT = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal - handpick\DK0015'
% EEG_DATASET_ROOT = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\98. convertsample'


% Sample mfSSVEP XDF file for analysis
EEG_DATASET_FILEPATH = fullfile(EEG_DATASET_ROOT, 'DK0006-10_12_1947-mfssvep-right-2.set');
EEG_CALIB_FILEPATH = fullfile(EEG_DATASET_ROOT, 'DK0006-10_12_1947-alpha.set');

% EEG_DATASET_FILEPATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\99. sample\DK0006\20181205\DK0006-10_12_1947-mfssvep-left-1.set';
% EEG_CALIB_FILEPATH = 'E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\99. sample\DK0006\20181205\DK0006-10_12_1947-alpha.set';

%% pipeline
% FIR -> ASR -> ORICA -> eyeCatch (IC classification) -> Reproject non-eye
% ICs
pipeline_desc = { ...
'fir', {'fspec', [0.5000 1 50 55], 'stopripple', -40, ...
    'normalize_amplitude', 0}, ...
'repair_bursts', {'stddev_cutoff', 20, 'window_len', 0.5}, ...
'orica', {'onlineWhitening', {'blockSize', 16}, 'adaptiveFF', {'constant', 'tau_const', 3},... 
    'options', {'blockSize', 16}, 'evalConvergence', 0.01}, ...
'eyecatch', {'cutoff', 0.8} ...
'ica_reproject', {'ProjectionMatrix', '.icawinv', 'ComponentSubset', '.reject'}, ...
};

pipeline_desc = { ...
'fir', {'fspec', [0.5000 1 35 40], 'stopripple', -40, ...
    'normalize_amplitude', 0}, ...
'repair_bursts', {'stddev_cutoff', 20, 'window_len', 0.5}, ...
'orica', {'onlineWhitening', {'blockSize', 16}, 'adaptiveFF', {'constant', 'tau_const', 3},... 
    'options', {'blockSize', 16}, 'evalConvergence', 0.01}, ...
'eyecatch', {'cutoff', 0.8} ...
'ica_reproject', {'ProjectionMatrix', '.icawinv', 'ComponentSubset', '.reject'}, ...
% 'ica_reproject', {'ProjectionMatrix', '.icawinv', 'ComponentSubset', '.reject'}, ...
};

pipe = [{'FilterOrdering', strcat('flt_', pipeline_desc(1:2:end))}, pipeline_desc];


%% load mfSSVEP EEG data
% eeg_path = 'eeg_data_path/';
% eeg_path = pop_loadxdf(EEG_DATASET_FILEPATH);

% calib = 'calibration_data_path/'; % or you can select time range in eeg data for calibration
% calib = pop_loadxdf(EEG_CALIB_FILEPATH);

% buffer = REST_offline(eeg_path, calib, pipe);


%% iterate file
FILE_LIST = get_all_files(EEG_DATASET_ROOT);
for i =1:length(FILE_LIST)
    filepath = FILE_LIST{i};
    disp(['Reading file: ' filepath]);
    
    expression = '.*mfssvep-.*[1-2].set'; % only process file with mfssvep inside name
    if regexp(filepath,expression)  
        [pathstr,name,ext] = fileparts(filepath); 
        C = strsplit(name,'-');
        % name = cell2mat([C(1) '-' C(4)]);
               
%         calib_filename = cell2mat( [C(1) '-' C(2) '-alpha.set'] );
%         calib_filepath = fullfile(pathstr, calib_filename);

        % check whether the merged file exist
        calib_merged_filename = [ C{1} '-' C{2} '-' C{3} '-' C{4} '-all.set' ];
        calib_merged_filepath = fullfile(pathstr, calib_merged_filename) 
        if exist(calib_merged_filepath)
            calib_filepath = calib_merged_filepath;
        else
            calib_filepath = filepath; % if the merged dataset not exist, use the file as input
        end
        
        % COMBINE SESSION
%         subject_id = C{1};
%         dob = C{2};
%         session_combined = [];
%         if strcmp(subject_id, 'DK0015')
%             if contains(name, 'left-1')
%                 set_name_1 = cell2mat([ C(1) '-' C(2) '-mfssvep-left-1.set']);
%                 set_filepath_1 = fullfile(pathstr, set_name_1);
%                 set_name_2 = cell2mat([ C(1) '-' C(2) '-mfssvep-left-2.set']);
%                 set_filepath_2 = fullfile(pathstr, set_name_2);                
%                 EEG_1 = pop_loadset(set_filepath_1);
%                 EEG_2 = pop_loadset(set_filepath_2);
%                 EEG_merged = pop_mergeset( EEG_1, EEG_2);
%                 
%                 set_merged_name = cell2mat([ C(1) '-' C(2) '-mfssvep-left-all.set']);
%                 pop_saveset( EEG_merged, ...
%                             'filename', [ set_merged_name ], ...
%                             'filepath', pathstr );                
%                 disp('merged!');
%                 
%                 % use this merged file as input to calib
%                 calib_filepath = fullfile(pathstr, set_merged_name);
%             end
%         end
        
%         EEG_p11 =  pop_epoch( EEG_merged, {12}, [0.2 5.2], 'epochinfo', 'yes');        
        
        disp(['Analyzing file=' filepath]);
%         disp(['Calib file=' calib_filepath]);
        % dataset, calibset
        
        disp([' Running REST offline...' ]);
        disp([ 'calibpath=' calib_filepath]);
        disp([ 'input=' filepath ]);
        buffer = REST_offline(filepath, calib_filepath, pipe);
        
        output_buffer_filename = [ name '.mat' ];
        output_buffer_filepath = fullfile(pathstr, output_buffer_filename);
        save(output_buffer_filepath, 'buffer');
        
%         % plot the EEG
% %         EEG_ori = pop_loadset(calib_filepath);
%         EEG_ori_step_1 = EEG_ori;
%         EEG_ori_step_2 = EEG_ori;
%         EEG_ori_step_3 = EEG_ori;
%         EEG_ori_step_4 = EEG_ori;
% %         
%         EEG_ori_step_1.data = buffer.data{1};
%         pop_eegplot(EEG_ori_step_1);
% %         title('raw');
%         
%         EEG_ori_step_2.data = buffer.data{2}; % fir
%         pop_eegplot(EEG_ori_step_2);
% %         title('fir 1-50');
%         
%         EEG_ori_step_3.data = buffer.data{3}; % asr
%         pop_eegplot(EEG_ori_step_3);  
% %         title('asr, sd=20');
%         
%         EEG_ori_step_4.data = buffer.data{4}; % orica
%         pop_eegplot(EEG_ori_step_4);   
% %         title('orica');
        
    end
    
end


% buffer = REST_offline(EEG_DATASET_FILEPATH, EEG_CALIB_FILEPATH, pipe);
% 
% cd(current_path);

% output_filepath = 
% saveas

disp('Done')