# Setup MATLAB PATH
Setup the Matlab PATH environment variables in the [docs] folder

In addition, download eeglab_14.1.1.zip, extract and use the [Add Folder] to add it to the PATH.
https://drive.google.com/drive/u/1/folders/1uhztShPilfeumcn7sOwxfaGnb6NadbYq

# mfSSVEP v2 in Duke Eye Center Analysis
Use the script in _Wenhao Hiobeen folder for Duke 2nd mfssvep xdf file analysis. The main execution file is main.m.

The dataset is found in dataset_links.txt
