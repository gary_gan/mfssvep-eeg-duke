%% addpath
addpath(your/path/to/EEGAnalyzer.m) 

clear; close all; clc
%% GUI
NuPodEEGAnalyzer
%% Instruction
INPUT_DATA_DIR = fullfile(fileparts(which('EEGAnalyzer')), 'examples');
OUTPUT_DATA_DIR = INPUT_DATA_DIR;
% load data
data = NuPodDataLoader;
ALLEEG = data.allToEEGStruct(INPUT_DATA_DIR);

% preprocessing
analyzer = EEGAnalyzer('outputDir', OUTPUT_DATA_DIR, 'outputOrg', 'flat');
ALLEEG = analyzer.doBandpass(ALLEEG, 'lowedge', 1, 'highedge', 80);
analyzer.showBandpassPRM();
ALLEEG = analyzer.doNotchfiltering(ALLEEG, 'notch_low', 59, 'notch_high', 61);
analyzer.showNotchfilteringPRM();

ALLEEG = analyzer.doASR(ALLEEG, 'enable_remove_flatline', 0, 'enable_remove_hopeless_channels', 0, 'enable_remove_hopeless_time_window', 0);
analyzer.showASRPRM();
% plot PSD
analyzer.plotPSD(ALLEEG, 'overlap_fraction', 0.5, 'sync_avg_segment_sec', 10,'y_max',30,'y_min',-20, 'x_max',50);
analyzer.plotPSD(ALLEEG, 'overlap_fraction', 0.5, 'sync_avg_segment_sec', 10,'y_max',30,'y_min',-30,'normalize_data',1,'x_max',50);
analyzer.plotPSD(ALLEEG, 'overlap_fraction', 0.5, 'sync_avg_segment_sec', 10,'y_max',30,'y_min',-50,'enable_CCA',1,'x_max',50);
analyzer.plotPSD(ALLEEG, 'overlap_fraction', 0.5, 'sync_avg_segment_sec', 10,'y_max',0,'y_min',-50,'normalize_data',1,'enable_CCA',1,'x_max',50);
% plot waveform
analyzer.plotWaveform(ALLEEG, 'window_sec', 5, 'normalized_output', 0);
analyzer.plotWaveform(ALLEEG, 'window_sec', 5, 'normalized_output', 1, 'normalized_offset', 5);

%plot oddball
analyzer.plotOddball(ALLEEG, 'plot_ERP_smoothing', 5, 'plot_ERP_amplitude_min', -250, 'plot_ERP_amplitude_max', 250)
