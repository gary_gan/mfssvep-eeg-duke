function cca_cor(s,filepath,epoch)
% s=setting;
% [A,B,r,U,V] = canoncorr(X,Y)
[pathstr,name,ext] = fileparts(filepath);
C = strsplit(name,'-');
name = cell2mat([C(1) '-' C(4)]);

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

data = epoch;

expression = '\d+Hz';
[startIndex,endIndex] = regexp(filepath,expression) ;
freq=str2num(filepath(startIndex:endIndex-2));

t = (1:length(data))/SRATE;
ref1 = sin(2*pi*freq*t);
ref2 = cos(2*pi*freq*t);
REF = [ref1; ref2];
for ch = 1:size(data,1)
    for i = 1:size(data,3)
        temp = squeeze(data(ch,:,i));
        [pxx(i,:),f] = pmtm(temp,4,length(temp),SRATE);
    end
    
    pxxdb = 10*log10(pxx);
    Q1 = prctile(pxxdb,25);
    Q2 = median(pxxdb,1);
    Q3 = prctile(pxxdb,75);
    
    figure;hold on;set(gcf,'visible','off')
    fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
    plot(f,Q2,'k')
    axis([5 40 -30 30]);
    grid on;set(gca,'fontsize',14)
    title([name ' \ ' char(CH_LABEL(ch))],'fontsize',12);
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);
    
    SAVE_PATH = [pathstr '\Figures\CCA+PSD( ' num2str(s.seg_len) 's seg)\' name];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(ch)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(ch)) '.fig'])
    close;
end

corvalue = [];
for i = 1:size(data,3)
    temp = squeeze(data(:,:,i));
    clear A B r U V
    [A,B,r,U,V] = canoncorr(temp',REF');
    corvalue = [corvalue r(1,1)];
    [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
end

pxxdb = 10*log10(pxx);
Q1 = prctile(pxxdb,25);
Q2 = median(pxxdb,1);
Q3 = prctile(pxxdb,75);

figure;hold on;set(gcf,'visible','off')
fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
plot(f,Q2,'k')
axis([5 40 -30 30]);
grid on;set(gca,'fontsize',14)
title([name ' \ CCA result \ Avg. Corr. = ' num2str(mean(corvalue),'%4.2f')],'fontsize',12);
xlabel('Frequency (Hz)','fontsize',14);
ylabel('Power (dB/Hz)','fontsize',14);

SAVE_PATH = [pathstr '\Figures\CCA+PSD( ' num2str(s.seg_len) 's seg)\' name];
if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
print('-dtiff','-r200',[SAVE_PATH '\CCA result.tiff'])
saveas(gcf,[SAVE_PATH '\CCA result.fig'])
close;
