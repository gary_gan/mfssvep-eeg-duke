% Get the white color intensity depending on the cca_rel value.
function [rgb] = get_sector_color(cca_rel)
    rgb = [1*cca_rel 1*cca_rel 1*cca_rel];
    rgb = rgb*5; % multiplifer boost
    
%     if (cca_rel < 0.1)
%         rgb = [1*0.25 1*0.25 1*0.25]
%     elseif (cca_rel < 0.2)
%         rgb = [1*0.5 1*0.5 1*0.5]
%     elseif (cca_rel < 1)
%         rgb = [1 1 1]
%     end
end