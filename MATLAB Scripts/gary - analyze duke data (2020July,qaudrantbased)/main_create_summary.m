clear all; clc;
summary_struct_filepath = "C:\Recordings\sub-Gary\ses-S001_right\eeg\20200729-run1\cca_avg_fstruct_20200729-run1.mat"
TAG = '20200729-run1';
SAVE_PATH = 'c:\Recordings\sub-Gary\ses-S001_right\eeg\';
SAVE_PATH_ROOT = fullfile(SAVE_PATH, TAG);

SAVE_PATH_BOXPLOT_PER_SESSION = fullfile(SAVE_PATH_ROOT, 'SUMMARY - CCA MEAN Box Plot2 (Per Session)');
SAVE_PATH_BOXPLOT_ALL_SESSION = fullfile(SAVE_PATH_ROOT, 'SUMMARY - CCA MEAN Box Plot2 (All Session)');
SAVE_PATH_BOXPLOT_CUSTOM_SESSION = fullfile(SAVE_PATH_ROOT, 'SUMMARY - CCA MEAN Box Plot2 (Custom Session)');
%% 
load(summary_struct_filepath);

xlab = {'p1_11_f8', 'p1_11_f9', 'p1_11_f10', 'p1_11_f11'};
xlab = {'p1_11_f8', 'p1_11_f9'};

% data=vertcat(Ac,Bc,Cc);
N_GROUP = 4; % 4 patterns

allsessions_p11_f8 = []

% store the info for all session
allsessions = struct();
allsessions.sessionid = '1-8';
PATTERN_LIST=[11, 12, 13, 14];

records = fieldnames(cca_rel_avg_struct);
for k=1:numel(records)
    % get filename
    filename = records{k};
    
    % get eye direction
    if (isfield(cca_rel_avg_struct.(records{k}), 'left')) eye_direction = 'left';
    else eye_direction = 'right';
    end

    % custom pick session
    custom_session = concat_session(cca_rel_avg_struct.(records{k}).(eye_direction), [1,3,6,7]);
    
    % iterate each session
    currsession = struct();
    for n_session=1:8
        for n_pattern=1:length(PATTERN_LIST)
            session_pattern_name = [ 'p' num2str(n_session) '_' num2str(PATTERN_LIST(n_pattern)) ];
            pattern_name = [ 'p' num2str(PATTERN_LIST(n_pattern)) ];
            
            currsession.sessionid = num2str(n_session); % referred by boxplot function for filename purpose
            currsession.(pattern_name).f8 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f8';
            currsession.(pattern_name).f9 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f9';
            currsession.(pattern_name).f10 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f10';
            currsession.(pattern_name).f11 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f11';
                
            if (n_session == 1)
                allsessions.(pattern_name).f8 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f8';
                allsessions.(pattern_name).f9 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f9';
                allsessions.(pattern_name).f10 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f10';
                allsessions.(pattern_name).f11 = cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f11';           
            
            % concatenate all sessions together
            elseif (n_session > 1) 
                allsessions.(pattern_name).f8 = vertcat(allsessions.(pattern_name).f8, cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f8');  
                allsessions.(pattern_name).f9 = vertcat(allsessions.(pattern_name).f9, cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f9');  
                allsessions.(pattern_name).f10 = vertcat(allsessions.(pattern_name).f10, cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f10');  
                allsessions.(pattern_name).f11 = vertcat(allsessions.(pattern_name).f11, cca_rel_avg_struct.(records{k}).(eye_direction).(session_pattern_name).e138.cca.f11');  
            end
            
        end
        
        %% draw boxplot for the current session
        fig_title = [ 'Session ' num2str(n_session) ];
        draw_boxplot(currsession, SAVE_PATH_BOXPLOT_PER_SESSION, fig_title);
        
    end
    
    %% Draw box plot for only selected session
    fig_title = [ 'Session ' num2str(custom_session.sessionid) ];
    draw_boxplot(custom_session, SAVE_PATH_BOXPLOT_CUSTOM_SESSION, fig_title);
    
    %% Draw box plot for all sessions
    fig_title = [ 'Session 1-8' ];
    draw_boxplot(allsessions, SAVE_PATH_BOXPLOT_ALL_SESSION, fig_title);
    
    disp('Done!');
    
    %% Finished concanteating all sessions, now plot boxplot
    % all freq 8
    A = concatarrays([allsessions.p11.f8, allsessions.p12.f8, allsessions.p13.f8, allsessions.p14.f8]);
    % all freq 9
    B = concatarrays([allsessions.p11.f9, allsessions.p12.f9, allsessions.p13.f9, allsessions.p14.f9]);
    % all freq 10
    C = concatarrays([allsessions.p11.f10, allsessions.p12.f10, allsessions.p13.f10, allsessions.p14.f10]);
    % all freq 11
    D = concatarrays([allsessions.p11.f11, allsessions.p12.f11, allsessions.p13.f11, allsessions.p14.f11]);

    data=cell(N_GROUP,4);  % 4 freq per group
    for ii=1:size(data,1)
    Ac{ii}=A(:,ii);
    Bc{ii}=B(:,ii);
    Cc{ii}=C(:,ii);
    Dc{ii}=D(:,ii);
    % Cc{ii}=p1_11_f10(:,ii);
    % Dc{ii}=p1_11_f11(:,ii);
    % Cc{ii}=C(:,ii);
    end

    data=vertcat(Ac, Bc, Cc, Dc);

    xlab = {'p11', 'p12', 'p13', 'p14'};

    col=[102,255,255, 120;
    51,153,255, 100;
    0, 0, 255, 100,
    255,111,255,100];
    col=col/255;

    multiple_boxplot(data',xlab,{'f8', 'f9', 'f10', 'f11'}, col');
    title([ 'NORMAL' char(10) 'Gary' char(10) 'CCA - ALL Sessions 1-8 (24 epochs per freq per pattern)']);
    
    %%
    
    % END< use for testing only!
    
    
    p1_11_f8 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_11.e138.cca.f8';
    p1_11_f9 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_11.e138.cca.f9';
    p1_11_f10 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_11.e138.cca.f10';
    p1_11_f11 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_11.e138.cca.f11';

    p1_12_f8 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_12.e138.cca.f8';
    p1_12_f9 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_12.e138.cca.f9';
    p1_12_f10 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_12.e138.cca.f10';
    p1_12_f11 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_12.e138.cca.f11';
    
    p1_13_f8 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_13.e138.cca.f8';
    p1_13_f9 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_13.e138.cca.f9';
    p1_13_f10 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_13.e138.cca.f10';
    p1_13_f11 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_13.e138.cca.f11';
    
    p1_14_f8 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_14.e138.cca.f8';
    p1_14_f9 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_14.e138.cca.f9';
    p1_14_f10 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_14.e138.cca.f10';
    p1_14_f11 = cca_rel_avg_struct.(records{k}).(eye_direction).p1_14.e138.cca.f11';   
    
    
    A = concatarrays([p1_11_f8, p1_12_f8, p1_13_f8, p1_14_f8]);
    B = concatarrays([p1_11_f9, p1_12_f9, p1_13_f9, p1_14_f9]);
    C = concatarrays([p1_11_f10, p1_12_f10, p1_13_f10, p1_14_f10]);
    D = concatarrays([p1_11_f11, p1_12_f11, p1_13_f11, p1_14_f11]);    

    data=cell(N_GROUP,4);  % 4 freq per group
    for ii=1:size(data,1)
    Ac{ii}=A(:,ii);
    Bc{ii}=B(:,ii);
    Cc{ii}=C(:,ii);
    Dc{ii}=D(:,ii);
    % Cc{ii}=p1_11_f10(:,ii);
    % Dc{ii}=p1_11_f11(:,ii);
    % Cc{ii}=C(:,ii);
    end

    data=vertcat(Ac, Bc, Cc, Dc);

    xlab = {'p11', 'p12', 'p13', 'p14'};

    col=[102,255,255, 120;
    51,153,255, 100;
    0, 0, 255, 100,
    255,111,255,100];
    col=col/255;

    multiple_boxplot(data',xlab,{'f8', 'f9', 'f10', 'f11'}, col');
    title([ 'Gary' char(10) 'CCA per Session']);
end



p1_11_f8 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_11.e138.cca.f8'
p1_11_f9 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_11.e138.cca.f9';
p1_11_f10 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_11.e138.cca.f10';
p1_11_f11 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_11.e138.cca.f11';

p1_12_f8 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_12.e138.cca.f8'
p1_12_f9 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_12.e138.cca.f9';
p1_12_f10 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_12.e138.cca.f10';
p1_12_f11 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_12.e138.cca.f11';

p1_13_f8 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_13.e138.cca.f8'
p1_13_f9 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_13.e138.cca.f9';
p1_13_f10 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_13.e138.cca.f10';
p1_13_f11 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_13.e138.cca.f11';

p1_14_f8 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_14.e138.cca.f8'
p1_14_f9 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_14.e138.cca.f9';
p1_14_f10 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_14.e138.cca.f10';
p1_14_f11 = cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_14.e138.cca.f11';

% A (Row, Col)
A = concatarrays([p1_11_f8, p1_12_f8, p1_13_f8, p1_14_f8]);
B = concatarrays([p1_11_f9, p1_12_f9, p1_13_f9, p1_14_f9]);
C = concatarrays([p1_11_f10, p1_12_f10, p1_13_f10, p1_14_f10]);
D = concatarrays([p1_11_f11, p1_12_f11, p1_13_f11, p1_14_f11]);

% A = concatarrays(A, p1_12_f8, 2);

% A{1,1} = p1_11_f8(1);
% A{2,1} =p1_11_f8(2);

data=cell(N_GROUP,4);  % 4 freq per group
for ii=1:size(data,1)
Ac{ii}=A(:,ii);
Bc{ii}=B(:,ii);
Cc{ii}=C(:,ii);
Dc{ii}=D(:,ii);
% Cc{ii}=p1_11_f10(:,ii);
% Dc{ii}=p1_11_f11(:,ii);
% Cc{ii}=C(:,ii);
end

data=vertcat(Ac, Bc, Cc, Dc);

xlab = {'p11', 'p12', 'p13', 'p14'};

col=[102,255,255, 120;
51,153,255, 100;
0, 0, 255, 100,
255,111,255,100];
col=col/255;

multiple_boxplot(data',xlab,{'f8', 'f9', 'f10', 'f11'}, col');
title([ 'Gary' char(10) 'CCA per Session']);

% 
% data=vertcat(cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_11.e138.cca.f8, ...
%     cca_rel_avg_struct.Gary_07_21_2020_mfssvep_right_1.right.p1_11.e138.cca.f9);
% 
% multiple_boxplot(data',xlab,{'A', 'B', 'C'},col')
% title('Here it is!')

% create_summary(load(summary_struct_filepath).cca_rel_avg_struct, SAVE_PATH_ROOT);

function A = concatarrays(inputs)
    for col=1:size(inputs,2)
        for row=1:size(inputs(:,1))
            A(row,col) = inputs(row,col);
        end
    end
end

function A = concatarray(A, input, col)
    for ii=1:size(input)
        A{ii,col} = input(ii);
    end
end

function draw_boxplot (sessionstruct, savepath, fig_title)
    N_GROUP  = 4;
    %% Finished concanteating all sessions, now plot boxplot
    % all freq 8
    A = concatarrays([sessionstruct.p11.f8, sessionstruct.p12.f8, sessionstruct.p13.f8, sessionstruct.p14.f8]);
    % all freq 9
    B = concatarrays([sessionstruct.p11.f9, sessionstruct.p12.f9, sessionstruct.p13.f9, sessionstruct.p14.f9]);
    % all freq 10
    C = concatarrays([sessionstruct.p11.f10, sessionstruct.p12.f10, sessionstruct.p13.f10, sessionstruct.p14.f10]);
    % all freq 11
    D = concatarrays([sessionstruct.p11.f11, sessionstruct.p12.f11, sessionstruct.p13.f11, sessionstruct.p14.f11]);

    data=cell(N_GROUP,4);  % 4 freq per group
    for ii=1:size(data,1)
    Ac{ii}=A(:,ii);
    Bc{ii}=B(:,ii);
    Cc{ii}=C(:,ii);
    Dc{ii}=D(:,ii);
    % Cc{ii}=p1_11_f10(:,ii);
    % Dc{ii}=p1_11_f11(:,ii);
    % Cc{ii}=C(:,ii);
    end

    data=vertcat(Ac, Bc, Cc, Dc);

    xlab = {'p11', 'p12', 'p13', 'p14'};

    col=[102,255,255, 120;
    51,153,255, 100;
    0, 0, 255, 100,
    255,111,255,100];
    col=col/255;

    figure('visible','off'); % one figure per frequency
    multiple_boxplot(data',xlab,{'f8', 'f9', 'f10', 'f11'}, col');
    % title([ 'NORMAL' char(10) 'Gary' char(10) 'CCA - ALL Sessions 1-8 (24 epochs per freq per pattern)']);
    title(fig_title);
    
    % save figure
    filename = [ 'boxplot_gary_session_' sessionstruct.sessionid '.png'];
    output_filepath = fullfile(savepath, filename);
    util_savefig(output_filepath);    

end

%% Allow user to select which session to concat
function allsessions = concat_session(cca_rel_avg_struct, passedsession)
    PATTERN_LIST=[11, 12, 13, 14];

    allsessions = struct();
    allsessions.sessionid = num2str(passedsession);
    
    for n_session=1:length(passedsession)
        session_num = passedsession(n_session);
        for n_pattern=1:length(PATTERN_LIST)
            session_pattern_name = [ 'p' num2str(session_num) '_' num2str(PATTERN_LIST(n_pattern)) ];
            pattern_name = [ 'p' num2str(PATTERN_LIST(n_pattern)) ];   
            
            if (n_session == 1)
                allsessions.(pattern_name).f8 = cca_rel_avg_struct.(session_pattern_name).e138.cca.f8';
                allsessions.(pattern_name).f9 = cca_rel_avg_struct.(session_pattern_name).e138.cca.f9';
                allsessions.(pattern_name).f10 = cca_rel_avg_struct.(session_pattern_name).e138.cca.f10';
                allsessions.(pattern_name).f11 = cca_rel_avg_struct.(session_pattern_name).e138.cca.f11';           
            
            % concatenate all sessions together
            elseif (n_session > 1) 
                allsessions.(pattern_name).f8 = vertcat(allsessions.(pattern_name).f8, cca_rel_avg_struct.(session_pattern_name).e138.cca.f8');  
                allsessions.(pattern_name).f9 = vertcat(allsessions.(pattern_name).f9, cca_rel_avg_struct.(session_pattern_name).e138.cca.f9');  
                allsessions.(pattern_name).f10 = vertcat(allsessions.(pattern_name).f10, cca_rel_avg_struct.(session_pattern_name).e138.cca.f10');  
                allsessions.(pattern_name).f11 = vertcat(allsessions.(pattern_name).f11, cca_rel_avg_struct.(session_pattern_name).e138.cca.f11');  
            end
        end
    end
end
