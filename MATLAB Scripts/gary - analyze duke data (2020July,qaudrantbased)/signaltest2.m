% EEG = pop_loadxdf('C:\Recordings\signaltest\lsl\sub-P001\ses-sine-8hz\eeg\sub-P001_ses-sine-8hz_task-T1_run-001_eeg.xdf' , 'streamtype', 'EEG', 'exclude_markerstreams', {});
filepath = 'C:\Recordings\signaltest\sub-P001\ses-sine-10hz_400uVpp\eeg\sub-P001_ses-sine-10hz_400uVpp_task-T1_run-002_eeg.xdf';


filepath = 'C:\Recordings\signaltest\lsl\sub-P001\ses-ramp-1hz\eeg\sub-P001_ses-ramp-1hz_task-T1_run-001_eeg.xdf';
% filepath = 'C:\Recordings\signaltest\lsl\sub-P001\ses-sine-12hz\eeg\sub-P001_ses-sine-12hz_task-T1_run-001_eeg.xdf'

%% Settings (used by custom pipeline)
SAVE_PATH = 'c:\Recordings\signaltest\Figures'

TAG = 'signaltest'

PREPROCESSING_PIPELINE = 'masaki'
CONCAT = 0;
ICA = 0;

setting.SAVE_PATH_ROOT = fullfile(SAVE_PATH, TAG);
if ~exist(setting.SAVE_PATH_ROOT,'dir') mkdir(setting.SAVE_PATH_ROOT); end;

setting.SAVE_FIGURE = 0;

% pattern_list = ['a', 'b', 'c', 'd', 'e', 'f'];
pattern_list = {'11', '12', '13', '14', '15', '16'};
% pattern_list = {'11', '12'};
pattern = pattern_list{1};
setting.pattern = pattern;

setting.chlabel = {'CH1', 'CH2', 'PO1', 'O1', 'Pz', 'O2', 'Oz', 'PO2'};

setting.seg_len = 10;
setting.overlap = 0;
setting.srate = 500;
% setting.NW = 2;
setting.NW = [2 : 1 : 4]; % Time-bandwidth product 'NW' should be greater than or equal to 1.25 when 'Droplasttaper' is true.
setting.NW = 1.25;
setting.RMBASE_flag = true;   % apply remove baseline filter or not
setting.BP_flag = true;      % apply bandpass filter or not
setting.ASR_flag = true;     % apply ASR algorithm or not
setting.BPLE = 1;           % bandpass low edge
setting.BPHE = 35;          % bandpass high edge

setting.epoch_range = [ 0 600 ];   


setting.CCA.ref_freq = 10;

%% Load Data
EEG = pop_loadxdf(filepath);
EEG.setname='10hz';
EEG = eeg_checkset( EEG );
EEG = pop_select( EEG, 'channel',{'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'});
EEG = eeg_checkset( EEG );

%% Preprocess
EEG = pop_rmbase( EEG,[], []);
EEG = pop_eegfiltnew( EEG, setting.BPLE, setting.BPHE); 
EEG.data = asr_algo(setting, EEG.data);

%% CCA PSD
epoch = EEG.data;
ref_freq = setting.CCA.ref_freq;
nw = setting.NW;

[cca_rel_avg_, snr_area_, snr_db_] = cca_psd_of_all_ch(setting,filepath,epoch,ref_freq,nw);


%% figure; 
[data, freq] = pop_spectopo(EEG, 1, [], 'EEG' , 'freqrange',[1 50],'electrodes','off');

for chan=1:6
%         subplot(2,3,chan);plot(freq, [EC(chan,:)' EO(chan,:)']);grid;    
    if strcmp(EEG.chanlocs(chan).labels, 'Pz') == 1
        subplot_idx = 2;
    elseif strcmp(EEG.chanlocs(chan).labels, 'PO1') == 1
        subplot_idx= 4;
    elseif strcmp(EEG.chanlocs(chan).labels, 'PO2') == 1
        subplot_idx= 6;
    elseif strcmp(EEG.chanlocs(chan).labels, 'O1') == 1
        subplot_idx= 7;
    elseif strcmp(EEG.chanlocs(chan).labels, 'Oz') == 1
        subplot_idx= 8;
    elseif strcmp(EEG.chanlocs(chan).labels, 'O2') == 1
        subplot_idx= 9;            
    end
    
    [~, marker_indices_highest_amp] = max(data(chan,:)');
    
%     subplot(3,3,subplot_idx);
    plot(freq, [data(chan,:)']);
    hold on;
    plot(freq(marker_indices_highest_amp),data(chan, marker_indices_highest_amp),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);
    
    grid;    
    title([EEG.chanlocs(chan).labels])
    xlim([1 25]);
    %axis([-0.2 1 -10 10])
end
legend('Sine 10Hz');
xlabel('Time (s)');
ylabel('dB');
figure(gcf);  



function [X] = getSin(srate, nchannel, frequency, isaddrandnoise)
    Fs = srate;            % Sampling frequency
    T = 1/Fs;             % Sampling period
    L = srate;             % Length of signal
    t = (0:L-1)*T;        % Time vector
    S = 0.7*sin(2*pi*frequency*t) + 0.3*sin(2*pi*60*t);
    for ch = 1:nchannel
        if isaddrandnoise
            X(:,ch) = S + 2*randn(size(t)); % add noise
        else
            X(:,ch) = S
        end
    end
end




