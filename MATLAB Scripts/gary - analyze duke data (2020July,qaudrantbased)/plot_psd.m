% function plot_psd (data, out_filepath, ref_freq, nw, cca_cor, xlim_range, tag)
% Plot PSD and PSD Diff
function plot_psd (data, srate, out_dir, out_filename, varargin)

%% Input Parser
p = inputParser;
addRequired(p, 'data');
addRequired(p, 'srate', @isnumeric);
addRequired(p, 'out_dir', @ischar);
addRequired(p, 'out_filename', @ischar);
addParameter(p, 'nw', 3, @isnumeric);
addParameter(p, 'plot_freq_range', [1 50], @isnumeric);
addParameter(p, 'dir_suffix', '', @ischar); % folder suffix
addParameter(p, 'marker_freq', 0, @isnumeric);
addParameter(p, 'sbj_name', '', @ischar);
addParameter(p, 'title_suffix', '', @ischar);
addParameter(p, 'epoch_baseline', struct());
addParameter(p, 'plot_psd_diff', 0, @isnumeric);
addParameter(p, 'plot_marker', 0, @isnumeric);
addParameter(p, 'legend', 'data', @ischar);

parse(p, data, srate, out_dir, out_filename, varargin{:});
p.Results

%% Extract user inputs
xlim_range = p.Results.plot_freq_range;
SRATE = p.Results.srate;
NW = p.Results.nw;
N_SEGMENT = size(p.Results.data, 3);
ref_freq = p.Results.marker_freq;
name = p.Results.sbj_name;
title_suffix = p.Results.title_suffix;
dir_suffix = p.Results.dir_suffix;
data_baseline = p.Results.epoch_baseline;
is_plot_psd_diff = p.Results.plot_psd_diff;
is_plot_marker = p.Results.plot_marker;
legend_name_ = p.Results.legend;

%% Figuring out the plot elements
% marker style for each fft spectral line
if is_plot_marker
    marker = '-*';
else
    marker = '-';
end

% legend name
legend_name = [];
legend_name{end+1} = legend_name_;
if ref_freq > 0
    legend_name{end+1} = 'Ref Freq';
end
if numel(data_baseline) > 1
    legend_name{end+1} = 'Baseline';
end

% [pathstr,name,ext] = fileparts(filepath);

% N_SEGMENT = size(data, 3);
% NW = nw;
% SRATE = s.srate;
% SEGLEN = (size(data,2)/SRATE);

%% 
SEGLEN = (size(data,2)/SRATE);

% iterate segment
for i=1:N_SEGMENT
%     temp = squeeze(data(ch,:,i));
    temp_ = squeeze(data(1,:,i));
    [pxx_(i,:),f] = pmtm(temp_,NW,length(temp_),SRATE);    
end

%% pmtm the Baseline
if numel(data_baseline) > 1
    % downsample
    NW_baseline = NW; % baseline uses the same NW as the arg
    N_DS_FACTOR = 3;
    %     data_baseline_ = data_baseline(:,1:end-1);
    data_baseline_ = data_baseline(:,1:end);
    data_baseline_ds = zeros(size(data_baseline_,1), size(data_baseline_,2)/N_DS_FACTOR);
    %     data_baseline_ds = zeros(size(data_baseline,1), 2500);

    for i=1:size(data_baseline,1)
        data_baseline_ds(i,:) = downsample(data_baseline_(i,:),N_DS_FACTOR);
    end
    t = squeeze(data_baseline_ds(1,:,:));
    [pxx_baseline, f_baseline] = pmtm(t,NW_baseline,length(t),SRATE);  
    pxx_baseline = pxx_baseline';    
    Q2_baseline = median(pxx_baseline,1);
end

%% Calculate metrics for plot
Q2 = median(pxx_,1);
Q2_zscore = zscore(Q2);

% cca out (db)
pxxdb = 10*log10(pxx_);
Q2_db = median(pxxdb,1);  
Q2_db_zscore = zscore(Q2_db);

if numel(data_baseline) > 1
    % base line (linear)
    Q2_baseline = median(pxx_baseline,1);
    Q2_baseline_zscore = zscore(Q2_baseline);   

    % base line (db)
    pxx_baseline_db = 10*log10(pxx_baseline);
    Q2_baseline_db = median(pxx_baseline_db,1);    
    Q2_baseline_db_zscore = zscore(Q2_baseline_db);   
end

%%
% ----------------------------------------------------------------------
% Plot in Linear scale
% ----------------------------------------------------------------------
figure; set(gcf,'visible','off')
% plot(f,Q2)
marker_indices = 1:length(f);
plot(f,Q2_zscore,marker,'MarkerIndices',marker_indices, ...
          'MarkerFaceColor','blue', ...
          'MarkerEdgeColor','blue',...
          'MarkerSize',1);
% plot a red * marker at a specific frequency (e.g. CCA ref freq)
hold on;
if ref_freq > 0
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_zscore(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);
end
xlim( xlim_range );
grid on; grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
xticks( [1:1:100] );

title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) char(10) title_suffix  ],'fontsize',12,'interpreter','none');
xlabel('Frequency (Hz)','fontsize',14);
ylabel('Power (Linear)','fontsize',14);
legend(legend_name);
% create a sub directory for PSD analysis
out_subdir = fullfile(out_dir, [ 'PSD (Linear) (Z-Score) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
util_savefig(fullfile(out_subdir, out_filename));

% plot overlap with baseline
if numel(data_baseline) > 1
%     plot(f,Q2_baseline, 'r');
    plot(f,Q2_baseline_zscore,marker,'MarkerIndices',marker_indices, ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',1);
    legend(legend_name);
    % create a sub directory for PSD analysis
    out_subdir = fullfile(out_dir, [ 'PSD (with Baseline) (Linear) (Z-Score) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
    util_savefig(fullfile(out_subdir, out_filename));
end
close;


%%
% ----------------------------------------------------------------------
% Plot in dB scale
% ----------------------------------------------------------------------
figure; set(gcf,'visible','off')
% plot(f,Q2)
marker_indices = 1:length(f);
plot(f,Q2_db_zscore,marker,'MarkerIndices',marker_indices, ...
          'MarkerFaceColor','blue', ...
          'MarkerEdgeColor','blue',...
          'MarkerSize',1);
% plot a red * marker at a specific frequency (e.g. CCA ref freq)
hold on;
if ref_freq > 0
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_db_zscore(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);
end
xlim( xlim_range );
grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
xticks([1:1:100]);

title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) char(10) title_suffix  ],'fontsize',12,'interpreter','none');
xlabel('Frequency (Hz)','fontsize',14);
ylabel('Power (dB)','fontsize',14);
legend(legend_name);
% create a sub directory for PSD analysis
out_subdir = fullfile(out_dir, [ 'PSD (dB) (Z-Score) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
util_savefig(fullfile(out_subdir, out_filename));    

% plot overlap with baseline
if numel(data_baseline) > 1
%     plot(f,Q2_baseline, 'r');
    plot(f,Q2_baseline_db_zscore,marker,'MarkerIndices',marker_indices, ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',1);
     legend(legend_name);
    % create a sub directory for PSD analysis
    out_subdir = fullfile(out_dir, [ 'PSD (with Baseline) (dB) (Z-Score) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
    util_savefig(fullfile(out_subdir, out_filename));
end
close;


if is_plot_psd_diff
    %%
    % ----------------------------------------------------------------------
    % Plot Diff in Linear scale
    % ----------------------------------------------------------------------
    Q2_diff = Q2./Q2_baseline;
    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    plot(f,Q2_diff,marker,'MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',1);
    hold on;
    if ref_freq > 0
        marker_indices_target_freq = find(f==ref_freq);
        plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
                  '-*', ...
                  'MarkerFaceColor','red', ...
                  'MarkerEdgeColor','red',...
                  'MarkerSize',13);
    end
    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
    xticks([1:1:100]);

    title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) char(10) title_suffix  ],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);    

    out_subdir = fullfile(out_dir, [ 'PSD Diff (Linear) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
    util_savefig(fullfile(out_subdir, out_filename));   
    close;       

    %%
    % ----------------------------------------------------------------------
    % Plot Diff (Z-Score) in Linear scale
    % ----------------------------------------------------------------------
    Q2_diff = Q2_zscore./Q2_baseline_zscore;
    figure;set(gcf,'visible','off')
    marker_indices = 1:length(f);
    plot(f,Q2_diff,marker,'MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',1);
    hold on;
    if ref_freq > 0
        marker_indices_target_freq = find(f==ref_freq);
        plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
                  '-*', ...
                  'MarkerFaceColor','red', ...
                  'MarkerEdgeColor','red',...
                  'MarkerSize',13);
    end
    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
    xticks([1:1:100]);

    title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) char(10) title_suffix  ],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);    

    out_subdir = fullfile(out_dir, [ 'PSD Diff (Linear) (Z-Score) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
    util_savefig(fullfile(out_subdir, out_filename));   
    close; 


    %%
    % ----------------------------------------------------------------------
    % Plot Diff in dB scale
    % ----------------------------------------------------------------------
    Q2_diff = Q2_db./Q2_baseline_db;
    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    plot(f,Q2_diff,marker,'MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',1);
    hold on;
    if ref_freq > 0
        marker_indices_target_freq = find(f==ref_freq);
        plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
                  '-*', ...
                  'MarkerFaceColor','red', ...
                  'MarkerEdgeColor','red',...
                  'MarkerSize',13);
    end
    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
    xticks([1:1:100]);

    title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) char(10) title_suffix  ],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);    

    out_subdir = fullfile(out_dir, [ 'PSD Diff (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
    util_savefig(fullfile(out_subdir, out_filename));   
    close;       

    %%
    % ----------------------------------------------------------------------
    % Plot Diff (Z-Score) in dB scale
    % ----------------------------------------------------------------------
    Q2_diff = Q2_db_zscore./Q2_baseline_db_zscore;
    figure;set(gcf,'visible','off')
    marker_indices = 1:length(f);
    plot(f,Q2_diff,marker,'MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',1);
    hold on;
    if ref_freq > 0
        marker_indices_target_freq = find(f==ref_freq);
        plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
                  '-*', ...
                  'MarkerFaceColor','red', ...
                  'MarkerEdgeColor','red',...
                  'MarkerSize',13);
    end
    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
    xticks([1:1:100]);

    title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) char(10) title_suffix  ],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);    

    out_subdir = fullfile(out_dir, [ 'PSD Diff (dB) (Z-Score) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' dir_suffix ]);
    util_savefig(fullfile(out_subdir, out_filename));   
    close; 
end

end
