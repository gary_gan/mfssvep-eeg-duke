clear;clc;

% default_example()
example_1()

function default_example()
    % Create example data
    A=rand(100,10);
    B=rand(200,10);
    C=rand(150,10);

    % prepare data
    data=cell(10,3);
    for ii=1:size(data,1)
    Ac{ii}=A(:,ii);
    Bc{ii}=B(:,ii);
    Cc{ii}=C(:,ii);
    end
    data=vertcat(Ac,Bc,Cc);

    xlab={'Hey','this','works','pretty','nicely.','And','it','has','colors','!!!!'};
    col=[102,255,255, 200;
    51,153,255, 200;
    0, 0, 255, 200];
    col=col/255;

    multiple_boxplot(data',xlab,{'A', 'B', 'C'},col')
    title('Here it is!')
end

%% Example 2
function example_1()
    % Create example data
    N_GROUP=5;
    A=rand(100,N_GROUP); % normal
    B=rand(100,N_GROUP); % glaucoma

    % prepare data
    data=cell(N_GROUP,2);
    for ii=1:size(data,1)
    Ac{ii}=A(:,ii);
    Bc{ii}=B(:,ii);
    end
    data=vertcat(Ac,Bc);

    xgrouplab={'CCASim', 'catCCASim', 'CCAQuad', 'catCCAQuad', 'TRCA'};
    xlab={'normal', 'glaucoma'};
    col=[102,255,255, 120;
    51,153,255, 100];
    col=col/255;

    multiple_boxplot(data',xgrouplab,xlab,col')
    title(['Box Plot for All Normal vs. Glaucoma' char(10) 'CCA Corr. Median'])
end



