% EEG = pop_loadxdf('C:\Recordings\signaltest\lsl\sub-P001\ses-sine-8hz\eeg\sub-P001_ses-sine-8hz_task-T1_run-001_eeg.xdf' , 'streamtype', 'EEG', 'exclude_markerstreams', {});
EEG = pop_loadxdf('C:\Recordings\signaltest\sub-P001\ses-sine-10hz_400uVpp\eeg\sub-P001_ses-sine-10hz_400uVpp_task-T1_run-002_eeg.xdf')
EEG.setname='10hz';
EEG = eeg_checkset( EEG );
EEG = pop_select( EEG, 'channel',{'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'});
EEG = eeg_checkset( EEG );

figure; 
[data, freq] = pop_spectopo(EEG, 1, [], 'EEG' , 'freqrange',[1 50],'electrodes','off');

for chan=1:6
%         subplot(2,3,chan);plot(freq, [EC(chan,:)' EO(chan,:)']);grid;    
    if strcmp(EEG.chanlocs(chan).labels, 'Pz') == 1
        subplot_idx = 2;
    elseif strcmp(EEG.chanlocs(chan).labels, 'PO1') == 1
        subplot_idx= 4;
    elseif strcmp(EEG.chanlocs(chan).labels, 'PO2') == 1
        subplot_idx= 6;
    elseif strcmp(EEG.chanlocs(chan).labels, 'O1') == 1
        subplot_idx= 7;
    elseif strcmp(EEG.chanlocs(chan).labels, 'Oz') == 1
        subplot_idx= 8;
    elseif strcmp(EEG.chanlocs(chan).labels, 'O2') == 1
        subplot_idx= 9;            
    end
    
    [~, marker_indices_highest_amp] = max(data(chan,:)');
    
%     subplot(3,3,subplot_idx);
    plot(freq, [data(chan,:)']);
    hold on;
    plot(freq(marker_indices_highest_amp),data(chan, marker_indices_highest_amp),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);
    
    grid;    
    title([EEG.chanlocs(chan).labels])
    xlim([1 25]);
    %axis([-0.2 1 -10 10])
end
legend('Sine 10Hz');
xlabel('Time (s)');
ylabel('dB');
figure(gcf);  