clear all; clc;

DATASET_ROOTDIR = 'c:\Recordings\visual-oddball\USE THIS DATA'
DATASET_ROOTDIR = 'c:\Recordings\visual-oddball-feb21'
%Scripts below compare the ERPs under two conditions
sbj_list = { 'sbj0' };

channelNameList = {'CH1', 'CH2', 'O1', 'PO1', 'PO2', 'Pz', 'Oz', 'O2' };

for i=1:length(sbj_list)
    xdf_filepath = fullfile(DATASET_ROOTDIR, sbj_list{i}, [ sbj_list{i} '_eceo.xdf'])    
        
    % SAVE SET
    out_filedir = fullfile(DATASET_ROOTDIR, 'Figures', sbj_list{i});     
    if ~exist(out_filedir,'dir') mkdir(out_filedir); end;
    
    for ev_idx=1:2 % EC and EO, EC=1, EO=2
        EEG.etc.eeglabvers = '2019.1'; % this tracks which version of EEGLAB is being used, you may ignore it
        EEG = pop_loadxdf(xdf_filepath , 'streamtype', 'EEG', 'exclude_markerstreams', {});
        
        for chidx=1:8
            EEG.chanlocs(chidx).labels= channelNameList{chidx};
        end
        
        EEG.setname='ECEO';
        EEG = eeg_checkset( EEG );
        EEG = pop_select( EEG, 'channel',{'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'});
        EEG = eeg_checkset( EEG );
        EEG = pop_eegfiltnew(EEG, 'locutoff',1,'hicutoff',40);
        EEG.setname='ECEO BP';
        EEG = eeg_checkset( EEG );
        EEG = clean_artifacts(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion','off','BurstRejection','off','Distance','Euclidian');
        EEG.setname='ECEO BP ASR';
        EEG = eeg_checkset( EEG );
        
        if ev_idx == 1 % Close
            event_marker='Close';
            event_type='EC';
        else
            event_marker='Open';
            event_type='EO';
        end        
        
        EEG = pop_epoch( EEG, {  event_marker  }, [0  60], 'newname', ['ECEO BP ASR ' event_type], 'epochinfo', 'yes');
        EEG = eeg_checkset( EEG );
        EEG = pop_rmbase( EEG, [],[]);
        EEG = eeg_checkset( EEG );
        % pop_eegplot( EEG, 1, 1, 1);
        
        if ev_idx == 1 % Close
            [EC, freq] = pop_spectopo(EEG, 1, [0  59998], 'EEG' , 'freqrange',[2 25],'electrodes','off');        
        else
            [EO, freq] = pop_spectopo(EEG, 1, [0  59998], 'EEG' , 'freqrange',[2 25],'electrodes','off');        
        end            
    end
    
    %% Plot
%     set(gcf,'visible','off');
    for chan=1:6
%         subplot(2,3,chan);plot(freq, [EC(chan,:)' EO(chan,:)']);grid;    
        if strcmp(EEG.chanlocs(chan).labels, 'Pz') == 1
            subplot_idx = 2;
        elseif strcmp(EEG.chanlocs(chan).labels, 'PO1') == 1
            subplot_idx= 4;
        elseif strcmp(EEG.chanlocs(chan).labels, 'PO2') == 1
            subplot_idx= 6;
        elseif strcmp(EEG.chanlocs(chan).labels, 'O1') == 1
            subplot_idx= 7;
        elseif strcmp(EEG.chanlocs(chan).labels, 'Oz') == 1
            subplot_idx= 8;
        elseif strcmp(EEG.chanlocs(chan).labels, 'O2') == 1
            subplot_idx= 9;            
        end
        subplot(3,3,subplot_idx);plot(freq, [EC(chan,:)' EO(chan,:)']);grid;    
        title([EEG.chanlocs(chan).labels])
        xlim([1 30]);
        %axis([-0.2 1 -10 10])
    end
    legend('EC','EO');
    xlabel('Time (s)');
    ylabel('dB');
    figure(gcf);    
    
    out_filename = [ sbj_list{i} '-eceo' ];
    out_filepath = fullfile(out_filedir, out_filename);
    util_savefig(out_filepath);    
    
    close all;
end


% set(gcf,'visible','on');
disp(['All done!']);
    


% for chan=1:6
%     subplot(2,3,chan);plot(freq, EC(chan,:));grid;    
%     title([EEG.chanlocs(chan).labels])
%     xlim([1 30]);
%     %axis([-0.2 1 -10 10])
% end
% legend('EC','EO')
% xlabel('Time (s)')
% ylabel('uV')
% figure(gcf)
