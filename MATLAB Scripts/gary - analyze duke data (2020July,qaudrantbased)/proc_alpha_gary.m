function proc_alpha_gary(s,filepath,EEG)
% s=setting;
% filepath=[FILE_PATH '\' char(FILE_LIST(i))];
[pathstr,name,ext] = fileparts(filepath);
C = strsplit(name,'-');
% name = cell2mat([C(1) '-' C(4)]);

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
if OVERLAP == 0 OVERLAP = 1;end
NW = s.NW;

% data = csvread(filepath)';
% eeg = data(2:9,:);

eeg = EEG.data;

% remove baseline
if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% bandpass
if s.BP_flag eeg = bandpass(s,eeg);end
% ASR algorithm
if s.ASR_flag eeg = asr_algo(s,eeg);end

eeg = eeg(:,1:60*SRATE);
[CH,PNT] = size(eeg);
for ch = [1:6]
    %% waveform
    SEGLEN = 0.5;
    seg = reshape(eeg(ch,:),SEGLEN*SRATE,[]);
    Q1 = prctile(seg',25);
    Q2 = median(seg',1);
    Q3 = prctile(seg',75);
    f = 1:SEGLEN*SRATE';
    figure;set(gcf,'visible','off');hold on
    h3=fill([f fliplr(f)],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5);
    h1=plot(f,Q2,'k','linewidth',2);
    h2=plot(mean(seg',1),'r','linewidth',2);
    ylim([-15 15])
    grid on;set(gca,'xticklabel',get(gca,'xtick')/SRATE,'fontsize',14)
    title(['Waveform of ' char(CH_LABEL(ch)) ' / ' name char(10) 'Sync. Avg. of ' num2str(60/SEGLEN) ' segments'],'fontsize',12,'interpreter','none');
    xlabel('Time (sec)','fontsize',14);
    ylabel('Amplitude (\muV)','fontsize',14);
    legend([h1 h3 h2],'Median','IQR','Mean')
    
    SAVE_PATH = [pathstr '\Figures\Waveform of Alpha( ' num2str(SEGLEN) 's seg)\'];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' name '_' char(CH_LABEL(ch)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' name '_' char(CH_LABEL(ch)) '.fig'])
    close;
    
    figure;set(gcf,'visible','off');hold on
    hist_range = [-50 50];
    bin_width = 0.5;
    [N,X]=hist(eeg(ch,:),hist_range(1):bin_width:hist_range(2));
    plot(X,N,'.')
    grid on;
    title(['Histogram of ' char(CH_LABEL(ch)) char(10) 'bin width: ' num2str(bin_width) '\muV'],'fontsize',12);
    xlabel('Amplitude (\muV)','fontsize',14);
    ylabel('Occurrence','fontsize',14);
    ylim([0 800]);set(gca,'xtick',hist_range(1):10:hist_range(2),'fontsize',12)
    
    SAVE_PATH = [pathstr '\Figures\Histogram of Alpha( ' num2str(SEGLEN) 's seg)\'];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' name '_' char(CH_LABEL(ch)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' name '_' char(CH_LABEL(ch)) '.fig'])
    close;
end
for ch = [1:6]
    pxx=[];
    for j = 1:10*SRATE:length(eeg)
        %% PSD
        temp = eeg(ch,j:j+10*SRATE-1);
        [pxx(end+1,:),fxx] = pmtm(temp,4,length(temp),SRATE);
    end
    % plot PSD
    pxxdb = 10*log10(pxx);
    Q1 = prctile(pxxdb,25);
    Q2 = median(pxxdb,1);
    Q3 = prctile(pxxdb,75);
    figure;set(gcf,'visible','off');hold on
    fill([fxx' fliplr(fxx')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5);
    plot(fxx,Q2,'k');
    grid on;axis([0 40 -30 30])
    title(['PSD(multitaper) of ' char(CH_LABEL(ch)) ' / ' name char(10) '6 segments(10sec)'],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);
    
    SAVE_PATH = [pathstr '\Figures\PSD of Alpha( ' num2str(SEGLEN) 's seg)\'];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' name '_' char(CH_LABEL(ch)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' name '_' char(CH_LABEL(ch)) '.fig'])
    close;
end
