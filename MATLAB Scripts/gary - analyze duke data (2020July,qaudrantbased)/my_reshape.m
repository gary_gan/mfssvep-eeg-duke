% returns a vector of final_size x 1 for table input
function output = my_reshape(input, final_size)
%     output = zeros(final_size, 1);
    output = NaN(final_size,1);
    for i=1:length(input)
        output(i,1) = input(i);
    end
end