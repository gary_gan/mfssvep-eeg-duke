function concat_session() 

    ALL_SESSION = struct();
    
    % p11 for session 1-8
    pattern_list = {'1_11', '2_11', '3_11', '4_11', '5_11', '6_11', '7_11', '8_11'};
    
    %%
    PREPROCESSING_PIPELINE = 'masaki' % rest_offline, masaki, gary
CONCAT = 0;
PREPROCESSING_PIPELINE = 'masaki' % rest_offline, masaki, gary
ICA = 0;    EPOCH_START = [138];
    FILE_LIST = get_all_files(FILE_PATH);

    for i =1:length(FILE_LIST)
        filepath = FILE_LIST{i};
        [pathstr,filename,ext] = fileparts(filepath);   
        expression = '.*mfssvep-.*[1-2].set'; % only process file with mfssvep inside name

        if regexp(FILE_LIST{i},expression)    
            C = strsplit(filename,'-');
                   
            %% extract diff epoch (event id 11-16) for each file
            for p_idx=1:length(pattern_list)
                pattern_id = pattern_list{p_idx};

                %% load .set for each pattern
                set_filename = [ filename '.set' ];
                set_filepath = fullfile(pathstr, set_filename)
                EEG = pop_loadset(set_filepath);                

                % Add metadata to the EEG struct from filename
                [~,filename,ext] = fileparts(set_filename);   
                C = strsplit(filename,'-');
                EEG.subject = cell2mat([ C(1) '-' C(2) ]);
                EEG.session = cell2mat([ C(3) '-' C(4) '-' C(5) ]);

                epoch_start = 0; % reset the epoch start 

                for e_idx=1:length(EPOCH_START)
                    try
                        epoch_start = (EPOCH_START(e_idx)/1000.0); %convert to seconds
                        epoch_end = epoch_start + 5;       
                        % Extract EEG per epoch
                        EEG = preproc(setting, pattern_id, EEG, FILE_LIST{i}, PREPROCESSING_PIPELINE, CONCAT, ICA, [epoch_start epoch_end]);
                    
                        % 6 x 2500 x 3
                        concat_epoch(EEG.data);
                    end
                end
                
            end
        end % end of for each file that matches regex
    end % end of for each file
end