function pass = check_epoch(filepath)

%% My Own loading XDF
    try
        % filepath='C:\Users\Gary\Desktop\_____________mfssvepduketest\DK0006-10_12_1947-mfssvep-left-1.xdf';
        % read xdf file
        eeg_ = pop_loadset(filepath);
        eeg_ = eeg_checkset(eeg_);

        % pop_eegplot(eeg, 1, 1, 1);
        % eeg = eeg.data;

        eeg = eeg_.data;
        
        if size(eeg_.event,2) < 54
            pass = false;
            disp([ 'Only found event # = ' num2str(size(eeg_.event,2)) ' in ' filepath ]);
            return;
            
        else
            pass = true;
        end
        
        pass = true;
    catch exception
        disp(filepath)
        disp(getReport(exception))
        pass = false;
        return;
    end

end
