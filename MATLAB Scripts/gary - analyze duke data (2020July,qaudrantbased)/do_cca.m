%% Modified 2020-07-31

function [cca_corrs, cca_signal] = do_cca(data, ref_freq, cca_type)
    cca_corrs = [];

    curr_cca_corr = 0; % cca corr computed in an epoch

    for i = 1:size(data,3)
        if (strcmp(cca_type, 'normal') == 1)
            [A, B, r, U, V, awx, ~] = CCA_normal(data(:, :, i)', ref_freq);
        elseif (strcmp(cca_type, 'pulse_trend') == 1)
            disp('pulse trend!');
            [A, B, r, U, V, awx, ~] = CCA_pulsetrend(data(:, :, i)', ref_freq);
        elseif (strcmp(cca_type, 'quad_pulse_trend') == 1)
            [A, B, r, U, V, awx, ~] = CCA_quadpulsetrend(data(:, :, i)', ref_freq);            
            disp('quad pulse trend');
        else
            disp('Unknown ref signal!');
        end
        curr_cca_corr = r(1,1);
        cca_corrs = [cca_corrs r(1,1)];
        
        cca_signal(1,:,i) = U(:,1)  % 1 x ccasignals x epoch
    end
    
end

