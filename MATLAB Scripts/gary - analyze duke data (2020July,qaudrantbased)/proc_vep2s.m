function proc_vep2s(s,filepath)
% s=setting;
% filepath=[FILE_PATH '\' char(FILE_LIST(i))];

SEGI = 2;% ignored segment in seconds
SEGD = 10;% data segement in seconds
SEGR = 2.5; % minimal rest segment in seconds: 2.5~3.5
TRIAL = 10;

[pathstr,name,ext] = fileparts(filepath);

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

data = csvread(filepath)';
ps = data(12,:);
% get onset index of each trial
onset = [];s2_onset = [];
CUTSEG = 2; % length of segment in trial
i = 1;
while i < length(ps)
    if i+7 <= length(ps) && ~any(~ps(i:i+7))
        onset(end+1) = i;
        % get onset index of segments in each trial
        j = i+SEGI*SRATE-1;
        while j <= i+(SEGI+SEGD)*SRATE-1
            if ~any(~ps(j:j+7))
                s2_onset(end+1) = j;
                j = j+CUTSEG*SRATE-1 ;
            else
                j = j+1 ;
            end
        end
        i = i+(SEGI+SEGD+SEGR)*SRATE;
    else
        i = i+1;
    end
end

% plot(onset,ones(1,length(onset)),'-b*')
% hold on
% plot(s2_onset,ones(1,length(s2_onset)),'ro')


eeg = data(2:9,:);
% remove baseline
if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% bandpass
if s.BP_flag eeg = bandpass(s,eeg);end
% ASR algorithm
if s.ASR_flag eeg = asr_algo(s,eeg);end

s2_onset = reshape(s2_onset,fix(SEGD/CUTSEG),[]);
k=[];
for i = 1:TRIAL
    for j = 1:fix(SEGD/CUTSEG)
        seg(:,:,j,i) = eeg(:,s2_onset(j,i):s2_onset(j,i)+CUTSEG*SRATE-1);
    end
   
end
epoch = squeeze(median(seg,3));

[CH,PNT,TRIAL] = size(epoch);

% plot PSD
for j = 1:CH
    pxx = [];f = [];
    for k = 1:TRIAL
        seg = epoch(j,:,k);
        [pxx(end+1,:),f] = pmtm(seg,NW,length(seg),SRATE);
    end
    pxxdb = 10*log10(pxx);
    Q1 = prctile(pxxdb,25);
    Q2 = median(pxxdb,1);
    Q3 = prctile(pxxdb,75);
    
    figure;hold on;set(gcf,'visible','off');
    fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
    plot(f,Q2,'k')
    axis([0 50 -40 60]);grid on;
    title([ name char(10) char(CH_LABEL(j)) ' \ ' num2str(SEG_LEN) 'sec segments \ Multitaper PSD'],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',12);
    ylabel('Power/Frequency (dB/Hz)','fontsize',12);
    
    SAVE_PATH = [pathstr '\Figures\PSD(multitaper, ' num2str(CUTSEG) 's seg)\' name];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(j)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(j)) '.fig'])
    close;
    
end