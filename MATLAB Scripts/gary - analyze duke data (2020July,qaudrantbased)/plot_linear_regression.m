SAVE_PATH = 'e:\_MFSSVEP DUKE\__share\_share with alessandro\linear_regression';

r_sq = struct;
pairs = struct;
pairs.Normal = {        { paramsnormal.SAPMD, paramsnormal.CCA_MEAN }, ...
                        { paramsnormal.SAPMD, paramsnormal.CCA_SD }, ...
                        { paramsnormal.SAPMD, paramsnormal.SBR } ...
                };
    
pairs.Glaucoma = {      { paramsglaucomaall.SAPMD, paramsglaucomaall.CCA_MEAN }, ...
                        { paramsglaucomaall.SAPMD, paramsglaucomaall.CCA_SD }, ...
                        { paramsglaucomaall.SAPMD, paramsglaucomaall.SBR } ...
                };    
    
groups = fieldnames(pairs);
for g_idx=1:numel(groups)
    group_name = groups{g_idx};
    
    pairs_ = pairs.(group_name);
    for i=1:numel(pairs_)
        y = pairs_{i}{1};
        x = pairs_{i}{2};

        mdl = fitlm(x,y)
        if i==1
            title_vs = 'MD vs. CCA Mean';
            fieldname = 'md_cmean';
        elseif i==2
            title_vs = 'MD vs. CCA SD';
            fieldname = 'md_csd';
        elseif i==3
            title_vs = 'MD vs. SBR';
            fieldname = 'md_sbr';
        end
%         r_sq_ord.(group_name).(fieldname) = mdl.Rsquared.Ordinary;
%         r_sq_adj.(group_name).(fieldname) = mdl.Rsquared.Adjusted;    
        result.(group_name).(fieldname).r_sq_ord = mdl.Rsquared.Ordinary;
        result.(group_name).(fieldname).r_sq_adj = mdl.Rsquared.Adjusted;
        result.(group_name).(fieldname).sse = mdl.SSE;
        result.(group_name).(fieldname).ssr = mdl.SSR;
        result.(group_name).(fieldname).sst = mdl.SST;
        result.(group_name).(fieldname).mse = mdl.MSE;
        result.(group_name).(fieldname).rmse = mdl.RMSE;
        result.(group_name).(fieldname).pval = mdl.Coefficients.pValue(2);
%         ano = anova(mdl,'summary')

%         figure;
%         h = plotResiduals(mdl, 'probability')
        
        figure;
        h1 = plotResiduals(mdl, 'fitted') 
        title([ 'Plots of residuals vs. fitted values' char(10) title_vs ' (' group_name ')' ], 'interpreter', 'none');
        out_filename = [ 'residuals_vs_fitted-' group_name '-' fieldname '.tiff' ];
        saveas(gcf, fullfile(SAVE_PATH, out_filename));  
        close;
        
        %% plot regression
        figure;
        format long
        b1 = x\y;

        yCalc1 = b1*x;
        scatter(x,y)
        hold on
%         plot(x,yCalc1)
        xlabel('MD')
        C = strsplit(fieldname,'_');
        ylabel_ = [ C(2) ];
        if strcmp(ylabel_, 'csd')
            ylabel_ = 'CCA SD';
        elseif strcmp(ylabel_, 'cmean')
            ylabel_ = 'CCA Mean';
        elseif strcmp(ylabel_, 'sbr')
            ylabel_ = 'SBR';
        end
        ylabel(ylabel_, 'interpreter', 'none');
        title(['Linear Regression Relation' char(10) title_vs ' (' group_name ')'], 'interpreter', 'none');
        grid on

        X = [ones(length(x),1) x];
        b = X\y
        yCalc2 = X*b;
        h2 = plot(x,yCalc2,'-')
%         legend('Data','Slope','Slope & Intercept','Location','best');

        Rsq1 = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
        Rsq2 = 1 - sum((y - yCalc2).^2)/sum((y - mean(y)).^2)        
        out_filename = [ 'linearregression-' group_name '-' fieldname '.tiff' ];
        saveas(gcf, fullfile(SAVE_PATH, out_filename));  
        close;  
        
    end
end

%% per severity
pairs = struct;
pairs.Normal = {        { paramsnormal.SAPMD, paramsnormal.CCA_MEAN }, ...
                        { paramsnormal.SAPMD, paramsnormal.CCA_SD }, ...
                        { paramsnormal.SAPMD, paramsnormal.SBR } ...
                };
    
pairs.Mild = {          { paramsmild.SAPMD, paramsmild.CCA_MEAN }, ...
                        { paramsmild.SAPMD, paramsmild.CCA_SD }, ...
                        { paramsmild.SAPMD, paramsmild.SBR } ...
                };    
            
pairs.Moderate = {          { paramsmoderate.SAPMD, paramsmoderate.CCA_MEAN }, ...
                        { paramsmoderate.SAPMD, paramsmoderate.CCA_SD }, ...
                        { paramsmoderate.SAPMD, paramsmoderate.SBR } ...
                };
            
pairs.Severe = {          { paramssevere.SAPMD, paramssevere.CCA_MEAN }, ...
                        { paramssevere.SAPMD, paramssevere.CCA_SD }, ...
                        { paramssevere.SAPMD, paramssevere.SBR } ...
                };            
    
groups = fieldnames(pairs);
for g_idx=1:numel(groups)
    group_name = groups{g_idx};
    
    pairs_ = pairs.(group_name);
    for i=1:numel(pairs_)
        y = pairs_{i}{1};
        x = pairs_{i}{2};

        mdl = fitlm(x,y)
        if i==1
            title_vs = 'MD vs. CCA Mean';
            fieldname = 'md_cmean';
        elseif i==2
            title_vs = 'MD vs. CCA SD';
            fieldname = 'md_csd';
        elseif i==3
            title_vs = 'MD vs. SBR';
            fieldname = 'md_sbr';
        end
%         r_sq_ord.(group_name).(fieldname) = mdl.Rsquared.Ordinary;
%         r_sq_adj.(group_name).(fieldname) = mdl.Rsquared.Adjusted;    
        result.(group_name).(fieldname).r_sq_ord = mdl.Rsquared.Ordinary;
        result.(group_name).(fieldname).r_sq_adj = mdl.Rsquared.Adjusted;
        result.(group_name).(fieldname).sse = mdl.SSE;
        result.(group_name).(fieldname).ssr = mdl.SSR;
        result.(group_name).(fieldname).sst = mdl.SST;
        result.(group_name).(fieldname).mse = mdl.MSE;
        result.(group_name).(fieldname).rmse = mdl.RMSE;
        result.(group_name).(fieldname).pval = mdl.Coefficients.pValue(2);
%         ano = anova(mdl,'summary')

%         figure;
%         h = plotResiduals(mdl, 'probability')
        
        figure;
        h1 = plotResiduals(mdl, 'fitted') 
        title([ 'Plots of residuals vs. fitted values' char(10) title_vs ' (' group_name ')' ], 'interpreter', 'none');
        out_filename = [ 'residuals_vs_fitted-' group_name '-' fieldname '.tiff' ];
        saveas(gcf, fullfile(SAVE_PATH, out_filename));  
        close;
        
        %% plot regression
        figure;
        format long
        b1 = x\y;

        yCalc1 = b1*x;
        scatter(x,y)
        hold on
%         plot(x,yCalc1)
        xlabel('MD')
        C = strsplit(fieldname,'_');
        ylabel_ = [ C(2) ];
        if strcmp(ylabel_, 'csd')
            ylabel_ = 'CCA SD';
        elseif strcmp(ylabel_, 'cmean')
            ylabel_ = 'CCA Mean';
        elseif strcmp(ylabel_, 'sbr')
            ylabel_ = 'SBR';
        end
        ylabel(ylabel_, 'interpreter', 'none');
        title(['Linear Regression Relation' char(10) title_vs ' (' group_name ')'], 'interpreter', 'none');
        grid on

        X = [ones(length(x),1) x];
        b = X\y
        yCalc2 = X*b;
        h2 = plot(x,yCalc2,'-')
%         legend('Data','Slope','Slope & Intercept','Location','best');

        Rsq1 = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
        Rsq2 = 1 - sum((y - yCalc2).^2)/sum((y - mean(y)).^2)        
        out_filename = [ 'linearregression-' group_name '-' fieldname '.tiff' ];
        saveas(gcf, fullfile(SAVE_PATH, out_filename));  
        close;  
        
    end
end





%%
y = paramsnormal.SAPMD;
x = paramsnormal.SBR;
mdl = fitlm(x,y)
r_sq_ord.md_sbr = mdl.Rsquared.Ordinary;
r_sq_adj.md_sbr = mdl.Rsquared.Adjusted;

ano = anova(mdl,'summary')

%% plot regression graph
figure;
format long
b1 = x\y;

yCalc1 = b1*x;
scatter(x,y)
hold on
plot(x,yCalc1)
xlabel('Population of state')
ylabel('Fatal traffic accidents per state')
title('Linear Regression Relation Between Accidents & Population')
grid on

X = [ones(length(x),1) x];
b = X\y
yCalc2 = X*b;
plot(x,yCalc2,'--')
legend('Data','Slope','Slope & Intercept','Location','best');

Rsq1 = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
Rsq2 = 1 - sum((y - yCalc2).^2)/sum((y - mean(y)).^2)


