function data = proc_rest_offline(set_filepath)
%% example code for running simulated online REST
% buffer blocksize = srate/10 without overlapping
% restoredefaultpath
% RESTpath = '/home/yuan/Documents/Tool/REST-git/';
% eeglab % to load any xdf plugins
RESTpath = 'C:\Users\AE\Documents\Bitbucket\mfssvep-eeg-duke\REST-blackbox';
current_path = pwd;
cd(RESTpath);

% Root folder for .xdf
EEG_DATASET_ROOT = fullfile('..', 'dataset', 'mfssvep-v2', 'sample');    

% Sample mfSSVEP XDF file for analysis
EEG_DATASET_FILEPATH = fullfile(EEG_DATASET_ROOT, 'DK0006-10_12_1947-mfssvep-left-1.set');
EEG_CALIB_FILEPATH = fullfile(EEG_DATASET_ROOT, 'DK0006-10_12_1947-alpha.set');

%% pipeline
% FIR -> ASR -> ORICA -> eyeCatch (IC classification) -> Reproject non-eye
% ICs
pipeline_desc = { ...
'fir', {'fspec', [0.5000 1 50 55], 'stopripple', -40, ...
    'normalize_amplitude', 0}, ...
'repair_bursts', {'stddev_cutoff', 20, 'window_len', 0.5}, ...
'orica', {'onlineWhitening', {'blockSize', 16}, 'adaptiveFF', {'constant', 'tau_const', 3},... 
    'options', {'blockSize', 16}, 'evalConvergence', 0.01}, ...
% 'eyecatch', {'cutoff', 0.8} ...
% 'ica_reproject', {'ProjectionMatrix', '.icawinv', 'ComponentSubset', '.reject'}, ...
};
pipe = [{'FilterOrdering', strcat('flt_', pipeline_desc(1:2:end))}, pipeline_desc];


%% load mfSSVEP EEG data
% eeg_path = 'eeg_data_path/';
% eeg_path = pop_loadxdf(EEG_DATASET_FILEPATH);

% calib = 'calibration_data_path/'; % or you can select time range in eeg data for calibration
% calib = pop_loadxdf(EEG_CALIB_FILEPATH);

% buffer = REST_offline(eeg_path, calib, pipe);
buffer = REST_offline(EEG_DATASET_FILEPATH, EEG_CALIB_FILEPATH, pipe);
% buffer = REST_offline(set_filepath, set_filepath, pipe);

% cd(current_path);
disp('REST offline Done')

end