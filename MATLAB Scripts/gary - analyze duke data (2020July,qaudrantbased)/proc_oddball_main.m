% Based on Prof. Jung discussion over Skype where he show me live eeglab tutorial
% Jan 29, 2020

clear all; clc;

DATASET_ROOTDIR = 'c:\Recordings\visual-oddball\USE THIS DATA'
DATASET_ROOTDIR = 'c:\Recordings\visual-oddball-feb21'

%Scripts below compare the ERPs under two conditions
sbj_list = { 'sbj0' };
oddball_list = { 'odd1', 'odd2' };

channelNameList = {'CH1', 'CH2', 'O1', 'PO1', 'PO2', 'Pz', 'Oz', 'O2' };

for i=1:length(sbj_list)
    for j=1:length(oddball_list)
        xdf_filepath = fullfile(DATASET_ROOTDIR, sbj_list{i}, [ sbj_list{i} '_' oddball_list{j} '.xdf'])    

        % SAVE SET
        out_filedir = fullfile(DATASET_ROOTDIR, 'Figures', sbj_list{i}, oddball_list{j});     
        if ~exist(out_filedir,'dir') mkdir(out_filedir); end;

        for ev_idx=1:2 % target and non-target
            EEG = pop_loadxdf(xdf_filepath, 'streamtype', 'EEG', 'exclude_markerstreams', {});
            for chidx=1:8
                EEG.chanlocs(chidx).labels= channelNameList{chidx};
            end        

            EEG.setname='ERP1';
            EEG = eeg_checkset( EEG );
            EEG = pop_select( EEG, 'channel',{'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'});
            EEG = eeg_checkset( EEG );
            EEG = pop_eegfiltnew(EEG, 'locutoff',1,'hicutoff',40);
            EEG.setname='ERP1 BP';
            EEG = eeg_checkset( EEG );
            EEG = clean_artifacts(EEG, 'FlatlineCriterion','off','ChannelCriterion','off','LineNoiseCriterion','off','Highpass','off','BurstCriterion',20,'WindowCriterion','off','BurstRejection','off','Distance','Euclidian');
            EEG.setname='ERP1 BP ASR';
            EEG = eeg_checkset( EEG );
            % pop_eegplot( EEG, 1, 1, 1);

            if ev_idx == 1 % target
                event='red';
                event_type='TARGET';
            else
                event='blue';
                event_type='NONTARGET';
            end
            EEG = pop_epoch( EEG, {  event  }, [-0.5           1], 'newname', ['ERP1 BP ASR ' event_type], 'epochinfo', 'yes');
            EEG = eeg_checkset( EEG );
            EEG = pop_rmbase( EEG, [-200 0] ,[]);
            EEG.setname=['ERP1 BP ASR ' event_type ' BASELINE'];
            EEG = eeg_checkset( EEG );
            EEG = eeg_checkset( EEG );
            EEG = eeg_checkset( EEG );     

            % save
            out = fullfile(DATASET_ROOTDIR, sbj_list{i});
            EEG = pop_saveset( EEG, 'filename',[ sbj_list{i} '_' oddball_list{j} '_erp1-' event_type '.set'],'filepath',out);
            EEG = eeg_checkset( EEG );    
        end 

        %% Jung - compare erp
        % load dataset
        [ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
        
        % TARGET
        EEG = pop_loadset('filename', [ sbj_list{i} '_' oddball_list{j} '_erp1-TARGET.set' ],'filepath', fullfile(DATASET_ROOTDIR, sbj_list{i}));
        EEG = eeg_checkset( EEG );
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0); 

        % NONTARGET
        EEG = pop_loadset('filename',[ sbj_list{i} '_' oddball_list{j} '_erp1-NONTARGET.set' ],'filepath', fullfile(DATASET_ROOTDIR, sbj_list{i}));
        EEG = eeg_checkset( EEG );
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1); 

        % pop_comperp uses dataset index in ALLEEG
        [erp1 erp2 erpsub time sig] = pop_comperp( ALLEEG, 1,1,2,'addavg','on','addstd','off','subavg','on','diffavg','off','diffstd','off','lowpass',10,'tplotopt',{'ydir' 1});

        out_erp_filename = [ sbj_list{i} '-erp-1' ];
        out_erp_filepath = fullfile(out_filedir, out_erp_filename);  
        util_savefig(out_erp_filepath);

        % plot individual channel erp Target and Non-Target
        for chan=1:6
            figure
            plot(time(151:end),[erp1(chan,151:end)' erp2(chan,151:end)']);grid;    
            title([EEG.chanlocs(chan).labels])
            %axis([-0.2 1 -10 10])
            legend('Targets','Non-targets')
            xlabel('Time (s)')
            ylabel('uV')    

            out_erp_filename = [ sbj_list{i} '-erp-' EEG.chanlocs(chan).labels ];
            out_erp_filepath = fullfile(out_filedir, out_erp_filename);  
            util_savefig(out_erp_filepath); 
        end
        
        % plot ERP Image of TARGET, sorted by latency, 'right' event type
        for chan=1:6
            figure; pop_erpimage(ALLEEG(1),1, [chan],[[]], [ sbj_list{i} '-erpimage-' EEG.chanlocs(chan).labels ],10,1,{ 'right'},[],'latency' ,'yerplabel','\muV','erp','on','cbar','on','topo', { [chan] EEG.chanlocs EEG.chaninfo } );
            out_erp_filename = [ sbj_list{i} '-erpimage-' EEG.chanlocs(chan).labels ];
            out_erp_filepath = fullfile(out_filedir, out_erp_filename);  
            util_savefig(out_erp_filepath); 
        end
        
        
        eeglab redraw;
        figure
        for chan=1:6
            subplot(2,3,chan);plot(time(151:end),[erp1(chan,151:end)' erp2(chan,151:end)']);grid;    
            title([EEG.chanlocs(chan).labels])
            %axis([-0.2 1 -10 10])
        end
        legend('Targets','Non-targets')
        xlabel('Time (s)')
        ylabel('uV')
        figure(gcf)

        out_erp_filename = [ sbj_list{i} '-erp-2' ];
        out_erp_filepath = fullfile(out_filedir, out_erp_filename);
        util_savefig(out_erp_filepath); 
    end
end

disp(['All done!']);


%% JUNG ORI code sent by email
% [erp1 erp2 erpsub time sig] = pop_comperp( ALLEEG, 1,6,8,'addavg','on','addstd','off','subavg','on','diffavg','off','diffstd','off','lowpass',10,'tplotopt',{'ydir' 1});
% eeglab redraw;
% figure
% for chan=1:6,
%     subplot(2,3,chan);plot(time(151:end),[erp1(chan,151:end)' erp2(chan,151:end)']);grid;    
%     title([EEG.chanlocs(chan).labels])
%     %axis([-0.2 1 -10 10])
% end
% legend('Targets','Non-targets')
% xlabel('Time (s)')
% ylabel('uV')
% figure(gcf)