function concat_data = concat_epoch(data)

    concat_data = zeros(size(data,1), size(data,2) * size(data,3), 1); % ch x data x epoch
    
    for ch=1:size(data,1)
        concat_data_ = [];
        for i=1:size(data,3)
            concat_data_ = horzcat(concat_data_, data(ch, :, i));
        end
        
        concat_data(ch,:) = concat_data_;
    end
end


% A = [1 2 3];
% B = [4 5 6];
% horzcat(A,B);