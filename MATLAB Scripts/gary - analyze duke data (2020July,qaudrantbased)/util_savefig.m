function util_savefig(out_filepath)
    [pathstr,name,ext] = fileparts(out_filepath);
    if ~exist(pathstr,'dir') mkdir(pathstr); end;
    
    disp(['Saving figure=' out_filepath '.png']);
    print('-dtiff','-r200', [out_filepath '.png']);
    saveas(gcf, [out_filepath '.fig']);
%     close;
end

