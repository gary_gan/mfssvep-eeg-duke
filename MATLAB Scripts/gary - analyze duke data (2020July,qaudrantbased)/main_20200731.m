clear all;close all;clc;

% TAG = 'severe_bp6-25'; % used in struct output
% TAG = 'normal_all_cca_preproc_rest_concat';

TAG = '20191202 - moderate_handpick_session_all_cca_preproc_masaki_noconcat-nw2-e138-snrarea-perepoch-baselinenw5-nofig';
TAG = '20200217 - moderate_handpick_session_all_cca_preproc_masaki_noconcat-nw2-e138-snrarea-perepoch-baselinenw5-nofig';
% TAG = 'tmp';
TAG = '20200316-moderate';
GROUP = 'moderate'; % normal, severe
CONCAT = 0;
PREPROCESSING_PIPELINE = 'masaki' % rest_offline, masaki, gary
ICA = 0;

CONVERT = 0; % convert .xdf to .set

%% inputs
% cd('E:\_CerebraTek\_ngoggle kist\_Script')
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(process)\';
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\test single\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(Healthy)\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch (with problem)\'

% FILE_PATH = 'c:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\'
% FILE_PATH = 'c:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test'
% FILE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\_filtered_complete_mfssvep1and2'
% FILE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\_filtered_complete_mf1and2\DK0001\20181107';
% FILE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\testetstst\DK0002\';

% FILE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\_filtered_complete_mfssvep1and2_bygroup\no'

% SAVE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\_filtered_complete_mfssvep1and2_bygroup\no'

FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal'
% FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\1. mild'
% FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\3. severe'
% FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\99. sample';
% FILE_PATH = 'c:\Users\AE\Documents\Bitbucket\mfssvep-eeg-duke\dataset\mfssvep-v2\sample';
% FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered\';
% FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal\DK0006\20181205';

FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\98. convertsample';

FILE_PATH='e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\3. severe - handpick';


if strcmp(GROUP, 'severe')
    FILE_PATH='e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\3. severe - handpick'
    FILE_PATH='e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\3. severe'
    FILE_PATH = 'e:\_MFSSVEP DUKE\_category\3. Severe'
    

elseif strcmp(GROUP, 'normal')
    FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal - handpick'    
    FILE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal' 
    FILE_PATH = 'e:\_MFSSVEP DUKE\_category\0. Normal'
    
elseif strcmp(GROUP, 'mild')
    FILE_PATH = 'e:\_MFSSVEP DUKE\_category\1. Mild'

elseif strcmp(GROUP, 'moderate')
    FILE_PATH = 'e:\_MFSSVEP DUKE\_category\2. Moderate'
    
    
end


% FILE_PATH = 'e:\_MFSSVEP DUKE\_category\X. Test'
TAG = '20200731-run1';
FILE_PATH = 'c:\Recordings\sub-Gary\ses-S001_right\eeg\';


SAVE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output'
SAVE_PATH = 'e:\_MFSSVEP DUKE\_category\output_cleanscripttest'
SAVE_PATH = FILE_PATH;
% SAVE_PATH = 'e:\_MFSSVEP DUKE\output'

%%
GROUP_NORMAL =      ['DK0013', 'DK0015', 'DK0033'];
GROUP_MILD =        ['DK0011', 'DK0012', 'DK0021'];
GROUP_MODERATE =    ['DK0008', 'DK0050', 'DK0053'];
GROUP_SEVERE =      ['DK0004', 'DK0014', 'DK0039'];

setting.SAVE_PATH_ROOT = fullfile(SAVE_PATH, TAG);
if ~exist(setting.SAVE_PATH_ROOT,'dir') mkdir(setting.SAVE_PATH_ROOT); end;

%% Settings
setting.seg_len = 10;
setting.overlap = 0;
setting.srate = 500;
setting.NW = [2 : 1 : 4]; % Time-bandwidth product 'NW' should be greater than or equal to 1.25 when 'Droplasttaper' is true.
setting.NW = 2;
setting.RMBASE_flag = true;   % apply remove baseline filter or not
setting.BP_flag = true;      % apply bandpass filter or not
setting.ASR_flag = true;     % apply ASR algorithm or not
setting.BPLE = 1;           % bandpass low edge
setting.BPHE = 35;          % bandpass high edge
% setting.chlabel = {'EOG1', 'O1', 'P5', 'POz', 'P6', 'CPz', 'EOG2', 'O2'};
% Gear VR Set
% setting.chlabel = {'O1', 'EOG-L', 'POz', 'PO5', 'CPz', 'PO6', 'O2', 'EOG-R'};
% duke eye center OSVR set
setting.chlabel = {'CH1', 'CH2', 'PO1', 'O1', 'Pz', 'O2', 'Oz', 'PO2'};

FREQUENCY_LIST = [8, 9, 10, 11];
pattern_list = {'11', '12', '13', '14', '15', '16'};
pattern_list = {'1_11', '1_12', '1_13', '1_14', '2_11'};
pattern_list = {};
NUM_TOTAL_SESSION = 8;
pattern_id = {'11', '12', '13', '14'};


JOIN_SESSION = 1;
if JOIN_SESSION == 1 % combine all epochs in 8 sessions into 24, e.g. 6 x 2500 x 24
    pattern_list = {   {'1_11', '2_11', '3_11', '4_11', '5_11', '6_11', '7_11', '8_11'}, ...
                        {'1_12', '2_12', '3_12', '4_12', '5_12', '6_12', '7_12', '8_12'}, ...
                        {'1_13', '2_13', '3_13', '4_13', '5_13', '6_13', '7_13', '8_13'}, ...
                        {'1_14', '2_14', '3_14', '4_14', '5_14', '6_14', '7_14', '8_14'} ...
                        };
else
    for i=1:NUM_TOTAL_SESSION
        for j=1:length(pattern_id)
            val_ = [ num2str(i) '_' pattern_id(j) ];
%             pattern_list{end+1} = [ val_{:} ];
            pattern_list{end+1} = { [val_{:}] };
        end
    end
end

% EPOCH_START_DELAY = 0;
% EPOCH_END_MAX = 0.5; % seconds
% EPOCH_STEP_MS = 0.05;
% EPOCH_DELAY_MAX = 0.1; % seconds, shift until 0.5s
EPOCH_START = [138];

out_dir = setting.SAVE_PATH_ROOT;

exp = [];

%% Load XDF and convert to .SET
FILE_LIST = get_all_files(FILE_PATH);
xdf_files_invalid = [];
xdf_files_missing_events = [];

disp([ 'Total files found: ' num2str(length(FILE_LIST))]);
for i =1:length(FILE_LIST)
    filepath = FILE_LIST{i};  
    [pathstr,filename,ext] = fileparts(filepath);   

    %% convert to set if .set not exist (REST offline need this)
    try
        expression = '.*.xdf'; 
        if regexp(filepath,expression) 
            % only convert if the .set for the xdf is not found
            set_filepath = fullfile(pathstr, [ filename '.set' ]);
            if ~isfile(set_filepath)
                convert_xdf_to_set(filepath);   
            end
        end
    catch exp
        exp.filepath = FILE_LIST{i};
        exp.event_id = pattern_list{p_idx};
        xdf_files_missing_events{end+1} = exp;
    end
end

%% new var
result = struct();
is_plot_baseline = 0;

%% Begin processing mfssvep recordings
for i =1:length(FILE_LIST)
    filepath = FILE_LIST{i};
    [pathstr,filename,ext] = fileparts(filepath);   
%     expression = '.*mfssvep-.*all.set'; % only process file with mfssvep inside name
    expression = '.*mfssvep-.*[1-2].set'; % only process file with mfssvep inside name

    if regexp(FILE_LIST{i},expression)    
        C = strsplit(filename,'-');
        subject = cell2mat([ C(1) '-' C(2) ]);
        baseline_set_filename = cell2mat([ C(1) '-' C(2) '-rest.set'] );        
        baseline_set_filepath = fullfile(pathstr, baseline_set_filename);
        
        %% plot baseline
        if (is_plot_baseline)
            % use alpha if rest not found
            if ~isfile(baseline_set_filepath)
                baseline_set_filename = cell2mat([ C(1) '-' C(2) '-alpha.set'] );  
                baseline_set_filepath = fullfile(pathstr, baseline_set_filename);
            end
            EEG_baseline = pop_loadset(baseline_set_filepath);
            EEG_baseline.subject = subject;
            EEG_baseline = preproc_baseline(setting, EEG_baseline, baseline_set_filepath, PREPROCESSING_PIPELINE, 0, 0);
            % just extract the 15s
            epoch_range_baseline = [10 25];
            EEG_baseline = pop_select( EEG_baseline, 'time', epoch_range_baseline );
            EEG_baseline.data = EEG_baseline.data(:,1:end-1);

            % Plot baseline PSD
            out_filename = [ baseline_set_filename(1:end-4) ];
            plot_psd (EEG_baseline.data, EEG_baseline.srate, out_dir, out_filename, 'nw', 2, 'plot_freq_range', [1 15], ...
                    'sbj_name', EEG_baseline.subject, 'title_suffix', 'Baseline', 'plot_marker', 0, 'legend', 'Baseline', 'dir_suffix', 'baseline');
        end
        
        %% extract diff epoch (event id 11-16) for each file
        for p_idx=1:length(pattern_list)
            pattern_id = pattern_list{p_idx};
                      
            %% load .set for each pattern
            set_filename = [ filename '.set' ];
            set_filepath = fullfile(pathstr, set_filename)
            EEG = pop_loadset(set_filepath);                
            
            % Add metadata to the EEG struct from filename
            [~,filename,ext] = fileparts(set_filename);   
            C = strsplit(filename,'-');
            EEG.subject = cell2mat([ C(1) '-' C(2) ]);
            EEG.session = cell2mat([ C(3) '-' C(4) '-' C(5) ]);
                     
            epoch_start = 0; % reset the epoch start 
            
            for e_idx=1:length(EPOCH_START)
                try
                    epoch_start = (EPOCH_START(e_idx)/1000.0); %convert to seconds
                    epoch_end = epoch_start + 5;       
                    % Extract EEG per epoch
                    if CONCAT == 1 % combine all epochs into single epoch, e.g. 6x60000
                        pattern_id = {'1_11', '2_11', '3_11', '4_11', '5_11', '6_11', '7_11', '8_11'};
                        EEG = preproc(setting, pattern_id, EEG, FILE_LIST{i}, PREPROCESSING_PIPELINE, CONCAT, ICA, [epoch_start epoch_end]);
                    else
                        EEG = preproc(setting, pattern_id, EEG, FILE_LIST{i}, PREPROCESSING_PIPELINE, CONCAT, ICA, [epoch_start epoch_end]);              
                    end
                 
                    for nw_idx=1:length(setting.NW)
                        nw = setting.NW(nw_idx);                       
                        
%                         concat_epoch = concat_epoch(EEG.data);
                        %% Phase 1 - Enhance
                        fieldname_sbj = strrep(EEG.subject, '-', '_');
                        fieldname_pattern = [ 'p_' pattern_id ]; 
                        
                        % draw psd for all channels
                        if (JOIN_SESSION == 1)
                            out_filename = [ EEG.subject '_' EEG.session '_p11(all_session)_e[' ']_psd_allchannels' ];                            
                        else
                            out_filename = [ EEG.subject '_' EEG.session '_p' num2str(pattern_id), '_e[' ']_psd_allchannels' ];                            
                        end
                        draw_psd(EEG.data, 500, fullfile(out_dir, 'Phase 1 - PSD (All Channels)'), out_filename, ['Gary Raw Channel PSD'], 'nw', 2, 'is_show_iqr', 1);
                        
                        % do CCASim
                        ref_freq=9; % to be removed
                        [result.(fieldname_sbj).enc_ccasim.(fieldname_pattern).cca_max_corr, result.(fieldname_sbj).enc_ccasim.(fieldname_pattern).cca_signal] = do_cca(EEG.data, ref_freq, 'pulse_trend');
                        out_filename = [ EEG.subject '_' EEG.session '_p' num2str(pattern_id), '_e[' ']_psd_ccachannel' ];                            
                        draw_psd(result.(fieldname_sbj).enc_cca.(fieldname_pattern).(fieldname_freq).cca_signal, 500, fullfile(out_dir, 'Phase 1 - PSD (CCA)'), out_filename, ['Gary CCA PSD'], 'nw', 2, 'is_show_iqr', 1);
                        
                        % do CCAQuad
                        
                        % do TRCA
                        
                        
                        
                        %% Phase 2 - Feature Extraction
                        %% iterate frequency
                        for f = 1:length(FREQUENCY_LIST)
                            ref_freq = FREQUENCY_LIST(f);                            
                            fprintf('CCA, ref signal=%dHz\n', ref_freq);
                            out_filename = [ EEG.subject '_' EEG.session '_p' num2str(pattern_id), '_e[' ']_f' num2str(ref_freq) ];
                            
                            fieldname_sbj = strrep(EEG.subject, '-', '_');
                            fieldname_pattern = [ 'p_' pattern_id ]; 
                            fieldname_freq = [ 'f_' num2str(ref_freq) ];
                            
                            % draw psd for all channels
                            out_filename = [ EEG.subject '_' EEG.session '_p' num2str(pattern_id), '_e[' ']_f' num2str(ref_freq) '_psd_allchannels' ];                            
                            draw_psd(EEG.data, 500, fullfile(out_dir, 'PSD (All Channels)'), out_filename, ['Gary Raw Channel PSD'], 'nw', 2, 'is_show_iqr', 1);
                            
                            % do cca
                            [result.(fieldname_sbj).enc_cca.(fieldname_pattern).(fieldname_freq).cca_max_corr, result.(fieldname_sbj).enc_cca.(fieldname_pattern).(fieldname_freq).cca_signal] = do_cca(EEG.data, ref_freq), 'normal';
                            % draw psd for just cca
                            out_filename = [ EEG.subject '_' EEG.session '_p' num2str(pattern_id), '_e[' ']_f' num2str(ref_freq) '_psd_ccachannel' ];                            
                            draw_psd(result.(fieldname_sbj).enc_cca.(fieldname_pattern).(fieldname_freq).cca_signal, 500, fullfile(out_dir, 'PSD (CCA)'), out_filename, ['Gary CCA PSD'], 'nw', 2, 'is_show_iqr', 1);

                            
%                             [cca_rel_avg] = cca_psd_of_all_ch_cleaned(EEG, ref_freq, out_dir, out_filename, "nw", nw, "plot_freq_range", [1 30], ...
%                                                                                             'event_id', pattern_id, 'epoch_baseline', EEG_baseline.data );                                                                                         
%                             [snr_area, snr_db] = get_snr_from_psd(EEG.data, EEG_baseline.data, 500, ref_freq, nw);
                        
                       
                            %% only write cca struct if nw=2
                            if nw == 21
                                filepath = FILE_LIST{i};
                                [pathstr,name,ext] = fileparts(filepath);
                                meta_filename = strrep(name, '-', '_');

                                C = strsplit(name,'-');
                                meta_eye_direction = char(C(4));
                                meta_pattern = pattern_list{p_idx};
% 
                                % cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).cca_max = cca_rel_avg;
                                cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).cca.(strcat('f', num2str(ref_freq))) = cca_rel_avg;

                                % write SNR
                                % cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).snr = cca_sideband_snr;
    %                             cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).cca_snr_sideband.(strcat('nw', num2str(nw))) = cca_sideband_snr;
                                cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).snr_area.(strcat('nw', num2str(nw))) = snr_area;                            
                                cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).snr_db.(strcat('nw', num2str(nw))) = snr_db;   
                                cca_rel_avg_struct;

                                output_name = fullfile(setting.SAVE_PATH_ROOT, ['cca_avg_fstruct_' TAG]);
                                save(output_name,'cca_rel_avg_struct');
                            end       
                        end
                        
                        
                        %% ica
                        if ~ICA
                            continue;
                        end
                        
%                         EEG_ori = EEG;
                        
                        %% Plot IC PSD
                       
                        brain_ic_prob = EEG.etc.ic_classification.ICLabel.classifications(:, 1) % brain IC prob
                        [val, idx] = max(EEG.etc.ic_classification.ICLabel.classifications(:, 1)); % highest brain IC prob
                        
                        EEG_icaact_prime = EEG.icaact .* brain_ic_prob; % element wise mult
                        EEG_projected = EEG_icaact_prime' * EEG.icawinv;   
                        
                        EEG_brainic = zeros(size(EEG.icaact,1), size(EEG.icaact,2)); % chnum x total samples
                        EEG_brainic(idx,:) = EEG.icaact(idx,:);
                        EEG_brain_ic_tmp = EEG_brainic' * EEG.icawinv;
                        
                        EEG.etc.ic_classification.ICLabel.classifications

                        % find which IC has the highest value in 'Brain' label
                        for ic_class_idx=1:1 %     {'Brain'}    {'Muscle'}    {'Eye'}    {'Heart'}    {'Line Noise'}    {'Channel Noise'}    {'Other'}
                            [val, idx] = max(EEG.etc.ic_classification.ICLabel.classifications(:, ic_class_idx));
%                             ic_max_brain = EEG.icaact(idx,:);    

%                             epoch = EEG.data;
                            EEG.data = EEG_projected';
%                             epoch = EEG_brain_ic_tmp';
                            epoch = EEG.data;

                            ic_class = EEG.etc.ic_classification.ICLabel.classes(ic_class_idx);
                            ic_prob = val;
                            
                            cca_rel_avg_ica = [];
                            for f = 1:length(FREQUENCY_LIST)
                                ref_freq = FREQUENCY_LIST(f);
                                
                                
                                
                                
                                % feed in before multiplication with
                                % brain_prob
                                cca_rel_avg_ica(end+1) = cca_psd_ica(setting,set_filepath,epoch,ref_freq,nw,ic_class,ic_prob,'allic');
                                
                                % plot just the cca psd of the brain ic
                                epoch = EEG.data(idx,:);
                                cca_psd_ica(setting,set_filepath,epoch,ref_freq,nw,ic_class,ic_prob,'justbrainic');
                            end
 
                            %% only write cca struct if nw=2
                            if nw == 2
                                filepath = FILE_LIST{i};
                                [pathstr,name,ext] = fileparts(filepath);
                                meta_filename = strrep(name, '-', '_');

                                C = strsplit(name,'-');
                                meta_eye_direction = char(C(4));
                                meta_pattern = pattern_list{p_idx};

                                % cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).cca_max = cca_rel_avg;
                                cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).cca_ica = cca_rel_avg_ica;
            %                     cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).snr = cca_sideband_snr;
                                cca_rel_avg_struct;

                                output_name = [setting.SAVE_PATH_ROOT '\cca_avg_fstruct_' TAG];
                                save(output_name,'cca_rel_avg_struct');
                            end                             
                            
                        end

                                             
                        
                        
                        
    %                     cca_rel_avg = proc_vep_concat(setting, FILE_LIST{i});%vep10s

    %                     cca_all = [cca_all cca_rel_avg];

    
%                         %% only write cca struct if nw=2
%                         if nw == 21
%                             filepath = FILE_LIST{i};
%                             [pathstr,name,ext] = fileparts(filepath);
%                             meta_filename = strrep(name, '-', '_');
% 
%                             C = strsplit(name,'-');
%                             meta_eye_direction = char(C(4));
%                             meta_pattern = pattern_list{p_idx};
% 
%                             % cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).cca_max = cca_rel_avg;
%                             cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).cca = cca_rel_avg;
%         %                     cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))).snr = cca_sideband_snr;
%                             cca_rel_avg_struct;
% 
%                             output_name = [setting.SAVE_PATH_ROOT '\cca_avg_fstruct_' TAG];
%                             save(output_name,'cca_rel_avg_struct');
%                         end
                    end
                catch exception
                    exp{end+1} = exception;
                end
            end
        end
    end
end

disp('All Done!');

disp('Creating summary...');
summary_stuct_filepath = fullfile(setting.SAVE_PATH_ROOT, ['cca_avg_fstruct_' TAG '.mat']);
create_summary(load(summary_stuct_filepath).cca_rel_avg_struct, setting.SAVE_PATH_ROOT);
disp('All Done!');

% for i =1:length(FILE_LIST)
%     if regexp(char(FILE_LIST(i)),'frameonset')
%         continue
%     end
%     if regexp(char(FILE_LIST(i)),'Alpha')
%         proc_alpha(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     end
%     
%     expression = '.*VEP.*';
%     if regexp(char(FILE_LIST(i)),expression)
%         proc_vep(setting,[FILE_PATH '\' char(FILE_LIST(i))]);%vep10s
%         setting.NW = 3;
%         proc_vep2s(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%         
%     elseif regexp(char(FILE_LIST(i)),'.*Supervised.*')
%         proc_caseSUP(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     else
%         proc_other(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     end
% end
% set(gcf,'visible','on');close;

