%% Modified 2020-07-31


function draw_psd(data, srate, out_dir, out_filename, fig_title, varargin)
    %% Input Parser
    p = inputParser;
    addRequired(p, 'data');
    addRequired(p, 'srate', @isnumeric);
    addRequired(p, 'out_dir', @ischar);
    addRequired(p, 'out_filename', @ischar);
    addRequired(p, 'fig_title', @ischar);
    
    addParameter(p, 'nw', 3, @isnumeric);
    addParameter(p, 'is_show_iqr', 0, @isnumeric);
    parse(p,data,srate,out_dir,out_filename,fig_title,varargin{:});
    p.Results
    
    %% local var use
    srate = p.Results.srate
    out_dir = p.Results.out_dir;
    out_filename = p.Results.out_filename;
    fig_title = p.Results.fig_title;
    nw = p.Results.nw;
    is_show_iqr = p.Results.is_show_iqr;

    SAVE_FIGURE = 1;
    
    %%
    figure; set(gcf,'visible','off')
    hold on;    
    
    for ch = 1:size(data,1)
        clear pxx f;
        for i = 1:size(data,3)
            [pxx(i,:),f] = pmtm(data(ch,:,i),nw,length(data(ch,:,i)),srate);
        end

        pxxdb = 10*log10(pxx);
        Q2 = median(pxxdb,1);

        % fill Q1 and Q3
        if (is_show_iqr)
            Q1 = prctile(pxxdb,25);
            Q3 = prctile(pxxdb,75);
            fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
        end
        plot(f,Q2,'color','r')    
    
    end
    
    if SAVE_FIGURE
        axis([5 50 -30 20]);
        grid on;set(gca,'fontsize',14)
        title(fig_title);
%         title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(size(epoch,3),'%d') ' epochs, Avg. CCA correlation:' num2str(mean_avg_r,'%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
        xlabel('Frequency (Hz)','fontsize',14);
        ylabel('Power (dB/Hz)','fontsize',14);
        xlim([ 5 35 ] );

    %     LG=legend(CH_LABEL([3:8]),'CCA result');
    %     set(LG,'location','northeastoutside')

        % SAVE_PATH = [pathstr '\Figures\p' num2str(s.pattern) '\CCA+PSD all6ch.( ' num2str(s.seg_len) 's seg)\'];
    %     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD all6ch.( ' num2str(s.seg_len) 's seg)'];
%         out_subdir = fullfile(out_dir, [ 'PSD for CCA ( NW=' num2str(nw) ' )' ]);

        if ~exist(out_dir,'dir') mkdir(out_dir); end;
%         out_filename = [ meta_filename '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
        % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])
        
        util_savefig(fullfile(out_dir, out_filename));
        close;
    end    

end