function [avg_r] = cca_psd_of_all_ch(s,filepath,epoch,ref_freq)

SAVE_FIGURE = s.SAVE_FIGURE;

% SAVE_PATH_ROOT = ['e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\Figures\' s.TAG];

% s=setting;
% [A,B,r,U,V] = canoncorr(X,Y)
[pathstr,name,ext] = fileparts(filepath);
meta_filename = name;
C = strsplit(name,'-');
% name = cell2mat([C(1) '-' C(4)]);
name = cell2mat([C(1) '-' C(2) '-' C(3) '-' C(4) '-' C(5)]);
name = strcat(name, ['-f' num2str(ref_freq,'%d')]);



CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

data = epoch;

color = {'b','r','r','g','k','m','c','c',[1 0.55 0]};
figure;set(gcf,'visible','off')

cca_signals = [];

for ch = [1:6 7]
    clear pxx f
    if ch == 7 % do cca
        avg_r = [];
        for i = 1:size(data,3)
%             freq = freq;
            [A, B, r, U, V, awx, ~] = CCA_normal(data(:, :, i)', ref_freq);
%             betas = cat(1, betas, B(:, 1)');
%             rel = cat(1, rel, r(1));
%             kkk = get_snr(awx(:, 1), f, FS);
%             SNR_ = cat(1, SNR_, kkk);
%             new_result(:, j) = awx(:, 1);  
%                         
            avg_r = [avg_r r(1,1)];
%             [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
            [pxx(i,:),f] = pmtm(U(:,1),NW,length(U(:,1)),SRATE);
            
            cca_signals = U(:,1);

        end
        mean_avg_r = mean(avg_r);
        
        % save cca output
        % output_name = strcat('C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_f', num2str(ref_freq))
        %output_name = ['C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_f' ref_freq]
        % cca_rel_all = avg_r;
        % save(output_name,'avg_r');
    else % ch 1-6
        for i = 1:size(data,3) % for each epoch / trial
            temp = squeeze(data(ch,:,i));
            [pxx(i,:),f] = pmtm(temp,NW,length(temp),SRATE);
        end
    end
    
    pxxdb = 10*log10(pxx);
    Q2 = median(pxxdb,1);
%     Q1 = prctile(pxxdb,25);
%     Q3 = prctile(pxxdb,75);


    %% synchronous average CCA signal
%     for i = 1:size(data,3)
%         temp = squeeze(data(ch,:,i));
%         [pxx(i,:),f] = pmtm(temp,NW,length(temp),SRATE);    
%     end
    
    %% calculate SNR
    % ...    
    t_freq_idx = find(f==ref_freq); % target freq index
    Psig = pxx(1, t_freq_idx);
    
    N_SIDEBAND_PTS = 2; % how many points in side band in each left/right.
    Pside = [];
    offset = t_freq_idx-1;
    for i=1:N_SIDEBAND_PTS % scan left
        offset = offset - 1;
       Pside(end+1) = pxx(1, offset);
    end
    
    offset = t_freq_idx+1;
    for i=1:N_SIDEBAND_PTS % scan right
        offset = offset + 1;
       Pside(end+1) = pxx(1, offset);
    end         
    
    PsideAvg = mean(Pside); 
    
    SNR = 10*log10(Psig/PsideAvg); % SNR of PSig / Pnoise
    
%     fprintf('SNR=%.4f\n', SNR);
%     disp('snr');
%% Plot the lines
    if s.SAVE_FIGURE
    % fill Q1 and Q3
    %     fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)


        hold on;
        plot(f,Q2,'color',color{ch})
    end
end

%% Plot
if s.SAVE_FIGURE
    axis([5 50 -30 20]);
    grid on;set(gca,'fontsize',14)
    title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(size(epoch,3),'%d') ' epochs, Avg. CCA correlation:' num2str(mean_avg_r,'%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);
    xlim([ 5 30] );

%     LG=legend(CH_LABEL([3:8]),'CCA result');
%     set(LG,'location','northeastoutside')

    % SAVE_PATH = [pathstr '\Figures\p' num2str(s.pattern) '\CCA+PSD all6ch.( ' num2str(s.seg_len) 's seg)\'];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD all6ch.( ' num2str(s.seg_len) 's seg)'];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ meta_filename '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);

    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;
end


%%
% expression = '\d+Hz';
% [startIndex,endIndex] = regexp(filepath,expression) ;
% freq=str2num(filepath(startIndex:endIndex-2));
% 
% t = (1:length(data))/SRATE;
% ref1 = sin(2*pi*freq*t);
% ref2 = cos(2*pi*freq*t);
% REF = [ref1; ref2];
% color = {'b','','r','g','k','m','c','',[1 0.55 0]};
% figure;set(gcf,'visible','off')
% for ch = [1 3:7 9]
%     clear pxx f
%     if ch == 9
%         avg_r = [];
%         for i = 1:size(data,3)
%             temp = squeeze(data([1 3:7],:,i));
%             clear A B r U V
%             [A,B,r,U,V] = canoncorr(temp',REF');
%             avg_r = [avg_r r(1,1)];
%             [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
%         end
%         avg_r = mean(avg_r);
%     else
%         for i = 1:size(data,3)
%             temp = squeeze(data(ch,:,i));
%             [pxx(i,:),f] = pmtm(temp,4,length(temp),SRATE);
%         end
%     end
%     
%     pxxdb = 10*log10(pxx);
%     Q2 = median(pxxdb,1);
%     
%     hold on;
%     plot(f,Q2,'color',color{ch})
% end
% axis([5 50 -30 20]);
% grid on;set(gca,'fontsize',14)
% title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of 10 epochs, Avg. CCA correlation:' num2str(avg_r,'%0.2f')],'fontsize',12,'interpreter','none');
% xlabel('Frequency (Hz)','fontsize',14);
% ylabel('Power (dB/Hz)','fontsize',14);
% 
% LG=legend(CH_LABEL([1 3:7]),'CCA result');
% set(LG,'location','northeastoutside')
% 
% SAVE_PATH = [pathstr '\Figures\CCA+PSD all7ch.( ' num2str(s.seg_len) 's seg)\'];
% if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
% print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])
% saveas(gcf,[SAVE_PATH '\' name '.fig'])
% close;

