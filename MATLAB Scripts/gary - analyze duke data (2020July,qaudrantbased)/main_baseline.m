% test baseline from mf

clear all;close all;clc;
% cd('E:\_CerebraTek\_ngoggle kist\_Script')
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(process)\';
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\test single\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(Healthy)\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch (with problem)\'

% FILE_PATH = 'c:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\'
FILE_PATH = 'c:\Recordings\sub-Gary\ses-S001_alpha\'


pattern_list = ['a', 'b'];
pattern = pattern_list(2);
setting.pattern = pattern;

setting.seg_len = 10;
setting.overlap = 0;
setting.srate = 500;
setting.NW = 4;
setting.RMBASE_flag = true;   % apply remove baseline filter or not
setting.BP_flag = true;      % apply bandpass filter or not
setting.ASR_flag = true;     % apply ASR algorithm or not
setting.BPLE = 1;           % bandpass low edge
setting.BPHE = 50;          % bandpass high edge
% setting.chlabel = {'EOG1', 'O1', 'P5', 'POz', 'P6', 'CPz', 'EOG2', 'O2'};
% Gear VR Set
% setting.chlabel = {'O1', 'EOG-L', 'POz', 'PO5', 'CPz', 'PO6', 'O2', 'EOG-R'};

% duke eye center OSVR set
% setting.chlabel = {'CH1', 'CH2', 'O1', 'PO1', 'PO2', 'Pz', 'Oz', 'O2'};
setting.chlabel = {'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'};
% FILE_LIST = dir([FILE_PATH '\*.csv']);
% FILE_LIST = {FILE_LIST.name};

% load XDF
FILE_LIST = get_all_files(FILE_PATH);

xdf_files_invalid = [];

% FILE_LIST = dir([FILE_PATH '\*.xdf']);

for i =1:length(FILE_LIST)
    filepath = FILE_LIST{i};
    expression = '.*.xdf'; % only process file with mfssvep inside name
    if regexp(FILE_LIST{i},expression) 
        EEG = convert_xdf_to_set(filepath);  
        
%         EEG = pop_epoch( EEG, {'Close'}, [0 60], 'epochinfo', 'yes');
        EEG = eeg_checkset (EEG); 
        
        
        proc_alpha_gary(setting, filepath, EEG);

    end
    
end

disp('end of proc alpha!');


set(gcf,'visible','on');close;

