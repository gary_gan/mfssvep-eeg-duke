SAVE_PATH = 'e:\_MFSSVEP DUKE\__share';
SAVE_PATH = 'e:\_MFSSVEP DUKE\__share\_share with alessandro\corr_matrix';

RESHAPE_SIZE = 107;    
paramsnormal_SAPMD_reshape = my_reshape(paramsnormal.SAPMD, RESHAPE_SIZE);
paramsglaucoma_SAPMD_reshape = my_reshape(paramsglaucomaall.SAPMD, RESHAPE_SIZE);

paramsnormal_CCA_MEAN_reshape = my_reshape(paramsnormal.CCA_MEAN, RESHAPE_SIZE);
paramsglaucoma_CCA_MEAN_reshape = my_reshape(paramsglaucomaall.CCA_MEAN, RESHAPE_SIZE);

paramsnormal_CCA_SD_reshape = my_reshape(paramsnormal.CCA_SD, RESHAPE_SIZE);
paramsglaucoma_CCA_SD_reshape = my_reshape(paramsglaucomaall.CCA_SD, RESHAPE_SIZE);

paramsnormal_SBR_reshape = my_reshape(paramsnormal.SBR, RESHAPE_SIZE);
paramsglaucoma_SBR_reshape = my_reshape(paramsglaucomaall.SBR, RESHAPE_SIZE);

T = table(paramsnormal_SAPMD_reshape, paramsglaucoma_SAPMD_reshape, ...
    paramsnormal_CCA_MEAN_reshape, paramsglaucoma_CCA_MEAN_reshape, ...
    paramsnormal_CCA_SD_reshape, paramsglaucoma_CCA_SD_reshape, ...
    paramsnormal_SBR_reshape, paramsglaucoma_SBR_reshape);

COL_NAMES = { 'MD_N', 'MD_G', ...
              'CM_N', 'CM_G', ...
              'CSD_N', 'CSD_G', ...
              'SBR_N', 'SBR_G' ...
              };  
          
T.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T, 'testR','on');          
          


%% plot corr for NORMAL
T2 = table(paramsnormal_SAPMD_reshape, paramsnormal_CCA_MEAN_reshape, paramsnormal_CCA_SD_reshape, paramsnormal_SBR_reshape);

COL_NAMES = { 'MD_N', 'CM_N', 'CSD_N', 'SBR_N' };
          
T2.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T2, 'testR','on');   
out_filename = [ 'corr_mat_normal.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

%% plot corr for GLAUCOMA
T2 = table(paramsglaucoma_SAPMD_reshape, paramsglaucoma_CCA_MEAN_reshape, paramsglaucoma_CCA_SD_reshape, paramsglaucoma_SBR_reshape);

COL_NAMES = { 'MD_G', 'CM_G', 'CSD_G', 'SBR_G' };
          
T2.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T2, 'testR','on');   
out_filename = [ 'corr_mat_glaucoma.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;




%%          

T = table(paramsnormal.SAPMD, paramsmild.SAPMD(range), paramsmoderate.SAPMD(range), paramssevere.SAPMD(range), ...
    paramsnormal.CCA_MEAN, paramsmild.CCA_MEAN(range), paramsmoderate.CCA_MEAN(range), paramssevere.SAPMD(range), ...
    paramsnormal.CCA_SD, paramsmild.CCA_SD(range), paramsmoderate.CCA_SD(range), paramssevere.CCA_SD(range), ...
    paramsnormal.SBR, paramsmild.SBR(range), paramsmoderate.SBR(range), paramssevere.SBR(range));

COL_NAMES = { 'SAP_N', 'SAP_I', 'SAP_O', 'SAP_S', ...
              'CM_N', 'CM_I', 'CM_O', 'CM_S', ...
              'CSD_N', 'CSD_I', 'CSD_O', 'CSD_S', ...
              'SBR_N', 'SBR_I', 'SBR_O', 'SBR_S' ...
              };   
          
T.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T, 'testR','on');


savefig(gcf, 'corr_matrix.fig');
% savefig(gcf, 'corr_matrix.tiff');
% % fig.Position = [0 0 2000 2000];
% print('-dtiff','-r100', 'corr-matrix.tiff');


% SAP_S, CSD_S
figure;
T = table(paramssevere_SAPMD_reshape, paramssevere_CCA_MEAN_reshape);
COL_NAMES = { 'SAP_S', 'CM_S' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Severe vs. CCA Correlation Mean)' ]);


%% Plot individual test case
figure; 
T = table(paramsnormal_SAPMD_reshape, paramsnormal_SBR_reshape);
COL_NAMES = { 'SAP_N', 'SBR_N' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Normal vs. SBR Normal)' ]);
out_filename = [ COL_NAMES{1} '_vs_' COL_NAMES{2} '.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

figure; 
T = table(paramssevere_SAPMD_reshape, paramssevere_CCA_MEAN_reshape);
COL_NAMES = { 'SAP_S', 'CM_S' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Severe vs. CCA Mean Severe)' ]);
out_filename = [ COL_NAMES{1} '_vs_' COL_NAMES{2} '.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

figure; 
T = table(paramssevere_SAPMD_reshape, paramssevere_CCA_SD_reshape);
COL_NAMES = { 'SAP_S', 'CSD_S' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Severe vs. CCA SD Severe)' ]);
out_filename = [ COL_NAMES{1} '_vs_' COL_NAMES{2} '.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;


%% plot normal vs all glaucoma



% set(H, 'titletitle([ 'Correlation Matrix (SAP Severe vs. CCA Correlation SD)' ]);
