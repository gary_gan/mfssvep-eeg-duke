clear all;close all;clc;
% cd('E:\_CerebraTek\_ngoggle kist\_Script')
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(process)\';
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\test single\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(Healthy)\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch (with problem)\'

% FILE_PATH = 'c:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\'
% FILE_PATH = 'c:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test'
FILE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\_filtered_complete_mfssvep1and2'
% FILE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\_filtered_complete_mf1and2\DK0001\20181107';
% FILE_PATH = 'c:\Recordings\duke_2019\_sorted_by_subject_id\testetstst\DK0002\';

GROUP_NORMAL = ['DK0013', 'DK0015', 'DK0033'];
GROUP_MILD = ['DK0011', 'DK0012', 'DK0021'];
GROUP_MODERATE = ['DK0008', 'DK0050', 'DK0053'];
GROUP_SEVERE = ['DK0004', 'DK0014', 'DK0039'];


% pattern_list = ['a', 'b', 'c', 'd', 'e', 'f'];
% pattern_list = {'11', '12', '13', '14', '15', '16'};
pattern_list = {'11', '12'};
pattern = pattern_list{1};
setting.pattern = pattern;

setting.seg_len = 10;
setting.overlap = 0;
setting.srate = 500;
setting.NW = 4;
setting.RMBASE_flag = true;   % apply remove baseline filter or not
setting.BP_flag = true;      % apply bandpass filter or not
setting.ASR_flag = true;     % apply ASR algorithm or not
setting.BPLE = 1;           % bandpass low edge
setting.BPHE = 50;          % bandpass high edge
% setting.chlabel = {'EOG1', 'O1', 'P5', 'POz', 'P6', 'CPz', 'EOG2', 'O2'};
% Gear VR Set
% setting.chlabel = {'O1', 'EOG-L', 'POz', 'PO5', 'CPz', 'PO6', 'O2', 'EOG-R'};

% duke eye center OSVR set
setting.chlabel = {'CH1', 'CH2', 'PO1', 'O1', 'Pz', 'O2', 'Oz', 'PO2'};

% FILE_LIST = dir([FILE_PATH '\*.csv']);
% FILE_LIST = {FILE_LIST.name};

% load XDF
FILE_LIST = get_all_files(FILE_PATH);

xdf_files_invalid = [];
xdf_files_missing_events = [];
% FILE_LIST = dir([FILE_PATH '\*.xdf']);

% pattern freuqnecy list
% if (pattern == 'a')
%     FREQUENCY_LIST = [8.2:0.4:10.2 10.4:0.4:11.6]; % pattern A
% elseif (pattern == 'b')
%     FREQUENCY_LIST = [8:0.4:10 10.6:0.4:11.8]; % pattern B
% end    

FREQUENCY_LIST = [8, 9, 10, 11];

setting.frequency_list = FREQUENCY_LIST;

EPOCH_START_DELAY = 0;
EPOCH_END_MAX = 0.5; % seconds
EPOCH_STEP_MS = 0.05;
EPOCH_DELAY_MAX = 0.1; % seconds, shift until 0.5s

epoch_start = 0;

EPOCH_START = [0 : 50: 500];

cca_all = [];

length(FILE_LIST)
for i =1:length(FILE_LIST)
    expression = '.*mfssvep.*'; % only process file with mfssvep inside name
    if regexp(FILE_LIST{i},expression)    
        
%         fprintf('Analyzing file=%s\n', FILE_LIST{i});
%         for j=1:length(pattern_list)
%             fprintf('Extracing epoch for pattern %s\n', pattern_list{j});
%             setting.pattern = pattern_list{j};
%             proc_vep(setting, FILE_LIST{i});%vep10s
%             
%         end
        
        is_xdf_valid = check_xdf(FILE_LIST{i});
        if (is_xdf_valid ~= 1)
           fprintf('%s\n', FILE_LIST{i});
             xdf_files_invalid{end+1} = FILE_LIST{i};
        end
        fprintf('status=%d\n', check_xdf(FILE_LIST{i}));
        
        % extract diff epoch (11-16) for each file
        for p_idx=1:length(pattern_list)
            setting.pattern = pattern_list{p_idx};
            
            epoch_start = 0; % reset the epoch start 
            
            for e_idx=1:length(EPOCH_START)
                try
                    epoch_start = (EPOCH_START(e_idx)/1000.0); %convert to seconds
                    epoch_end = epoch_start + 5;
                    setting.epoch_range = [ epoch_start epoch_end ];   
                    
                    fprintf('Extracting event id=%s for %s, epoch range=%d\n', pattern_list{p_idx}, FILE_LIST{i}, setting.epoch_range);
                    
                    
                    cca_rel_avg = proc_vep(setting, FILE_LIST{i});%vep10s
                    
                    cca_all = [cca_all cca_rel_avg];
                    
                    filepath = FILE_LIST{i};
                    [pathstr,name,ext] = fileparts(filepath);
                    meta_filename = strrep(name, '-', '_');

                    C = strsplit(name,'-');
                    meta_eye_direction = char(C(4));
                    meta_pattern = pattern_list{p_idx};

                    % cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).cca_max = cca_rel_avg;
                    cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).(strcat('e', num2str(EPOCH_START(e_idx)))) = cca_rel_avg;
                    cca_rel_avg_struct;
                catch exception
                    exp.filepath = FILE_LIST{i};
                    exp.event_id = pattern_list{p_idx};
                    xdf_files_missing_events{end+1} = exp;
                end
            end
        end
    end
end







% for i =1:length(FILE_LIST)
%     if regexp(char(FILE_LIST(i)),'frameonset')
%         continue
%     end
%     if regexp(char(FILE_LIST(i)),'Alpha')
%         proc_alpha(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     end
%     
%     expression = '.*VEP.*';
%     if regexp(char(FILE_LIST(i)),expression)
%         proc_vep(setting,[FILE_PATH '\' char(FILE_LIST(i))]);%vep10s
%         setting.NW = 3;
%         proc_vep2s(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%         
%     elseif regexp(char(FILE_LIST(i)),'.*Supervised.*')
%         proc_caseSUP(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     else
%         proc_other(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     end
% end
set(gcf,'visible','on');close;

