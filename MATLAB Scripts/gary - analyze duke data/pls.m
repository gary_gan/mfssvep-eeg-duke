% Partial Least Square Regression (Jung)
ncomp = 3;

% row range for the glaucoma severity
mild = [1:62];
moderate = [63:83];
severe = [84:107];
normal = [108:125];
all = [1:size(paramsall,1)];

% group to analyze
group = all

X = [ paramsall.CCA_MEAN(group) paramsall.CCA_SD(group) paramsall.SBR(group) ]; 
y = paramsall.SAPMD(group);

[n,p] = size(X);
[Xloadings,Yloadings,Xscores,Yscores,betaPLS10,PLSPctVar] = plsregress(...
    X,y,ncomp);
% [Xloadings,Yloadings,Xscores,Yscores,betaPLS10,PLSPctVar] = plsregress(...
%     X,y,10);
% plot(1:10,cumsum(100*PLSPctVar(2,:)),'-bo');

plot(1:ncomp,cumsum(100*PLSPctVar(2,:)),'-bo');
xlabel('Number of PLS components');
ylabel('Percent Variance Explained in Y');
 
%PLS
% ncomp = 2;
[Xloadings,Yloadings,Xscores,Yscores,betaPLS] = plsregress(X,y,ncomp);
yfitPLS = [ones(n,1) X]*betaPLS;
 
TSS = sum((y-mean(y)).^2);
RSS_PLS = sum((y-yfitPLS).^2);
rsquaredPLS = 1 - RSS_PLS/TSS
 
% PCR
[PCALoadings,PCAScores,PCAVar] = pca(X,'Economy',false);
betaPCR = regress(y-mean(y), PCAScores(:,1:ncomp));
%To make the PCR results easier to interpret in terms of the original spectral data, transform to regression coefficients for the original, uncentered variables.
betaPCR = PCALoadings(:,1:ncomp)*betaPCR;
betaPCR = [mean(y) - mean(X)*betaPCR; betaPCR];
yfitPCR = [ones(n,1) X]*betaPCR;
 
RSS_PCR = sum((y-yfitPCR).^2);
rsquaredPCR = 1 - RSS_PCR/TSS
 
% Plot PCR and PLS results
figure; plot(y,yfitPLS,'bo',y,yfitPCR,'r^');
xlabel('Observed Response');
ylabel('Fitted Response');
legend({'PLSR ' 'PCR '},  ...
    'location','NW','FontSize',14);