function proc_caseSUP(s,filepath)
% s=setting;
% filepath=[FILE_PATH '\' char(FILE_LIST(i))];
SEGD = 10;% data segement in seconds
SEGR = 5; % rest segment in seconds

[pathstr,name,ext] = fileparts(filepath);

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

data = csvread(filepath)';
eeg = data(2:9,:);
% remove baseline
if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% bandpass
if s.BP_flag eeg = bandpass(s,eeg);end
% ASR algorithm
if s.ASR_flag eeg = asr_algo(s,eeg);end

epoch = [];
for i = 1:(SEGD+SEGR)*SRATE:length(eeg)
    if i+SEGD*SRATE-1 <= length(eeg)
        if i == 1
            epoch(:,:,end) = eeg(:,i:i+SEGD*SRATE-1);
        else
            epoch(:,:,end+1) = eeg(:,i:i+SEGD*SRATE-1);
        end
    end
end
[CH,PNT,TRIAL] = size(epoch);

% plot PSD
for j = 1:CH
    pxx = [];f = [];
    for k = 1:TRIAL
        seg = epoch(j,:,k);
        [pxx(end+1,:),f] = pmtm(seg,NW,length(seg),SRATE);
    end
    pxxdb = 10*log10(pxx);
    Q1 = prctile(pxxdb,25);
    Q2 = median(pxxdb,1);
    Q3 = prctile(pxxdb,75);
    
    figure;hold on;set(gcf,'visible','off');
    fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
    plot(f,Q2,'k')
    axis([0 50 -40 60]);
    title([ name char(10) char(CH_LABEL(j)) ' \ ' num2str(SEG_LEN) 'sec segments \ Multitaper PSD'],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',12);
    ylabel('Power/Frequency (dB/Hz)','fontsize',12);
    
    SAVE_PATH = [pathstr '\Figures\PSD(multitaper, ' num2str(s.seg_len) 's seg)\' name];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(j)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(j)) '.fig'])
    close;
    
end