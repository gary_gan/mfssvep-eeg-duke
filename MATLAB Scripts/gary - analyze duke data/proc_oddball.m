%Scripts below compare the ERPs under two conditions

%% JUNG
[erp1 erp2 erpsub time sig] = pop_comperp( ALLEEG, 1,6,8,'addavg','on','addstd','off','subavg','on','diffavg','off','diffstd','off','lowpass',10,'tplotopt',{'ydir' 1});
eeglab redraw;
figure
for chan=1:6,
    subplot(2,3,chan);plot(time(151:end),[erp1(chan,151:end)' erp2(chan,151:end)']);grid;    
    title([EEG.chanlocs(chan).labels])
    %axis([-0.2 1 -10 10])
end
legend('Targets','Non-targets')
xlabel('Time (s)')
ylabel('uV')
figure(gcf)