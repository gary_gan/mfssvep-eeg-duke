
normalization = 'probability';

%% Normal vs Glaucoma (SBR)
figure;
edges = [0:0.2:5]
h1 = histogram(paramsnormal.SBR, 'Normalization', normalization);
% h1.Normalization = 'probability';
h1.BinWidth = 1;
hold on;
edges = [0:0.2:5]
h2 = histogram(paramsglaucomaall.SBR, 'Normalization', normalization);
% h2.Normalization = 'probability';;
h2.BinWidth = 1;

xlabel([ 'SBR (3x 5s mfSSVEP Trial per sector)']);
ylabel(normalization);
title([ 'Histogram - Normal vs. Glaucoma (SBR) ' char(10) 'Normalization: ' normalization]);
legend('Normal', 'Glaucoma');

%% Normal vs Glaucoma (cca mean)
figure;
edges = [0:0.2:5]
h1 = histogram(paramsnormal.CCA_MEAN, 'Normalization', normalization);
% h1.Normalization = 'probability';
h1.BinWidth = 0.01;
hold on;
edges = [0:0.2:5]
h2 = histogram(paramsglaucomaall.CCA_MEAN, 'Normalization', normalization);
% h2.Normalization = 'probability';;
h2.BinWidth = 0.01;

xlabel([ 'CCA Mean (3x 5s mfSSVEP Trial per sector)']);
ylabel(normalization);
title([ 'Histogram - Normal vs. Glaucoma (CCA Mean)' char(10) 'Normalization: ' normalization]);
legend('Normal', 'Glaucoma');

%% Normal vs Glaucoma (cca sd)
figure;
edges = [0:0.2:5]
h1 = histogram(paramsnormal.CCA_SD, 'Normalization', normalization);
% h1.Normalization = 'probability';
h1.BinWidth = 0.01;
hold on;
edges = [0:0.2:5]
h2 = histogram(paramsglaucomaall.CCA_SD, 'Normalization', normalization);
% h2.Normalization = 'probability';;
h2.BinWidth = 0.01;

xlabel([ 'CCA SD (3x 5s mfSSVEP Trial per sector)']);
ylabel(normalization);
title([ 'Histogram - Normal vs. Glaucoma (CCA SD)' char(10) 'Normalization: ' normalization]);
legend('Normal', 'Glaucoma');


