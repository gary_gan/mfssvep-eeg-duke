TEST = 0
PLOT_CCA_PER_EPOCH = 1
PLOT_CCA_MEAN = 1
PLOT_CCA_MEAN_OVERLAP = 1


% TAG = 'severe_bp6-25';
TAG = 'moderate';

SAVE_PATH = ['e:\_MFSSVEP DUKE\_category\_for analysis\' TAG ];

if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;

GROUP_NORMAL = ['DK0013', 'DK0015', 'DK0033'];
GROUP_MILD = ['DK0011', 'DK0012', 'DK0021'];
GROUP_MODERATE = ['DK0008', 'DK0050', 'DK0053'];
GROUP_SEVERE = ['DK0004', 'DK0014', 'DK0039'];

if (TEST) EPOCH_START = [0 : 50: 100]; else EPOCH_START = [0 : 50: 500]; end;

if (TEST) PATTERN_LIST = {'p11', 'p12'}; else PATTERN_LIST = {'p11', 'p12', 'p13', 'p14', 'p15', 'p16'}; end;
% ;
% [1:3] f8

r_mean = [];

FREQ_LIST = { 'f8', 'f9', 'f10', 'f11' };
TRIAL_LIST = { 't1-3' };

% columns index
% f8 = [1:3];
% f9 = [4:6];
% f10 = [7:9];
% f11 = [10:12];

f8 = [1];
f9 = [2];
f10 = [3];
f11 = [4];

freq = { 8, 9, 10, 11 };
if (TEST) freq_cols = { f8, f9 }; else freq_cols = { f8, f9, f10, f11 }; end

epoch = { 'e138' };

eye_direction = '';

%% Plot CCA
rows = {}; % rows to write in the csv


% Iterate each subject
records = fieldnames(cca_rel_avg_struct);

row_idx = 1;
col_idx = 1;

snr_mean = [];

for k=1:numel(records)
    row_idx = k;
    % get filename
    filename = records{k}
    
    % get subject name
    fields = strsplit(filename,'-');
        
    % get eye direction
    if (isfield(cca_rel_avg_struct.(records{k}), 'left'))
        eye_direction = 'left';
    else
        eye_direction = 'right';
    end
    
    rows{row_idx, col_idx} = filename;

    % iterate each pattern
    
    for p_idx=1:length(PATTERN_LIST)
        for e_idx=1:length(epoch)
                
            for freq_idx=1:length(freq_cols)
                        %     r = [];
            %     r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.(strcat('e', num2str(EPOCH_START(e_idx))))(:, f8)]

                % get all the cca max for each epochs separately, 3 trials
                % will have 3 r's
                snr = cca_rel_avg_struct.(records{k}).(eye_direction).(PATTERN_LIST{p_idx}).(epoch{e_idx}).snr_area.nw2(:, freq_cols{freq_idx});
              
                arr = num2cell(snr);
                for i=1:length(arr)
                    col_idx = col_idx+1;
                    rows{row_idx, col_idx} = arr{i};
                end
                
%                 snr_mean{end+1} = mean(snr);
%                 snr_mean{end+1} = snr;
            end            
        end
                 
    end
    
    % write the r_mean, mean_p11_f8, p11_f9...
%     for i=1:length(snr_mean)
%         col_idx = col_idx+1;
%         rows{row_idx, col_idx} = snr_mean(i);
%     end
    
    snr_mean = [];
    col_idx = 1;
end


%% Generate csv columns name
col_name = {};
col_name{end+1} = 'filename';
for p_idx=1:length(PATTERN_LIST)
    for freq_idx=1:length(FREQ_LIST)
        for t_idx=1:length(TRIAL_LIST)
            col_name{end+1} = [ PATTERN_LIST{p_idx} '_' FREQ_LIST{freq_idx} '_' TRIAL_LIST{t_idx} ];
        end
    end
end

% for p_idx=1:length(PATTERN_LIST)
%     for freq_idx=1:length(FREQ_LIST)
%         col_name{end+1} = [ PATTERN_LIST{p_idx} '_' FREQ_LIST{freq_idx} '_snr_mean' ];        
%     end
% end



c = rows;
T = cell2table(c(1:end,:),'VariableNames', col_name)
% T = cell2table(c(2:end,:));
 
% Write the table to a CSV file
% writetable(T,[TAG '_snr.csv'])
writetable(T, fullfile(SAVE_PATH, [ TAG, '-stats-sbr.csv']));


% 
% 
% %% Plot CCA Mean Overlap
% if PLOT_CCA_MEAN_OVERLAP
%     for k=1:numel(records)
%         % get filename
%         filename = records{k};
% 
%         % get subject name
%         fields = strsplit(filename,'-');
% 
% 
%         % get eye direction
%         if (isfield(cca_rel_avg_struct.(records{k}), 'left'))
%             eye_direction = 'left';
%         else
%             eye_direction = 'right';
%         end
% 
%         % iterate each pattern
% 
%         for p_idx=1:length(PATTERN_LIST)
%             f = figure('visible','off'); % one figure per pattern
%             hold on;
% 
%             for f_idx=1:length(freq_cols)
% 
%                 r = [];
%                 r_mean = [];
% 
%                 for e_idx=1:length(EPOCH_START)
%                     hold on;
%                 %     r = [];
%                 %     r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.(strcat('e', num2str(EPOCH_START(e_idx))))(:, f8)]
% 
%                     % get all the cca max for each epochs separately, 3 trials
%                     % will have 3 r's
%                     r = cca_rel_avg_struct.(records{k}).(eye_direction).(PATTERN_LIST{p_idx}).(strcat('e', num2str(EPOCH_START(e_idx))))(:, freq_cols{f_idx});
%                 %     scatter((strcat('e', num2str(EPOCH_START(e_idx)))), r);
% 
%                     r_mean(end+1) = mean(r); % cca max mean for all the epochs
%                 end
% 
%                 % PLOT CCA MEAN         
%                 plot(r_mean);
%                 legend({'f8', 'f9', 'f10', 'f11'});
%                 title({ ['CCA Max (Mean) for 3 Epochs over different epoch onset delay [0,50,...500]'], ...
%                         ['Pattern=' PATTERN_LIST{p_idx} ',freq=8-11' ], ...
%                         [ char(filename) ], ...
%                         [ TAG ] ...
%                          }, 'Interpreter', 'none');    % do not subscript underscore            % xlabel('Epoch');
%                 xlabel('Epoch Onset Delay (s)');                 
%                 ylabel('CCA Max (Mean)');
%                 ylim([0, 0.5]);
%                 set(gca,'XTick', [1 : 1 : length(EPOCH_START)]);
%                 set(gca,'XTickLabel', {'e[0 5]', 'e[0.05 5.05]', 'e[0.1 5.1]', 'e[0.15 5.15]', 'e[0.2 5.2]', 'e[0.25 5.25]', 'e[0.3 5.3]', 'e[0.35 5.35]', 'e[0.4 5.4]', 'e[0.45 5.45]', 'e[0.5 5.5]'});       
%                 set(gca,'XTickLabelRotation',45);
%                 grid on;
% 
% 
%             end
% 
%     %             set(gca,'XTickLabel', {'a', 'b', 'c'});
% 
%             output_filepath = [SAVE_PATH_CCA_MEAN_OVERLAP '\' filename '_' PATTERN_LIST{p_idx} '_f8-11_cca_mean_overlap.png'];
%     %             output_filepath = [ output_filepath{:} ]; % convert to single cell array
%             disp(['Saving file=' output_filepath]);
%             saveas(f, output_filepath);
%             close;              
% 
%         end
% 
%     end
% end

disp('All Done!');


%% 
% for p_idx=1:length(PATTERN_LIST)
%     for e_idx=1:length(EPOCH_START)
%         figure;
%         hold on;
%     %     r = [];
%     %     r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.(strcat('e', num2str(EPOCH_START(e_idx))))(:, f8)]
%         r = cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.(PATTERN_LIST{p_idx}).(strcat('e', num2str(EPOCH_START(e_idx))))(:, f8);
%         plot(r);
%     %     scatter((strcat('e', num2str(EPOCH_START(e_idx)))), r);
% 
%         % calculate mean
%         r_mean(end+1) = mean(r);
%     end
%     % title(['CCA Max for 3 Epochs over different epoch onset delay [0,50,...500] \n Pattern ' PATTERN_LIST{1} ' freq=8' ]);
%     title({ ['CCA Max for 3 Epochs over different epoch onset delay [0,50,...500]'], ...
%             ['Pattern=' PATTERN_LIST{1} ',freq=8' ] });    
% end
% 
% 
% 
% 
% legend('e0', 'e50', 'e100', 'e150', 'e200', 'e250', 'e300', 'e350', 'e400', 'e450', 'e500');
% xlabel('Epoch');
% ylabel('CCA Max');

%% plot mean
% figure(2);
% plot(r_mean);
% legend('e0-500');
% title('CCA Max (Mean) of 3 Epochs over different epoch onset delay [0,50,...500]');
% xlabel('Epoch');
% ylabel('CCA Max (Mean)');

%%

% r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.e100(:, [1:3])]
% r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.e150(:, [1:3])]
% 
% r