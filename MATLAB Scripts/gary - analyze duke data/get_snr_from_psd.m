function [snr_area, snr_db] = get_snr_from_psd(data, data_baseline, srate, ref_freq, nw)

    %% pmtm of CCA
    N_SEGMENT = size(data, 3);
    NW = nw;
    SRATE = srate;
    SEGLEN = (size(data,2)/SRATE);

    % iterate segment
    for i=1:N_SEGMENT
        temp_ = squeeze(data(1,:,i));
        [pxx_(i,:),f] = pmtm(temp_,NW,length(temp_),SRATE);    
    end
    
    %% pmtm of baseline
    % downsample
    NW_baseline = 5; % baseline uses the same NW as the arg
    N_DS_FACTOR = 3;
    %     data_baseline_ = data_baseline(:,1:end-1);
    data_baseline_ = data_baseline(:,1:end);
    data_baseline_ds = zeros(size(data_baseline_,1), size(data_baseline_,2)/N_DS_FACTOR);
    %     data_baseline_ds = zeros(size(data_baseline,1), 2500);

    for i=1:size(data_baseline,1)
        data_baseline_ds(i,:) = downsample(data_baseline_(i,:),N_DS_FACTOR);
    end
    t = squeeze(data_baseline_ds(1,:,:));
    [pxx_baseline, f_baseline] = pmtm(t,NW_baseline,length(t),SRATE);  
    pxx_baseline = pxx_baseline';    

    %% Calculation
    Q2 = median(pxx_,1);
    Q2_zscore = zscore(Q2);

    pxxdb = 10*log10(pxx_);
    Q2_db = median(pxxdb,1);
    Q2_db_zscore = zscore(Q2_db);
    
    % baseline (linear)
    Q2_baseline = median(pxx_baseline,1);    
    Q2_baseline_zscore = zscore(Q2_baseline);
    % base line (db)
    pxx_baseline_db = 10*log10(pxx_baseline);
    Q2_baseline_db = median(pxx_baseline_db,1);    
    Q2_baseline_db_zscore = zscore(Q2_baseline_db);    
    
    %% SNR Area
    N_POINTS = 5;
    power_cca_out = [];
    power_baseline = [];
    
    indices_target_freq = find(f==ref_freq);
    
%     for idx_pts=1:N_POINTS
    power_cca_out =  Q2_zscore(indices_target_freq-2)*0.2 + ...
                            Q2_zscore(indices_target_freq-1)*0.2 + ...
                            Q2_zscore(indices_target_freq)*0.2 + ...
                            Q2_zscore(indices_target_freq+1)*0.2 + ...
                            Q2_zscore(indices_target_freq+2)*0.2;
%     end
    
  
    power_baseline =  Q2_baseline_zscore(indices_target_freq-2)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq-1)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq+1)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq+2)*0.2;    
    
   
    snr_area = power_cca_out / power_baseline; 
    disp([ 'SNR (Z-Score) CCA Out / Baseline (Rest) Area = ' num2str(snr_area)]);
    
    %% SNR dB
    Q2_diff = Q2_db_zscore-Q2_baseline_db_zscore;
    snr_db = Q2_diff(find(f==ref_freq)); % return the snr at target freq
    disp([ 'SNR (Z-Score dB) CCA Out / Baseline (Rest) = ' num2str(snr_db) 'dB']);
        
end