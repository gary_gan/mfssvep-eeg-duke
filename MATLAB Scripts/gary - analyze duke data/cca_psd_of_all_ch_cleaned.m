% Wrapper class to plot PSD of CCA signals
% Inputs:
% EEG
% ref_freq
% out_filepath - Suffix will be added to this
%
% Optional inputs:
%   'nw'               
%   'plot_freq_range'  
%   'epoch_baseline'
%
% Outputs:
%   [EEG]                       - EEGLAB data structure

function [avg_r] = cca_psd_of_all_ch_cleaned(EEG, ref_freq, out_dir, out_filename, varargin)


%% Input Parser
p = inputParser;
addRequired(p, 'EEG');
addRequired(p, 'ref_freq', @isnumeric);
addRequired(p, 'out_dir', @ischar);
addRequired(p, 'out_filename', @ischar);
addParameter(p, 'nw', 3, @isnumeric);
addParameter(p, 'plot_freq_range', [1 50], @isnumeric);
addParameter(p, 'epoch_baseline', struct());
addParameter(p, 'event_id', '', @ischar);

parse(p,EEG,ref_freq,out_dir,out_filename,varargin{:});
p.Results

%% Extract user inputs
SRATE = EEG.srate;
data = EEG.data;
NW = p.Results.nw;
plot_freq_range = p.Results.plot_freq_range;
event_id = p.Results.event_id;
epoch_baseline = p.Results.epoch_baseline;

%%
% SAVE_PATH_ROOT = ['e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\Figures\' s.TAG];

% s=setting;
% [A,B,r,U,V] = canoncorr(X,Y)
% [pathstr,name,ext] = fileparts(filepath);
% meta_filename = name;
% C = strsplit(name,'-');
% % name = cell2mat([C(1) '-' C(4)]);
% name = cell2mat([C(1) '-' C(2) '-' C(3) '-' C(4) '-' C(5)]);
% name = strcat(name, ['-p' num2str(s.pattern,'%d') '-f' num2str(ref_freq,'%d')]);


% ORI
% CH_LABEL = s.chlabel;
% SRATE = s.srate;
% SEG_LEN = s.seg_len;
% OVERLAP = s.overlap;
% NW = nw;
% data = epoch;

color = {'b','r','r','g','k','m','c','c',[1 0.55 0]};
figure;set(gcf,'visible','off')

cca_signals = zeros(1, size(data,2),size(data,3)); % ch x data x trial

%% MOD

avg_r = [];
for i = 1:size(data,3) % for each trial
    [A, B, r, U, V, awx, ~] = CCA_normal(data(:, :, i)', ref_freq);           
    avg_r = [avg_r r(1,1)];
    cca_signals(1,:,i) = U(:,1); % linear combinated signal with highest correlation with ref freq
    
    disp( ['CCA Cor. Max=' num2str(avg_r) ]);

    % save the cca signals
    out_filename_ = [ out_filename '_cca_output.mat'];
    save(fullfile(out_dir, out_filename_), 'cca_signals');           

    % calculate mean cca
    mean_avg_r = mean(avg_r);

    % plot PSD with baseline overlap if epochbaseline exist
%     if exist('epoch_baseline', 'var')
%     if numel(epoch_baseline) > 1
%         xlim_range = [5 15];
%         tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-aftercca' ];            
%         [snr_area, snr_db] = plot_psd_with_baseline (cca_signals, epoch_baseline, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);        
% %             plot_psd_with_baseline_per_epoch (cca_signals, epoch_baseline, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_)        
%     else
%         snr_area = 0;
%         snr_db = 0;
%     end      
end

% Plot PSD of the CCA signal. Multiple trials are synchronously averaged together
xlim_range = plot_freq_range;
dir_suffix = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca' ]; % folder suffix
title_suffix = [ 'CCA: ' num2str(mean_avg_r) ', p' event_id ];
% out_filename = [ EEG.subject '_p' event_id, '_e[' ']_f' num2str(ref_freq) ];
% plot_psd (cca_signals, EEG.srate, out_dir, out_filename, 'nw', NW, 'plot_freq_range', plot_freq_range, 'cca_cor', mean_avg_r, 'dir_suffix', dir_suffix, 'marker_freq', ref_freq, 'sbj_name', EEG.subject, 'title_suffix', title_suffix);
% plot_psd (cca_signals, EEG.srate, out_dir, out_filename, 'nw', NW, 'plot_freq_range', plot_freq_range, 'dir_suffix', dir_suffix, 'marker_freq', ref_freq, 'sbj_name', EEG.subject, 'title_suffix', title_suffix); % no baseline given test
plot_psd (cca_signals, EEG.srate, out_dir, out_filename, 'nw', NW, 'plot_freq_range', plot_freq_range, ...
         'dir_suffix', dir_suffix, 'marker_freq', ref_freq, 'sbj_name', EEG.subject, ...
         'title_suffix', title_suffix, 'epoch_baseline', epoch_baseline, 'plot_psd_diff', 0, ...
         'legend', 'CCA Out', 'plot_marker', 1 );

return;
    
% Plot PSD of the CCA signal + PSD of baseline
% if numel(epoch_baseline) > 1
%     xlim_range = [5 15];
%     tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-aftercca' ];            
%     [snr_area, snr_db] = plot_psd_with_baseline (cca_signals, epoch_baseline, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);        
% %             plot_psd_with_baseline_per_epoch (cca_signals, epoch_baseline, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_)        
% else
%     snr_area = 0;
%     snr_db = 0;
% end 



% plot_psd (cca_signals, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);

% xlim_range = [5 15];
% tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca' ];
% plot_psd (cca_signals, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);



%%
for ch = 1:EEG.nbchan
    clear pxx f
    
    for i = 1:size(data,3) % for each epoch / trial
        temp = squeeze(data(ch,:,i));
        [pxx(i,:),f] = pmtm(temp,NW,length(temp),SRATE);
    end    
end



%% ORI
for ch = [1:6 7]
    clear pxx f
    if ch == 7 % do cca
        avg_r = [];
        for i = 1:size(data,3)
%             freq = freq;
            [A, B, r, U, V, awx, ~] = CCA_normal(data(:, :, i)', ref_freq);
%             betas = cat(1, betas, B(:, 1)');
%             rel = cat(1, rel, r(1));
%             kkk = get_snr(awx(:, 1), f, FS);
%             SNR_ = cat(1, SNR_, kkk);
%             new_result(:, j) = awx(:, 1);  
%                         
            avg_r = [avg_r r(1,1)];
%             [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
            [pxx(i,:),f] = pmtm(U(:,1),NW,length(U(:,1)),SRATE);
            
%             cca_signals = U(:,1);
            cca_signals(1,:,i) = U(:,1);
            
         
            
%             %% calculate SNR
%             % ...    
%             t_freq_idx = find(f==ref_freq); % target freq index
%             Psig = pxx(1, t_freq_idx);
% 
%             N_SIDEBAND_PTS = 2; % how many points in side band in each left/right.
%             Pside = [];
%             offset = t_freq_idx-1;
%             for i=1:N_SIDEBAND_PTS % scan left
%                 offset = offset - 1;
%                Pside(end+1) = pxx(1, offset);
%             end
% 
%             offset = t_freq_idx+1;
%             for i=1:N_SIDEBAND_PTS % scan right
%                 offset = offset + 1;
%                Pside(end+1) = pxx(1, offset);
%             end         
% 
%             PsideAvg = mean(Pside); 
% 
%             SNR = 10*log10(Psig/PsideAvg); % SNR of PSig / Pnoise

        %     fprintf('SNR=%.4f\n', SNR);
        %     disp('snr');            
            

        end
        
        disp( ['CCA Cor. Max=' num2str(avg_r) ]);
        
        % save the cca signals
        output_name = [s.SAVE_PATH_ROOT '\' meta_filename '_p' s.pattern '_f' num2str(ref_freq) '_cca_output.mat'];
        save(output_name,'cca_signals');           
        
        % calculate mean cca
        mean_avg_r = mean(avg_r);
        
        %% draw figure for CCA signal
%         xlim_range = [5 35];
%         tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-aftercca' ];
%         plot_psd (cca_signals, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);
        
%         xlim_range = [5 15];
%         tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-aftercca' ];
%         plot_psd (cca_signals, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);       
        
        % plot PSD with baseline overlap if epochbaseline exist
        if exist('epoch_baseline', 'var')
            xlim_range = [5 15];
            tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-aftercca' ];            
            [snr_area, snr_db] = plot_psd_with_baseline (cca_signals, epoch_baseline, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);        
%             plot_psd_with_baseline_per_epoch (cca_signals, epoch_baseline, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_)        
        else
            snr_area = 0;
            snr_db = 0;
        end
        
        
        
%         
%         pxxdb = 10*log10(pxx);
%         Q2 = median(pxxdb,1);
%         figure;set(gcf,'visible','off')
%         % plot(f,Q2)
%         marker_indices = 1:length(f);
%         plot(f,Q2,'-','MarkerIndices',marker_indices, ...
%                   'MarkerFaceColor','blue', ...
%                   'MarkerEdgeColor','blue',...
%                   'MarkerSize',10);
%         hold on;
%         marker_indices_target_freq = find(f==ref_freq);
%         plot(f(marker_indices_target_freq),Q2(marker_indices_target_freq),...
%                   '-*', ...
%                   'MarkerFaceColor','red', ...
%                   'MarkerEdgeColor','red',...
%                   'MarkerSize',13);
% 
% %             xlim([ 7 12] );
%         xlim([ 5 35 ] );
% 
%         grid on;set(gca,'fontsize',14)
%         title(['PSD(multitaper) / ' name char(10) 'Segment length:' num2str(size(U(:,1),1)/SRATE) 's' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', Avg. CCA correlation:' num2str(mean_avg_r,'%0.2f')  ],'fontsize',12,'interpreter','none');
%         xlabel('Frequency (Hz)','fontsize',14);
%         ylabel('Power (dB/Hz)','fontsize',14);
% 
%         SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-' num2str(size(U(:,1),1)/SRATE) 's segment'];
% 
%         if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
%         output_filename = [ meta_filename '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
%         % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])
% 
%         disp(['Saving figure=' SAVE_PATH '\' output_filename]);
%         print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
%         saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
%         close;      
        
        
        
        
        % save cca output
        % output_name = strcat('C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_f', num2str(ref_freq))
        %output_name = ['C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_f' ref_freq]
        % cca_rel_all = avg_r;
        % save(output_name,'avg_r');
    else % ch 1-6
        for i = 1:size(data,3) % for each epoch / trial
            temp = squeeze(data(ch,:,i));
            [pxx(i,:),f] = pmtm(temp,NW,length(temp),SRATE);
        end
    end
    
    %%
    
    
    pxxdb = 10*log10(pxx);
    Q2 = median(pxxdb,1);
%     Q1 = prctile(pxxdb,25);
%     Q3 = prctile(pxxdb,75);


    %% synchronous average CCA signal
%     for i = 1:size(data,3)
%         temp = squeeze(data(ch,:,i));
%         [pxx(i,:),f] = pmtm(temp,NW,length(temp),SRATE);    
%     end
    

%% Plot the lines
    if SAVE_FIGURE
    % fill Q1 and Q3
    %     fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)


        hold on;
        plot(f,Q2,'color',color{ch})
    end
end


%% Plot
if SAVE_FIGURE
    axis([5 50 -30 20]);
    grid on;set(gca,'fontsize',14)
    title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(size(epoch,3),'%d') ' epochs, Avg. CCA correlation:' num2str(mean_avg_r,'%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);
    xlim([ 5 35 ] );

%     LG=legend(CH_LABEL([3:8]),'CCA result');
%     set(LG,'location','northeastoutside')

    % SAVE_PATH = [pathstr '\Figures\p' num2str(s.pattern) '\CCA+PSD all6ch.( ' num2str(s.seg_len) 's seg)\'];
%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD all6ch.( ' num2str(s.seg_len) 's seg)'];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD all6ch.( NW=' num2str(NW) ')'];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ meta_filename '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);

    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;
end

%% synchronous average the CCA
% split into segments of TRIAL SIZE
N_SEGMENT = 3;
ONSET_LENGTH = 5*500;
offset=0;
segment = zeros(1, ONSET_LENGTH, N_SEGMENT); % channel x data x trial

if size(data,3) > 1
    segment = cca_signals;
    return;

else % split the long segment
    for i=1:N_SEGMENT
        start = offset+1;
        e = start+ONSET_LENGTH-1;    
        segment(:,:,i) = cca_signals(start:e);  
        offset = start + ONSET_LENGTH-1;
    end
end

%% Plot the PSD average of the CCA signal
% plot_psd_avg (segment, s, filepath, ref_freq, nw, [5 35], 'xlim[5-35]');
% plot_psd_avg (segment, s, filepath, ref_freq, nw, [5 15], 'xlim[5-15]');


xlim_range = [5 35];
tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca' ];
plot_psd (segment, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);

xlim_range = [5 15];
tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca' ];
plot_psd (segment, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);

return;



%% moved following function to plot_psd_avg.m
clear pxx_ f
% iterate segment
for i=1:N_SEGMENT
%     temp = squeeze(data(ch,:,i));
    temp_ = squeeze(segment(1,:,i));
    [pxx_(i,:),f] = pmtm(temp_,NW,length(temp_),SRATE);    
end
   pxxdb = 10*log10(pxx_);
    Q2 = median(pxxdb,1);
    
figure;set(gcf,'visible','off')
% plot(f,Q2)
marker_indices = 1:length(f);
plot(f,Q2,'-','MarkerIndices',marker_indices, ...
          'MarkerFaceColor','blue', ...
          'MarkerEdgeColor','blue',...
          'MarkerSize',10);
hold on;
marker_indices_target_freq = find(f==ref_freq);
plot(f(marker_indices_target_freq),Q2(marker_indices_target_freq),...
          '-*', ...
          'MarkerFaceColor','red', ...
          'MarkerEdgeColor','red',...
          'MarkerSize',13);
      
xlim([ 5 35 ] );
grid on;set(gca,'fontsize',14)
title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
xlabel('Frequency (Hz)','fontsize',14);
ylabel('Power (dB/Hz)','fontsize',14);

SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)'];

if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
output_filename = [ meta_filename '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
% print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

disp(['Saving figure=' SAVE_PATH '\' output_filename]);
print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
close;


% calc SNR of the synchronous segments
%% calculate SNR
% ...    
% t_freq_idx = find(f==ref_freq); % target freq index
% Psig = pxx_(1, t_freq_idx);
% 
% N_SIDEBAND_PTS = 2; % how many points in side band in each left/right.
% Pside = [];
% offset = t_freq_idx-1;
% for i=1:N_SIDEBAND_PTS % scan left
%     offset = offset - 1;
%    Pside(end+1) = pxx_(1, offset);
% end
% 
% offset = t_freq_idx+1;
% for i=1:N_SIDEBAND_PTS % scan right
%     offset = offset + 1;
%    Pside(end+1) = pxx_(1, offset);
% end         
% 
% PsideAvg = mean(Pside); 
% 
% Power=(Psig/PsideAvg)
% SNR = 10*log10(Psig/PsideAvg); % SNR of PSig / Pnoise
% disp(['SNR=' SNR]);
% disp('sync done!');


%%
% expression = '\d+Hz';
% [startIndex,endIndex] = regexp(filepath,expression) ;
% freq=str2num(filepath(startIndex:endIndex-2));
% 
% t = (1:length(data))/SRATE;
% ref1 = sin(2*pi*freq*t);
% ref2 = cos(2*pi*freq*t);
% REF = [ref1; ref2];
% color = {'b','','r','g','k','m','c','',[1 0.55 0]};
% figure;set(gcf,'visible','off')
% for ch = [1 3:7 9]
%     clear pxx f
%     if ch == 9
%         avg_r = [];
%         for i = 1:size(data,3)
%             temp = squeeze(data([1 3:7],:,i));
%             clear A B r U V
%             [A,B,r,U,V] = canoncorr(temp',REF');
%             avg_r = [avg_r r(1,1)];
%             [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
%         end
%         avg_r = mean(avg_r);
%     else
%         for i = 1:size(data,3)
%             temp = squeeze(data(ch,:,i));
%             [pxx(i,:),f] = pmtm(temp,4,length(temp),SRATE);
%         end
%     end
%     
%     pxxdb = 10*log10(pxx);
%     Q2 = median(pxxdb,1);
%     
%     hold on;
%     plot(f,Q2,'color',color{ch})
% end
% axis([5 50 -30 20]);
% grid on;set(gca,'fontsize',14)
% title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of 10 epochs, Avg. CCA correlation:' num2str(avg_r,'%0.2f')],'fontsize',12,'interpreter','none');
% xlabel('Frequency (Hz)','fontsize',14);
% ylabel('Power (dB/Hz)','fontsize',14);
% 
% LG=legend(CH_LABEL([1 3:7]),'CCA result');
% set(LG,'location','northeastoutside')
% 
% SAVE_PATH = [pathstr '\Figures\CCA+PSD all7ch.( ' num2str(s.seg_len) 's seg)\'];
% if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
% print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])
% saveas(gcf,[SAVE_PATH '\' name '.fig'])
% close;

