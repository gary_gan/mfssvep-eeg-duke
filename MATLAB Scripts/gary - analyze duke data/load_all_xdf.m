function ALLEEG = load_all_xdf(obj, inputDir)
    allxdf = rdir([inputDir '\**\*.xdf']);
    allxdf = {allxdf.name}';
    disp([num2str(length(allxdf)) ' files found: ']);
    disp(allxdf);
    if obj.fnBaseExist('ALLEEG'), ALLEEG = evalin('base','ALLEEG');
    else ALLEEG = [];end  

    for idx = 1:length(allxdf)
        EEG = pop_loadxdf( allxdf{idx} );
        [ALLEEG, ~, ~] = eeg_store(ALLEEG, EEG, 0);
    end
end  
