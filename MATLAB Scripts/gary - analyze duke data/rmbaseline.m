function eeg = rmbaseline(s,eeg)

% s = setting;
SRATE = s.srate;
[CH,PNT] = size(eeg);

EEG = pop_importdata('dataformat','array','nbchan',CH,'data',eeg,'setname','temp','srate',SRATE,'pnts',0,'xmin',0);
EEG = eeg_checkset( EEG );
% EEG = pop_rmbase( EEG,[]);
EEG = pop_rmbase( EEG );
eeg = EEG.data;