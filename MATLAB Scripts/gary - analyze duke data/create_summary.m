%% Create graphs showing CCA per epoch and CCA per frequencies in each pattern.
function create_summary(cca_rel_avg_struct, savepath)
if ~exist('savepath', 'var') || isempty(savepath)
    SAVE_PATH_ROOT = '.';
else
    SAVE_PATH_ROOT = savepath;
end

SAVE_PATH = fullfile(SAVE_PATH_ROOT, 'CCA PER EPOCH');
SAVE_PATH_CCA_MEAN = fullfile(SAVE_PATH_ROOT, 'CCA MEAN (PER FREQ)');
SAVE_PATH_CCA_MEAN_OVERLAP = fullfile(SAVE_PATH_ROOT, 'CCA MEAN (Per Freq)');

if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
if ~exist(SAVE_PATH_CCA_MEAN,'dir') mkdir(SAVE_PATH_CCA_MEAN); end;
if ~exist(SAVE_PATH_CCA_MEAN_OVERLAP,'dir') mkdir(SAVE_PATH_CCA_MEAN_OVERLAP); end;

eye_direction = '';

%% Plot CCA
% Iterate each subject
records = fieldnames(cca_rel_avg_struct);
for k=1:numel(records)
    % get filename
    filename = records{k};
    
    % get eye direction
    if (isfield(cca_rel_avg_struct.(records{k}), 'left')) eye_direction = 'left';
    else eye_direction = 'right';
    end

    % iterate each pattern
    PATTERN_LIST = fieldnames(cca_rel_avg_struct.(records{k}).(eye_direction));   
    for p_idx=1:length(PATTERN_LIST)
        % find out epochstartdelay
        EPOCHSTARTDELAY_LIST = fieldnames(cca_rel_avg_struct.(records{k}).(eye_direction).(PATTERN_LIST{p_idx}));   
       
        for e_idx=1:length(EPOCHSTARTDELAY_LIST)
           % find out list of frequencies
            FREQUENCY_LIST = fieldnames(cca_rel_avg_struct.(records{k}).(eye_direction).(PATTERN_LIST{p_idx}).(EPOCHSTARTDELAY_LIST{e_idx}).cca);
            r_mean_all_freqs = []; % stores CCA mean for all frequencies, f1, f2, f3...
            for f_idx=1:length(FREQUENCY_LIST)
                % get all the cca max for each epochs separately, 3 trials will have 3 r's
                r = cca_rel_avg_struct.(records{k}).(eye_direction).(PATTERN_LIST{p_idx}).(EPOCHSTARTDELAY_LIST{e_idx}).cca.(FREQUENCY_LIST{f_idx});

                %% Plot CCA Cor for each epoch
                figure('visible','off'); % one figure per frequency
                plot(r);
                title({ ['CCA Max Cor. for 3 Epochs over different epoch onset delay ' EPOCHSTARTDELAY_LIST{e_idx} 'ms'], ...
                        ['Pattern=' PATTERN_LIST{p_idx} ',freq=' FREQUENCY_LIST{f_idx} ], ...
                        [ char(filename) ] ...
                         }, 'Interpreter', 'none');    % do not subscript underscore
                % legend('e[0 5]', 'e[0.05 5.05]', 'e[0.1 5.1]', 'e[0.15 5.15]', 'e[0.2 5.2]', 'e[0.25 5.25]', 'e[0.3 5.3]', 'e[0.35 5.35]', 'e[0.4 5.4]', 'e[0.45 5.45]', 'e[0.5 5.5]');
                legend(EPOCHSTARTDELAY_LIST{e_idx});
                xlabel('Epoch');
                ylabel('CCA Max');    
                ylim([0, 0.5]);
                set(gca,'XTick', [1 : 1 : 5]);
                grid on; 
                output_filepath = [SAVE_PATH '\' filename '_' PATTERN_LIST{p_idx} '_' FREQUENCY_LIST{f_idx} '_cca_all.png'];
                util_savefig(output_filepath);
                close;                

                r_mean_all_freqs(end+1) = mean(r); % cca max mean for all the epochs
            end % for each FREQ
            
            %% Plot CCA mean for all frequency in a pattern
            figure('visible','off'); 
            plot(r_mean_all_freqs, '-*');
            legend('CCA Mean');
            title({ ['CCA Max Cor. (Mean) for ' num2str(size(r_mean_all_freqs,2)) ' frequencies on epoch onset delay ' EPOCHSTARTDELAY_LIST{e_idx} 'ms'], ...
                    ['Pattern=' PATTERN_LIST{p_idx} ], ...
                    [ char(filename) ] ...
                     }, 'Interpreter', 'none');    % do not subscript underscore                
            xlabel('Frequencies');                 
            ylabel('CCA Max (Mean)');
            xlim([0, length(FREQUENCY_LIST)+1]); % add some margin at the xticks
            ylim([0, 0.5]);
            set(gca,'XTick', [1:length(FREQUENCY_LIST)]);
            set(gca,'XTickLabel', cellstr(FREQUENCY_LIST)');       
%             set(gca,'XTickLabelRotation',45); 
            grid on;
            output_filepath = [SAVE_PATH_CCA_MEAN '\' filename '_' PATTERN_LIST{p_idx} '_' FREQUENCY_LIST{f_idx} '_cca_mean.png'];
            util_savefig(output_filepath);
            close;            
            
        end % for each EPOCHSTARTDELAY
    end % for each PATTERN
end % for each RECORD

return % go back!







%% Plot CCA Mean Overlap
if PLOT_CCA_MEAN_OVERLAP
    for k=1:numel(records)
        % get filename
        filename = records{k};

        % get subject name
        fields = strsplit(filename,'-');


        % get eye direction
        if (isfield(cca_rel_avg_struct.(records{k}), 'left'))
            eye_direction = 'left';
        else
            eye_direction = 'right';
        end

        % iterate each pattern

        for p_idx=1:length(PATTERN_LIST)
            f = figure('visible','off'); % one figure per pattern
            hold on;

            for f_idx=1:length(freq_cols)

                r = [];
                r_mean = [];

                for e_idx=1:length(EPOCH_START)
                    hold on;
                %     r = [];
                %     r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.(strcat('e', num2str(EPOCH_START(e_idx))))(:, f8)]

                    % get all the cca max for each epochs separately, 3 trials
                    % will have 3 r's
                    r = cca_rel_avg_struct.(records{k}).(eye_direction).(PATTERN_LIST{p_idx}).(strcat('e', num2str(EPOCH_START(e_idx)))).cca(:, freq_cols{f_idx});
                %     scatter((strcat('e', num2str(EPOCH_START(e_idx)))), r);

                    r_mean(end+1) = mean(r); % cca max mean for all the epochs
                end

                % PLOT CCA MEAN 
                if size(EPOCH_START,2) > 1
                    plot(r_mean);
                else
                    % use scatter if epoch = 1
                    scatter([1:size(EPOCH_START, 2)], r_mean);
                end
                
                legend({'f8', 'f9', 'f10', 'f11'});
                title({ ['CCA Max (Mean) for 3 Epochs over different epoch onset delay [0,50,...500]'], ...
                        ['Pattern=' PATTERN_LIST{p_idx} ',freq=8-11' ], ...
                        [ char(filename) ], ...
                        [ TAG ] ...
                         }, 'Interpreter', 'none');    % do not subscript underscore            % xlabel('Epoch');
                xlabel('Epoch Onset Delay (s)');                 
                ylabel('CCA Max (Mean)');
                ylim([0, 0.5]);
                set(gca,'XTick', [1 : 1 : length(EPOCH_START)]);
                set(gca,'XTickLabel', {'e[0 5]', 'e[0.05 5.05]', 'e[0.1 5.1]', 'e[0.15 5.15]', 'e[0.2 5.2]', 'e[0.25 5.25]', 'e[0.3 5.3]', 'e[0.35 5.35]', 'e[0.4 5.4]', 'e[0.45 5.45]', 'e[0.5 5.5]'});       
                set(gca,'XTickLabelRotation',45);
                grid on;


            end

    %             set(gca,'XTickLabel', {'a', 'b', 'c'});

            output_filepath = [SAVE_PATH_CCA_MEAN_OVERLAP '\' filename '_' PATTERN_LIST{p_idx} '_f8-11_cca_mean_overlap.png'];
    %             output_filepath = [ output_filepath{:} ]; % convert to single cell array
            disp(['Saving file=' output_filepath]);
            saveas(f, output_filepath);
            close;              

        end

    end
end

disp('All Done!');


%% 
% for p_idx=1:length(PATTERN_LIST)
%     for e_idx=1:length(EPOCH_START)
%         figure;
%         hold on;
%     %     r = [];
%     %     r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.(strcat('e', num2str(EPOCH_START(e_idx))))(:, f8)]
%         r = cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.(PATTERN_LIST{p_idx}).(strcat('e', num2str(EPOCH_START(e_idx))))(:, f8);
%         plot(r);
%     %     scatter((strcat('e', num2str(EPOCH_START(e_idx)))), r);
% 
%         % calculate mean
%         r_mean(end+1) = mean(r);
%     end
%     % title(['CCA Max for 3 Epochs over different epoch onset delay [0,50,...500] \n Pattern ' PATTERN_LIST{1} ' freq=8' ]);
%     title({ ['CCA Max for 3 Epochs over different epoch onset delay [0,50,...500]'], ...
%             ['Pattern=' PATTERN_LIST{1} ',freq=8' ] });    
% end
% 
% 
% 
% 
% legend('e0', 'e50', 'e100', 'e150', 'e200', 'e250', 'e300', 'e350', 'e400', 'e450', 'e500');
% xlabel('Epoch');
% ylabel('CCA Max');

%% plot mean
% figure(2);
% plot(r_mean);
% legend('e0-500');
% title('CCA Max (Mean) of 3 Epochs over different epoch onset delay [0,50,...500]');
% xlabel('Epoch');
% ylabel('CCA Max (Mean)');

%%

% r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.e100(:, [1:3])]
% r = [r cca_rel_avg_struct.DK0002_10_10_1985_mfssvep_left_1.left.p11.e150(:, [1:3])]
% 
% r