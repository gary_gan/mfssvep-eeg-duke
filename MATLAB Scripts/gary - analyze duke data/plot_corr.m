range = [ 1:18 ];

SAVE_PATH = 'e:\_MFSSVEP DUKE\__share';
SAVE_PATH = 'e:\_MFSSVEP DUKE\__share\_share with alessandro\corr_matrix';


RESHAPE_SIZE = 62;    
paramsnormal_SAPMD_reshape = my_reshape(paramsnormal.SAPMD, RESHAPE_SIZE);
paramsmild_SAPMD_reshape = my_reshape(paramsmild.SAPMD, RESHAPE_SIZE);
paramsmoderate_SAPMD_reshape = my_reshape(paramsmoderate.SAPMD, RESHAPE_SIZE);
paramssevere_SAPMD_reshape = my_reshape(paramssevere.SAPMD, RESHAPE_SIZE);

paramsnormal_CCA_MEAN_reshape = my_reshape(paramsnormal.CCA_MEAN, RESHAPE_SIZE);
paramsmild_CCA_MEAN_reshape = my_reshape(paramsmild.CCA_MEAN, RESHAPE_SIZE);
paramsmoderate_CCA_MEAN_reshape = my_reshape(paramsmoderate.CCA_MEAN, RESHAPE_SIZE);
paramssevere_CCA_MEAN_reshape = my_reshape(paramssevere.CCA_MEAN, RESHAPE_SIZE);

paramsnormal_CCA_SD_reshape = my_reshape(paramsnormal.CCA_SD, RESHAPE_SIZE);
paramsmild_CCA_SD_reshape = my_reshape(paramsmild.CCA_SD, RESHAPE_SIZE);
paramsmoderate_CCA_SD_reshape = my_reshape(paramsmoderate.CCA_SD, RESHAPE_SIZE);
paramssevere_CCA_SD_reshape = my_reshape(paramssevere.CCA_SD, RESHAPE_SIZE);

paramsnormal_SBR_reshape = my_reshape(paramsnormal.SBR, RESHAPE_SIZE);
paramsmild_SBR_reshape = my_reshape(paramsmild.SBR, RESHAPE_SIZE);
paramsmoderate_SBR_reshape = my_reshape(paramsmoderate.SBR, RESHAPE_SIZE);
paramssevere_SBR_reshape = my_reshape(paramssevere.SBR, RESHAPE_SIZE);

T2 = table(paramsnormal_SAPMD_reshape, paramsmild_SAPMD_reshape, paramsmoderate_SAPMD_reshape, paramssevere_SAPMD_reshape, ...
    paramsnormal_CCA_MEAN_reshape, paramsmild_CCA_MEAN_reshape, paramsmoderate_CCA_MEAN_reshape, paramssevere_CCA_MEAN_reshape, ...
    paramsnormal_CCA_SD_reshape, paramsmild_CCA_SD_reshape, paramsmoderate_CCA_SD_reshape, paramssevere_CCA_SD_reshape, ...
    paramsnormal_SBR_reshape, paramsmild_SBR_reshape, paramsmoderate_SBR_reshape, paramssevere_SBR_reshape);

COL_NAMES = { 'MD_N', 'MD_I', 'MD_O', 'MD_S', ...
              'CM_N', 'CM_I', 'CM_O', 'CM_S', ...
              'CSD_N', 'CSD_I', 'CSD_O', 'CSD_S', ...
              'SBR_N', 'SBR_I', 'SBR_O', 'SBR_S' ...
              };  
          
T2.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T2, 'testR','on');          
          
          
%% plot corr for NORMAL
T2 = table(paramsnormal_SAPMD_reshape, paramsnormal_CCA_MEAN_reshape, paramsnormal_CCA_SD_reshape, paramsnormal_SBR_reshape);

COL_NAMES = { 'MD_N', 'CM_N', 'CSD_N', 'SBR_N' };
          
T2.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T2, 'testR','on');   
out_filename = [ 'corr_mat_normal.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

%% plot corr for MILD
T2 = table(paramsmild_SAPMD_reshape, paramsmild_CCA_MEAN_reshape, paramsmild_CCA_SD_reshape, paramsmild_SBR_reshape);

COL_NAMES = { 'MD_I', 'CM_I', 'CSD_I', 'SBR_I' };
          
T2.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T2, 'testR','on');   
out_filename = [ 'corr_mat_mild.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

%% plot corr for MODERATE
T2 = table(paramsmoderate_SAPMD_reshape, paramsmoderate_CCA_MEAN_reshape, paramsmoderate_CCA_SD_reshape, paramsmoderate_SBR_reshape);

COL_NAMES = { 'MD_O', 'CM_O', 'CSD_O', 'SBR_O' };
          
T2.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T2, 'testR','on');   
out_filename = [ 'corr_mat_moderate.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

%% plot corr for SEVERE
T2 = table(paramssevere_SAPMD_reshape, paramssevere_CCA_MEAN_reshape, paramssevere_CCA_SD_reshape, paramssevere_SBR_reshape);

COL_NAMES = { 'MD_S', 'CM_S', 'CSD_S', 'SBR_S' };
          
T2.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T2, 'testR','on');   
out_filename = [ 'corr_mat_severe.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

%% rnova test
load fisheriris

t = table(species,meas(:,1),meas(:,2),meas(:,3),meas(:,4),...
'VariableNames',{'species','meas1','meas2','meas3','meas4'});
Meas = table([1 2 3 4]','VariableNames',{'Measurements'});
rm = fitrm(t,'meas1-meas4~species','WithinDesign',Meas);
ranovatbl = ranova(rm)




%%

T = table(paramsnormal.SAPMD, paramsmild.SAPMD(range), paramsmoderate.SAPMD(range), paramssevere.SAPMD(range), ...
    paramsnormal.CCA_MEAN, paramsmild.CCA_MEAN(range), paramsmoderate.CCA_MEAN(range), paramssevere.SAPMD(range), ...
    paramsnormal.CCA_SD, paramsmild.CCA_SD(range), paramsmoderate.CCA_SD(range), paramssevere.CCA_SD(range), ...
    paramsnormal.SBR, paramsmild.SBR(range), paramsmoderate.SBR(range), paramssevere.SBR(range));

COL_NAMES = { 'SAP_N', 'SAP_I', 'SAP_O', 'SAP_S', ...
              'CM_N', 'CM_I', 'CM_O', 'CM_S', ...
              'CSD_N', 'CSD_I', 'CSD_O', 'CSD_S', ...
              'SBR_N', 'SBR_I', 'SBR_O', 'SBR_S' ...
              };   
          
T.Properties.VariableNames = COL_NAMES;
figure;
[R, PValue, H] = corrplot(T, 'testR','on');


savefig(gcf, 'corr_matrix.fig');
% savefig(gcf, 'corr_matrix.tiff');
% % fig.Position = [0 0 2000 2000];
% print('-dtiff','-r100', 'corr-matrix.tiff');


% SAP_S, CSD_S
figure;
T = table(paramssevere_SAPMD_reshape, paramssevere_CCA_MEAN_reshape);
COL_NAMES = { 'SAP_S', 'CM_S' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Severe vs. CCA Correlation Mean)' ]);


%% Plot individual test case
figure; 
T = table(paramsnormal_SAPMD_reshape, paramsnormal_SBR_reshape);
COL_NAMES = { 'SAP_N', 'SBR_N' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Normal vs. SBR Normal)' ]);
out_filename = [ COL_NAMES{1} '_vs_' COL_NAMES{2} '.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

figure; 
T = table(paramssevere_SAPMD_reshape, paramssevere_CCA_MEAN_reshape);
COL_NAMES = { 'SAP_S', 'CM_S' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Severe vs. CCA Mean Severe)' ]);
out_filename = [ COL_NAMES{1} '_vs_' COL_NAMES{2} '.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;

figure; 
T = table(paramssevere_SAPMD_reshape, paramssevere_CCA_SD_reshape);
COL_NAMES = { 'SAP_S', 'CSD_S' };
T.Properties.VariableNames = COL_NAMES;
[R, PValue, H] = corrplot(T, 'testR','on');
set(gcf,'name', [ 'Correlation Matrix (SAP Severe vs. CCA SD Severe)' ]);
out_filename = [ COL_NAMES{1} '_vs_' COL_NAMES{2} '.tiff' ];
saveas(gcf, fullfile(SAVE_PATH, out_filename));
close;


%% plot normal vs all glaucoma



% set(H, 'titletitle([ 'Correlation Matrix (SAP Severe vs. CCA Correlation SD)' ]);
