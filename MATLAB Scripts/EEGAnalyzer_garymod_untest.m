classdef EEGAnalyzer < handle
    
    properties (Access = private)
        % Default Param---------------------------
        DEFAULT_srate = 500
        DEFAULT_outputDir = 'C:\'
        DEFAULT_outputOrg = 'flat' % hierarchy or flat
        DEFAULT_title_comp = {'activity.type' 'session.title' 'subject._id'}; % fields from metadata (.ngmeta) file
        
        DEFAULT_log_obj = []
        % 0~7:{'ALL', 'TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'OFF'};
        DEFAULT_log_level = 0
        DEFAULT_command_window_level = 0
        % ----Bandpass parameters----
        DEFAULT_lowedge = 1
        DEFAULT_highedge = 100
        % ----Notchfilter parameters----
        DEFAULT_notch_low = 59
        DEFAULT_notch_high = 61
        % ----ASR parameters----
        DEFAULT_enable_remove_flatline = 0
        DEFAULT_flatline_crit = 5
        
        DEFAULT_enable_highpass_filter = 1
        DEFAULT_highpass_from = 0.5
        DEFAULT_highpass_end = 1
        
        DEFAULT_enable_remove_hopeless_channels = 0
        DEFAULT_channel_crit = 0.45
        DEFAULT_channel_crit_excluded = 0.1
        DEFAULT_channel_crit_maxbad_time = 0.5
        
        DEFAULT_enable_burst_repair = 1
        DEFAULT_burst_crit = 3
        DEFAULT_burst_crit_refmaxbadchns = 0.075
        DEFAULT_burst_crit_reftolerances_from = -5
        DEFAULT_burst_crit_reftolerances_end = 3
        
        DEFAULT_enable_remove_hopeless_time_window = 0
        DEFAULT_window_crit = 0.1
        DEFAULT_window_crit_tolerances_lower = []
        DEFAULT_window_crit_tolerances_upper = []
        % ----Plot PSD parameters----
        DEFAULT_normalize_data = 1 % ori 0
        DEFAULT_NW = 4
        DEFAULT_sync_avg_segment_sec = 10 % 0:whole data, otherwise extract data into segments
        DEFAULT_overlap_fraction = 0.5
        DEFAULT_enable_auto_x_axis = 0
        DEFAULT_x_min = 0 % Hz
        DEFAULT_x_max = 100 % Hz
        DEFAULT_enable_auto_y_axis = 0
        DEFAULT_y_min = -40 % dB
        DEFAULT_y_max = 40 % dB
        % ----Plot CCA PSD parameters----
        DEFAULT_enable_CCA = 1; % ori 0
        DEFAULT_use_sine_ref = 1
        DEFAULT_use_cosine_ref = 1
        % ----Plot waveform parameters----
        DEFAULT_window_sec = 5
        DEFAULT_normalized_output = 0
        DEFAULT_normalized_offset = 5
        % ----Plot Oddball parameters----
        DEFAULT_plot_each_channel_TvsNT = 1 % enable plot each channel target v.s non-target
        DEFAULT_plot_ERP_image = 1 % enable plot ERP image
        DEFAULT_plot_ERP_amplitude_min = -300 % amplitude after baseline removal. If any amplitude in a trial is lower than this value, remove this trial.
        DEFAULT_plot_ERP_amplitude_max = 300 % amplitude after baseline removal. If any amplitude in a trial is larger than this value, remove this trial.
        DEFAULT_plot_ERP_smoothing = 10 % smoothing trials window length, in trial
        DEFAULT_oddball_x_min = -200 % oddball image (T v.s NT / ERP image) time range, in ms
        DEFAULT_oddball_x_max = 1000 % oddball image (T v.s NT / ERP image) image time range, in ms
        DEFAULT_oddball_y_min = -30 % oddball image (T v.s NT / ERP image) image Y-axis, in uV
        DEFAULT_oddball_y_max = 30 % oddball image (T v.s NT / ERP image) image Y-axis, in uV
        DEFAULT_plot_ERP_caxis_min = -25 % color bar min
        DEFAULT_plot_ERP_caxis_max = 25 % color bar max
        % End of Default Param---------------------------
        srate
        outputDir
        outputOrg
        title_comp
        
        log_obj
        log_level
        command_window_level
        % ----Bandpass parameters----
        lowedge
        highedge
        % ----Notchfilter parameters----
        notch_low
        notch_high
        % ----ASR parameters----
        enable_remove_flatline
        flatline_crit
        
        enable_highpass_filter
        highpass_from
        highpass_end
        
        enable_remove_hopeless_channels
        channel_crit
        channel_crit_excluded
        channel_crit_maxbad_time
        
        enable_burst_repair
        burst_crit
        burst_crit_refmaxbadchns
        burst_crit_reftolerances_from
        burst_crit_reftolerances_end
        
        enable_remove_hopeless_time_window
        window_crit
        window_crit_tolerances_lower
        window_crit_tolerances_upper
        % ----Plot PSD parameters----
        normalize_data
        NW
        sync_avg_segment_sec
        overlap_fraction
        enable_auto_x_axis
        x_min
        x_max
        enable_auto_y_axis
        y_min
        y_max
        % ----Plot CCA PSD parameters----
        enable_CCA
        use_sine_ref
        use_cosine_ref
        % ----Plot waveform parameters----
        window_sec
        normalized_output
        normalized_offset
        % ----Plot Oddball parameters----
        plot_each_channel_TvsNT
        plot_ERP_image
        plot_ERP_amplitude_min
        plot_ERP_amplitude_max
        plot_ERP_smoothing
        oddball_x_min
        oddball_x_max
        oddball_y_min
        oddball_y_max
        plot_ERP_caxis_min
        plot_ERP_caxis_max
    end
    
    methods
        function obj = EEGAnalyzer(varargin) % if varargin == 0 , no folder / log file are created
            obj.srate = obj.DEFAULT_srate;
            obj.outputDir = obj.DEFAULT_outputDir;
            obj.outputOrg = obj.DEFAULT_outputOrg;
            obj.title_comp = obj.DEFAULT_title_comp;
            
            obj.log_obj = obj.DEFAULT_log_obj;
            obj.log_level = obj.DEFAULT_log_level;
            obj.command_window_level = obj.DEFAULT_command_window_level;
            % ----Bandpass parameters----
            obj.lowedge = obj.DEFAULT_lowedge;
            obj.highedge = obj.DEFAULT_highedge;
            % ----Notchfilter parameters----
            obj.notch_low = obj.DEFAULT_notch_low;
            obj.notch_high = obj.DEFAULT_notch_high;
            % ----ASR parameters----
            obj.enable_remove_flatline = obj.DEFAULT_enable_remove_flatline;
            obj.flatline_crit = obj.DEFAULT_flatline_crit;
            
            obj.enable_highpass_filter = obj.DEFAULT_enable_highpass_filter;
            obj.highpass_from = obj.DEFAULT_highpass_from;
            obj.highpass_end = obj.DEFAULT_highpass_end;
            
            obj.enable_remove_hopeless_channels = obj.DEFAULT_enable_remove_hopeless_channels;
            obj.channel_crit = obj.DEFAULT_channel_crit;
            obj.channel_crit_excluded = obj.DEFAULT_channel_crit_excluded;
            obj.channel_crit_maxbad_time = obj.DEFAULT_channel_crit_maxbad_time;
            
            obj.enable_burst_repair = obj.DEFAULT_enable_burst_repair;
            obj.burst_crit = obj.DEFAULT_burst_crit;
            obj.burst_crit_refmaxbadchns = obj.DEFAULT_burst_crit_refmaxbadchns;
            obj.burst_crit_reftolerances_from = obj.DEFAULT_burst_crit_reftolerances_from;
            obj.burst_crit_reftolerances_end = obj.DEFAULT_burst_crit_reftolerances_end;
            
            obj.enable_remove_hopeless_time_window = obj.DEFAULT_enable_remove_hopeless_time_window;
            obj.window_crit = obj.DEFAULT_window_crit;
            obj.window_crit_tolerances_lower = obj.DEFAULT_window_crit_tolerances_lower;
            obj.window_crit_tolerances_upper = obj.DEFAULT_window_crit_tolerances_upper;
            % ----Plot PSD parameters----
            obj.normalize_data = obj.DEFAULT_normalize_data;
            obj.NW = obj.DEFAULT_NW;
            obj.sync_avg_segment_sec = obj.DEFAULT_sync_avg_segment_sec;
            obj.overlap_fraction = obj.DEFAULT_overlap_fraction;
            obj.enable_auto_x_axis = obj.DEFAULT_enable_auto_x_axis;
            obj.x_min = obj.DEFAULT_x_min;
            obj.x_max = obj.DEFAULT_x_max;
            obj.enable_auto_y_axis = obj.DEFAULT_enable_auto_y_axis;
            obj.y_min = obj.DEFAULT_y_min;
            obj.y_max = obj.DEFAULT_y_max;
            % ----Plot CCA PSD parameters----
            obj.enable_CCA = obj.DEFAULT_enable_CCA;
            obj.use_sine_ref = obj.DEFAULT_use_sine_ref;
            obj.use_cosine_ref = obj.DEFAULT_use_cosine_ref;
            % ----Plot waveform parameters----
            obj.window_sec = obj.DEFAULT_window_sec;
            obj.normalized_output = obj.DEFAULT_normalized_output;
            obj.normalized_offset = obj.DEFAULT_normalized_offset;
            % ----Plot Oddball parameters----
            obj.plot_each_channel_TvsNT = obj.DEFAULT_plot_each_channel_TvsNT;
            obj.plot_ERP_image = obj.DEFAULT_plot_ERP_image;
            obj.plot_ERP_amplitude_min = obj.DEFAULT_plot_ERP_amplitude_min;
            obj.plot_ERP_amplitude_max = obj.DEFAULT_plot_ERP_amplitude_max;
            obj.plot_ERP_smoothing = obj.DEFAULT_plot_ERP_smoothing;
            obj.oddball_x_min = obj.DEFAULT_oddball_x_min;
            obj.oddball_x_max = obj.DEFAULT_oddball_x_max;
            obj.oddball_y_min = obj.DEFAULT_oddball_y_min;
            obj.oddball_y_max = obj.DEFAULT_oddball_y_max;
            obj.plot_ERP_caxis_min = obj.DEFAULT_plot_ERP_caxis_min;
            obj.plot_ERP_caxis_max = obj.DEFAULT_plot_ERP_caxis_max;
            
            [eeglabpath, ~, ~] = fileparts(which('eeglab'));
            [analyzererpath, ~, ~] = fileparts(which('EEGAnalyzer'));
            if isempty(eeglabpath)
                error('Cannot find EEGLAB!');
            elseif isempty(analyzererpath)
                error('Cannot find EEGAnalyzer!');
            else
                try
                    addpath(genpath(fullfile(analyzererpath,'_lib')));
                    addpath(genpath(fullfile(eeglabpath,'functions')));
                    addpath(genpath(fullfile(eeglabpath,'plugins')));
                catch
                end
            end
            
            if isempty(varargin)
                warning('Analyzing using default setting');
                obj = obj.init(varargin{:});
            elseif ~isempty(varargin) && length(varargin) == 1 && all(varargin{end} == 0)
                return;
            else
                obj = obj.init(varargin{:});
            end
        end
        
        function Config = getDefaultValue(obj)
            Config.srate  = obj.DEFAULT_srate;
            Config.outputDir  = obj.DEFAULT_outputDir;
            Config.outputOrg  = obj.DEFAULT_outputOrg;
            Config.title_comp = obj.DEFAULT_title_comp;
            
            Config.log_obj  = obj.DEFAULT_log_obj;
            Config.log_level  = obj.DEFAULT_log_level;
            Config.command_window_level  = obj.DEFAULT_command_window_level;
            % ----Bandpass parameters----
            Config.lowedge  = obj.DEFAULT_lowedge;
            Config.highedge  = obj.DEFAULT_highedge;
            % ----Notchfilter parameters----
            Config.notch_low  = obj.DEFAULT_notch_low;
            Config.notch_high  = obj.DEFAULT_notch_high;
            % ----ASR parameters----
            Config.enable_remove_flatline  = obj.DEFAULT_enable_remove_flatline;
            Config.flatline_crit  = obj.DEFAULT_flatline_crit;
            
            Config.enable_highpass_filter  = obj.DEFAULT_enable_highpass_filter;
            Config.highpass_from  = obj.DEFAULT_highpass_from;
            Config.highpass_end  = obj.DEFAULT_highpass_end;
            
            Config.enable_remove_hopeless_channels  = obj.DEFAULT_enable_remove_hopeless_channels;
            Config.channel_crit  = obj.DEFAULT_channel_crit;
            Config.channel_crit_excluded  = obj.DEFAULT_channel_crit_excluded;
            Config.channel_crit_maxbad_time  = obj.DEFAULT_channel_crit_maxbad_time;
            
            Config.enable_burst_repair  = obj.DEFAULT_enable_burst_repair;
            Config.burst_crit  = obj.DEFAULT_burst_crit;
            Config.burst_crit_refmaxbadchns  = obj.DEFAULT_burst_crit_refmaxbadchns;
            Config.burst_crit_reftolerances_from  = obj.DEFAULT_burst_crit_reftolerances_from;
            Config.burst_crit_reftolerances_end  = obj.DEFAULT_burst_crit_reftolerances_end;
            
            Config.enable_remove_hopeless_time_window  = obj.DEFAULT_enable_remove_hopeless_time_window;
            Config.window_crit  = obj.DEFAULT_window_crit;
            Config.window_crit_tolerances_lower  = obj.DEFAULT_window_crit_tolerances_lower;
            Config.window_crit_tolerances_upper  = obj.DEFAULT_window_crit_tolerances_upper;
            % ----Plot PSD parameters----
            Config.normalize_data  = obj.DEFAULT_normalize_data;
            Config.NW  = obj.DEFAULT_NW;
            Config.sync_avg_segment_sec  = obj.DEFAULT_sync_avg_segment_sec;
            Config.overlap_fraction  = obj.DEFAULT_overlap_fraction;
            Config.enable_auto_x_axis  = obj.DEFAULT_enable_auto_x_axis;
            Config.x_min  = obj.DEFAULT_x_min;
            Config.x_max  = obj.DEFAULT_x_max;
            Config.enable_auto_y_axis  = obj.DEFAULT_enable_auto_y_axis;
            Config.y_min  = obj.DEFAULT_y_min;
            Config.y_max  = obj.DEFAULT_y_max;
            % ----Plot CCA PSD parameters----
            Config.enable_CCA  = obj.DEFAULT_enable_CCA;
            Config.use_sine_ref  = obj.DEFAULT_use_sine_ref;
            Config.use_cosine_ref  = obj.DEFAULT_use_cosine_ref;
            % ----Plot waveform parameters----
            Config.window_sec  = obj.DEFAULT_window_sec;
            Config.normalized_output  = obj.DEFAULT_normalized_output;
            Config.normalized_offset  = obj.DEFAULT_normalized_offset;
            % ----Plot Oddball parameters----
            Config.plot_each_channel_TvsNT = obj.DEFAULT_plot_each_channel_TvsNT;
            Config.plot_ERP_image = obj.DEFAULT_plot_ERP_image;
            Config.plot_ERP_amplitude_min = obj.DEFAULT_plot_ERP_amplitude_min;
            Config.plot_ERP_amplitude_max = obj.DEFAULT_plot_ERP_amplitude_max;
            Config.plot_ERP_smoothing = obj.DEFAULT_plot_ERP_smoothing;
            Config.oddball_x_min = obj.DEFAULT_oddball_x_min;
            Config.oddball_x_max = obj.DEFAULT_oddball_x_max;
            Config.oddball_y_min = obj.DEFAULT_oddball_y_min;
            Config.oddball_y_max = obj.DEFAULT_oddball_y_max;
            Config.plot_ERP_caxis_min = obj.DEFAULT_plot_ERP_caxis_min;
            Config.plot_ERP_caxis_max = obj.DEFAULT_plot_ERP_caxis_max;
        end
        
        function ALLEEG = doBandpass(obj, ALLEEG, varargin)
            if ~isArgPaired(obj, varargin{:})
                return;
            end
            options = finputcheck( varargin, { ...
                'lowedge'     {'real' 'integer'}     [0 Inf]    obj.DEFAULT_lowedge; ...
                'highedge'    {'real' 'integer'}     [0 Inf]    obj.DEFAULT_highedge});
            if isstr(options), error(options); end;
            obj.setProperties(options);
            obj.logMessage('doBandpass', 3, ['Bandpass filtering [' num2str(obj.lowedge) '-' num2str(obj.highedge) ']Hz ...']);
            % do bandpass filtering
            for i = 1:length(ALLEEG)
                try
                    clear EEG
                    EEG = ALLEEG(i);
                    EEG = pop_eegfiltnew( EEG, obj.lowedge, obj.highedge, [], 0);
                    [ALLEEG, ~, ~] = eeg_store( ALLEEG, EEG, i );
                catch
                    obj.logMessage('doBandpass', 5, [ALLEEG(i).filename ' failure ...']);
                end
            end
            obj.logMessage('doBandpass', 3, 'Bandpass filtering finished.');
        end
        
        function showBandpassPRM(obj)
            disp('------------------------------------');
            disp(['Bandpass filtering at [' num2str(obj.lowedge) '-' num2str(obj.highedge) ']Hz']);
            disp('------------------------------------');
        end
        
        function ALLEEG = doNotchfiltering(obj, ALLEEG, varargin)
            if ~isArgPaired(obj, varargin{:})
                return;
            end
            options = finputcheck( varargin, { ...
                'notch_low'     {'real' 'integer'}     [0 Inf]    obj.DEFAULT_notch_low; ...
                'notch_high'    {'real' 'integer'}     [0 Inf]    obj.DEFAULT_notch_high});
            if isstr(options), error(options); end;
            obj.setProperties(options);
            obj.logMessage('doNotchfiltering', 3, ['Notch filtering [' num2str(obj.notch_low) '-' num2str(obj.notch_high) ']Hz ...']);
            % do bandpass filtering
            for i = 1:length(ALLEEG)
                try
                    clear EEG
                    EEG = ALLEEG(i);
                    EEG = pop_eegfiltnew( EEG, obj.notch_low, obj.notch_high, [], 1, [], 0, 0);
                    [ALLEEG, ~, ~] = eeg_store( ALLEEG, EEG, i );
                catch
                    obj.logMessage('doNotchfiltering', 5, [ALLEEG(i).filename ' failure ...']);
                end
            end
            obj.logMessage('doNotchfiltering', 3, 'Notch filtering finished.');
        end
        
        function showNotchfilteringPRM(obj)
            disp('------------------------------------');
            disp(['Notch filtering at [' num2str(obj.notch_low) '-' num2str(obj.notch_high) ']Hz']);
            disp('------------------------------------');
        end
        
        function ALLEEG = doASR(obj,ALLEEG, varargin)
            if ~isArgPaired(obj, varargin{:})
                return;
            end
            options = finputcheck( varargin, { ...
                'enable_remove_flatline'           'boolean'       [0 1]          obj.DEFAULT_enable_remove_flatline; ...
                'flatline_crit'                   'real'          []            obj.DEFAULT_flatline_crit; ...
                'enable_highpass_filter'           'boolean'       [0 1]          obj.DEFAULT_enable_highpass_filter; ...
                'highpass_from'                   'real'          []            obj.DEFAULT_highpass_from; ...
                'highpass_end'                    'real'          []            obj.DEFAULT_highpass_end; ...
                'enable_remove_hopeless_channels'   'boolean'       [0 1]          obj.DEFAULT_enable_remove_hopeless_channels; ...
                'channel_crit'                    'real'          [0.4 0.6]      obj.DEFAULT_channel_crit; ...
                'channel_crit_excluded'            'real'          [0.1 0.3]      obj.DEFAULT_channel_crit_excluded; ...
                'channel_crit_maxbad_time'          'real'         [0.15 0.6]     obj.DEFAULT_channel_crit_maxbad_time; ...
                'enable_burst_repair'              'boolean'       [0 1]         obj.DEFAULT_enable_burst_repair; ...
                'burst_crit'                      'real'          [2.5 5]       obj.DEFAULT_burst_crit; ...
                'burst_crit_refmaxbadchns'          'real'         [0.05 0.3]     obj.DEFAULT_burst_crit_refmaxbadchns; ...
                'burst_crit_reftolerances_from'     'real'          []            obj.DEFAULT_burst_crit_reftolerances_from; ...
                'burst_crit_reftolerances_end'      'real'          []            obj.DEFAULT_burst_crit_reftolerances_end; ...
                'enable_remove_hopeless_time_window' 'boolean'       [0 1]         obj.DEFAULT_enable_remove_hopeless_time_window; ...
                'window_crit'                     'real'          [0.05 0.3]     obj.DEFAULT_window_crit; ...
                'window_crit_tolerances_lower'      'real'          []            obj.DEFAULT_window_crit_tolerances_lower; ...
                'window_crit_tolerances_upper'      'real'          []            obj.DEFAULT_window_crit_tolerances_upper});
            
            if isstr(options), error(options); end;
            obj.setProperties(options);
            % remove flat-line channels
            if obj.enable_remove_flatline
                obj.logMessage('doASR', 3, '[Remove flat-line] enabled!');
                obj.logMessage('doASR', 3, ['*** flatline_crit:' num2str(obj.flatline_crit)]);
            else
                obj.logMessage('doASR', 3, '[Remove flat-line] disabled!');
            end
            % high-pass
            if obj.enable_highpass_filter
                obj.logMessage('doASR', 3, '[Highpass filter] enabled!');
                obj.logMessage('doASR', 3, ['*** highpass: [' num2str(obj.highpass_from,'%2.2f') '-' num2str(obj.highpass_end,'%2.2f') ']Hz']);
            else
                obj.logMessage('doASR', 3, '[Highpass filter] disabled!');
            end
            % remove hopeless channels
            if obj.enable_remove_hopeless_channels
                obj.logMessage('doASR', 3, '[Remove hopeless channel] enabled!');
                obj.logMessage('doASR', 3, ['*** channel_crit:' num2str(obj.channel_crit)]);
                obj.logMessage('doASR', 3, ['*** channel_crit_excluded:' num2str(obj.channel_crit_excluded)]);
                obj.logMessage('doASR', 3, ['*** channel_crit_maxbad_time:' num2str(obj.channel_crit_maxbad_time)]);
            else
                obj.logMessage('doASR', 3, '[Remove hopeless channel] disabled!');
            end
            % repair bursts (It seems like that this function needs data exceeds 10s, or it cannot proceed)
            if obj.enable_burst_repair
                obj.logMessage('doASR', 3, '[Repaire burst] enabled!');
                obj.logMessage('doASR', 3, ['*** burst_crit:' num2str(obj.burst_crit)]);
                obj.logMessage('doASR', 3, ['*** burst_crit_refmaxbadchns:' num2str(obj.burst_crit_refmaxbadchns)]);
                obj.logMessage('doASR', 3, ['*** burst_crit_reftolerances: [' num2str(obj.burst_crit_reftolerances_from) ' ' num2str(obj.burst_crit_reftolerances_end) ']']);
            else
                obj.logMessage('doASR', 3, '[Repaire burst] disabled!');
            end
            % remove hopeless time windows
            if obj.enable_remove_hopeless_time_window
                obj.logMessage('doASR', 3, '[Remove hopeless time window] enabled!');
                obj.logMessage('doASR', 3, ['*** window_crit:' num2str(obj.window_crit)]);
                obj.logMessage('doASR', 3, ['*** window_crit_tolerances: [' num2str(obj.window_crit_tolerances_lower) ' ' num2str(obj.window_crit_tolerances_upper) ']']);
            else
                obj.logMessage('doASR', 3, '[Remove hopeless time window] disabled!');
            end
            obj.logMessage('doASR', 3, 'ASR processing ...');
            
            % do bandpass filtering
            % remove flat-line channels
            for i = 1:length(ALLEEG)
                if obj.enable_remove_flatline
                    try
                        clear EEG
                        EEG = ALLEEG(i);
                        EEG = clean_flatlines(EEG, obj.flatline_crit);
                        [ALLEEG, ~, ~] = eeg_store( ALLEEG, EEG, i );
                    catch
                        obj.logMessage('doASR', 5, ['Removing flat-line failure: ' ALLEEG(i).filename]);
                    end
                end
                % high-pass
                if obj.enable_highpass_filter
                    try
                        clear EEG
                        EEG = ALLEEG(i);
                        EEG = clean_drifts(EEG, [obj.highpass_from obj.highpass_end]);
                        [ALLEEG, ~, ~] = eeg_store( ALLEEG, EEG, i );
                    catch
                        obj.logMessage('doASR', 5, ['Highpass filtering failure: ' ALLEEG(i).filename]);
                    end
                end
                % remove hopeless channels
                if obj.enable_remove_hopeless_channels
                    try
                        clear EEG
                        EEG = ALLEEG(i);
                        EEG = clean_channels(EEG, obj.channel_crit, obj.channel_crit_excluded, [], obj.channel_crit_maxbad_time);
                        [ALLEEG, ~, ~] = eeg_store( ALLEEG, EEG, i );
                    catch
                        obj.logMessage('doASR', 5, ['Removing hopeless channel failure: ' ALLEEG(i).filename]);
                    end
                end
                % repair bursts (It seems like that this function needs data exceeds 10(15?)s, or it cannot proceed)
                if obj.enable_burst_repair
                    try
                        clear EEG
                        EEG = ALLEEG(i);
                        EEG = repair_bursts(EEG, obj.burst_crit, [], [], [], obj.burst_crit_refmaxbadchns, ...
                            [obj.burst_crit_reftolerances_from obj.burst_crit_reftolerances_end], []);
                        [ALLEEG, ~, ~] = eeg_store( ALLEEG, EEG, i );
                    catch
                        
                        if EEG.pnts / EEG.srate < 15
                            obj.logMessage('doASR', 5, ['Repairing burst failure: ' ALLEEG(i).filename ', time < 15sec!']);
                        else
                            obj.logMessage('doASR', 5, ['Repairing burst failure: ' ALLEEG(i).filename]);
                        end
                    end
                end
                % remove hopeless time windows
                if obj.enable_remove_hopeless_time_window
                    try
                        clear EEG
                        EEG = ALLEEG(i);
                        EEG = clean_windows(EEG, obj.window_crit, [obj.window_crit_tolerances_lower obj.window_crit_tolerances_upper]);
                        [ALLEEG, ~, ~] = eeg_store( ALLEEG, EEG, i );
                    catch
                        obj.logMessage('doASR', 5, ['Removing hopeless time window failure: ' ALLEEG(i).filename]);
                    end
                end
                
            end
            obj.logMessage('doASR', 3, 'ASR processing finished.');
        end
        
        function showASRPRM(obj)
            disp('------------------------------------');
            % remove flat-line channels
            if obj.enable_remove_flatline
                disp('[Remove flat-line] enabled!');
            else
                disp('[Remove flat-line] disabled!');
            end
            disp(['*** flatline_crit:' num2str(obj.flatline_crit)]);
            % high-pass
            if obj.enable_highpass_filter
                disp('[Highpass filter] enabled!');
            else
                disp('[Highpass filter] disabled!');
            end
            disp(['*** highpass: [' num2str(obj.highpass_from,'%2.2f') '-' num2str(obj.highpass_end,'%2.2f') ']Hz']);
            % remove hopeless channels
            if obj.enable_remove_hopeless_channels
                disp('[Remove hopeless channel] enabled!');
            else
                disp('[Remove hopeless channel] disabled!');
            end
            disp(['*** channel_crit:' num2str(obj.channel_crit)]);
            disp(['*** channel_crit_excluded:' num2str(obj.channel_crit_excluded)]);
            disp(['*** channel_crit_maxbad_time:' num2str(obj.channel_crit_maxbad_time)]);
            % repair bursts (It seems like that this function needs data exceeds 10s, or it cannot proceed)
            if obj.enable_burst_repair
                disp('[Repaire burst] enabled!');
            else
                disp('[Repaire burst] disabled!');
            end
            disp(['*** burst_crit:' num2str(obj.burst_crit)]);
            disp(['*** burst_crit_refmaxbadchns:' num2str(obj.burst_crit_refmaxbadchns)]);
            disp(['*** burst_crit_reftolerances: [' num2str(obj.burst_crit_reftolerances_from) ' ' num2str(obj.burst_crit_reftolerances_end) ']']);
            % remove hopeless time windows
            if obj.enable_remove_hopeless_time_window
                disp('[Remove hopeless time window] enabled!');
            else
                disp('[Remove hopeless time window] disabled!');
            end
            disp(['*** window_crit:' num2str(obj.window_crit)]);
            disp(['*** window_crit_tolerances: [' num2str(obj.window_crit_tolerances_lower) ' ' num2str(obj.window_crit_tolerances_upper) ']']);
            disp('------------------------------------');
        end
        
        function plotPSD(obj, ALLEEG, varargin)
            if ~isArgPaired(obj, varargin{:})
                return;
            end
            options = finputcheck( varargin, { ...
                'title_comp'      {'cell' 'string'}           []        obj.DEFAULT_title_comp; ...
                'normalize_data'       'boolean'              [0 1]     obj.DEFAULT_normalize_data; ...
                'NW'                  {'real' 'integer'}     [0 Inf]    obj.DEFAULT_NW; ...
                'sync_avg_segment_sec'   {'real' 'integer'}    [0 Inf]    obj.DEFAULT_sync_avg_segment_sec; ...
                'overlap_fraction'    {'real' 'integer'}       [0 1]     obj.DEFAULT_overlap_fraction; ...
                'enable_auto_x_axis'    'boolean'             [0 1]      obj.DEFAULT_enable_auto_x_axis; ...
                'x_min'               'real'                []         obj.DEFAULT_x_min; ...
                'x_max'               'real'                []         obj.DEFAULT_x_max; ...
                'enable_auto_y_axis'    'boolean'             [0 1]      obj.DEFAULT_enable_auto_y_axis; ...
                'y_min'               'real'                []         obj.DEFAULT_y_min; ...
                'y_max'               'real'                []         obj.DEFAULT_y_max; ...
                'enable_CCA'           'boolean'             [0 1]      obj.DEFAULT_enable_CCA; ...
                'use_sine_ref'         'boolean'             [0 1]      obj.DEFAULT_use_sine_ref; ...
                'use_cosine_ref'       'boolean'             [0 1]       obj.DEFAULT_use_cosine_ref});
            if isstr(options), error(options); end;
            if options.overlap_fraction == 1; error('It makes no sense that overlap fraction = 1!'); end;
            obj.setProperties(options);
            if ~obj.enable_auto_x_axis
                obj.logMessage('plotPSD', 3, ['*** Set X axis: [' num2str(obj.x_min) '-' num2str(obj.x_max) '] Hz']);
            end
            if ~obj.enable_auto_y_axis
                obj.logMessage('plotPSD', 3, ['*** Set Y axis: [' num2str(obj.y_min) '-' num2str(obj.y_max) '] dB']);
            end
            if obj.normalize_data
                obj.logMessage('plotPSD', 3, 'PSD data is normalized(zscore)!');
            end
            if obj.enable_CCA
                ref_str = [];
                if obj.use_sine_ref && obj.use_cosine_ref
                    ref_str = 'sine & cosine';
                elseif obj.use_sine_ref && ~obj.use_cosine_ref
                    ref_str = 'sine';
                elseif ~obj.use_sine_ref && obj.use_cosine_ref
                    ref_str = 'cosine';
                end
                obj.logMessage('plotPSD', 3, ['Using CCA result to plot PSD, reference signal with ' ref_str ' function']);
                if ~obj.use_sine_ref && ~obj.use_cosine_ref
                    error('Using CCA without giving reference signal!');
                end
            end
            obj.logMessage('plotPSD', 3, 'Plotting PSD figures ...');
            % plot
            for i = 1:length(ALLEEG)
                
                %% CCA Processing
                if obj.enable_CCA
                    fprintf('Doing CCA Analysis...\n');
                    
                    titleComp = obj.getTitleComp(ALLEEG(i), obj.title_comp);
                    segmentSec = obj.sync_avg_segment_sec;
                    overlapFct = obj.overlap_fraction;
                    dataSegmentSec = ALLEEG(i).pnts/ALLEEG(i).srate;
                    if segmentSec == 0; segmentSec = dataSegmentSec; end;
                    if segmentSec*obj.srate > ALLEEG(i).pnts
                        obj.logMessage('plotPSD', 5, [ALLEEG(i).filename ' length < Sync. Avg. segment(' num2str(segmentSec) 'sec)']);
                        continue;
                    end
                    % if use normalized data
                    EEGdata = ALLEEG(i).data;
                    if obj.normalize_data
                        EEGdata =zscore(EEGdata')';
                        nstr = 'Zscored data';
                    else
                        nstr = [];
                    end
                    clear pxx f                    
                    
%                     % get VEP stimuli frequency
%                     try
%                         si = []; ei = [];
%                         [si,ei] = regexp(ALLEEG(i).meta.session.title, '(\d+[hH][zZ])');
%                     catch
%                         warning('Cannot find session.title in metadata!');
%                         obj.logMessage('plotPSD', 4, ['VEP stimuli frequency format error in metadata, file:' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
%                         continue
%                     end
%                     freq = str2double(ALLEEG(i).meta.session.title(si:ei-2));
%                     if isempty(si) && isempty(ei)
%                         warning('Not a VEP recording but [%s]!',ALLEEG(i).meta.session.title);
%                         obj.logMessage('plotPSD', 4, ['VEP stimuli frequency format error in metadata, file:' ...
%                             'Cannot find session.title in metadata or not a VEP recording(' ALLEEG(i).meta.session.title '), file:' ALLEEG(i).filepath ALLEEG(i).filename]);
%                         continue;
%                     end
                    %% CCA reference signal
%                     t = 0:1/ALLEEG(i).srate:ALLEEG(i).xmax;
%                     if obj.use_sine_ref, ref1 = sin(2*pi*freq*t); else ref1 = []; end;
%                     if obj.use_cosine_ref, ref2 = cos(2*pi*freq*t); else ref2 = []; end;
%                     REF = [ref1; ref2];
%                     % CCA
%                     [A,B,r,U,V] = canoncorr(EEGdata',REF');
%                     data = U(:,1);
                    
                    %% CCA (Jerry Version)
                    FREQUENCY_LIST = [8 : 1 : 11];
                    for j = 1 : length(FREQUENCY_LIST)
                        fprintf('CCA freq=%d\n', FREQUENCY_LIST(j));
                        f = FREQUENCY_LIST(j);
                        [~, B, r, U, ~, awx, ~] = CCA_normal(EEGdata', f);
%                         betas = cat(1, betas, B(:, 1)');
%                         rel = cat(1, rel, r(1));
%                         kkk = get_snr(awx(:, 1), f, FS);
%                         SNR_ = cat(1, SNR_, kkk);
%                         new_result(:, j) = awx(:, 1);  
                        
                        data = U(:,1);
                        
                        
                        %% Plot a figure for each frequency
                        try
                            for idx = uint32(1:segmentSec*(1-overlapFct)*obj.srate:(dataSegmentSec-segmentSec)*obj.srate)
                                segment_num = ceil((idx-1)/(segmentSec*(1-overlapFct)*obj.srate)+1);
                                [pxx(segment_num,:),f(segment_num,:)] = pmtm(data(idx:idx+segmentSec*obj.srate-1),obj.NW,segmentSec*obj.srate,obj.srate);
                            end
                            pxxdb=10*log10(pxx);
                            Q2 = median(pxxdb,1);
                            Q3 = prctile(pxxdb,75);
                            Q1 = prctile(pxxdb,25);

                            fHandler = figure(5566);set(fHandler,'visible','off');hold on
                            freq_reso = 1/segmentSec;
                            x_axis = 0:freq_reso:obj.srate/2;
                            hPlot = plot(f,Q2,'k');
                            if segmentSec ~= dataSegmentSec
                                hShade = fill([x_axis fliplr(x_axis)],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.7);
                                LG = legend([hShade(1) hPlot(1)],'IQR','Median');set(LG,'fontsize',12);
                            end

                            if ~obj.enable_auto_x_axis, xlim([obj.x_min obj.x_max]);end
                            if ~obj.enable_auto_y_axis, ylim([obj.y_min obj.y_max]);end
                            grid on;
                            title([nstr 'PSD(multitaper) / Sync. Avg. (' num2str(segmentSec) ' sec)' char(10) titleComp ' / CCA result(r = ' num2str(r(1,1),'%0.2f') ')'],...
                                'fontsize',12,'interpreter','none');
                            xlabel('Frequency (Hz)','fontsize',14);
                            ylabel('Power (dB/Hz)','fontsize',14);

                            try
                                oDir = obj.getHierarchyDir(ALLEEG(i).filepath);
                            catch
                                error('Cannot get hierarchy directory!');
                            end;
                            fileDir = [oDir '\' nstr 'PSD (' num2str(segmentSec) 'sec segment, ' num2str(obj.overlap_fraction*100) '% overlap)\' ALLEEG(i).filename '(' titleComp ')\'];
                            fileName = 'CCA result';
                            if ~exist(fileDir,'dir'), mkdir(fileDir);end
                            obj.logMessage('plotPSD', 3, [fileDir fileName]);
                            print(fHandler, '-dtiff','-r200',[fileDir fileName '.tiff']);
                            saveas(fHandler,[fileDir fileName '.fig']);
                            close(fHandler)
                        catch
                            obj.logMessage('plotPSD', 5, [ALLEEG(i).filename ' / CCA result failure ...']);
                        end                        
                    end                    
                    
                    
%                     for i = 1 : size(data, 3)
%                         betas = [];
%                         rel = [];
%                         SNR_ = [];
%                         new_result = zeros(size(data, 1), length(FREQUENCY_LIST));
%                         for j = 1 : length(FREQUENCY_LIST)
%                             fprintf('CCA freq=%d\n', j);
%                             f = FREQUENCY_LIST(j);
%                             [~, B, r, ~, ~, awx, ~] = CCA_normal(data(:, :, i), f);
%                             betas = cat(1, betas, B(:, 1)');
%                             rel = cat(1, rel, r(1));
%                             kkk = get_snr(awx(:, 1), f, FS);
%                             SNR_ = cat(1, SNR_, kkk);
%                             new_result(:, j) = awx(:, 1);  
%                         end
%                         result(i, :, :) = bp(new_result, FS);
%                         epoch_betas(i, :, :) = betas;
%                         correlation(i, :) = rel;
%                         SNR(i, :) = SNR_;
%                     end                 
                    
%                     %% Plot
%                     try
%                         for idx = uint32(1:segmentSec*(1-overlapFct)*obj.srate:(dataSegmentSec-segmentSec)*obj.srate)
%                             segment_num = ceil((idx-1)/(segmentSec*(1-overlapFct)*obj.srate)+1);
%                             [pxx(segment_num,:),f(segment_num,:)] = pmtm(data(idx:idx+segmentSec*obj.srate-1),obj.NW,segmentSec*obj.srate,obj.srate);
%                         end
%                         pxxdb=10*log10(pxx);
%                         Q2 = median(pxxdb,1);
%                         Q3 = prctile(pxxdb,75);
%                         Q1 = prctile(pxxdb,25);
%                         
%                         fHandler = figure(5566);set(fHandler,'visible','off');hold on
%                         freq_reso = 1/segmentSec;
%                         x_axis = 0:freq_reso:obj.srate/2;
%                         hPlot = plot(f,Q2,'k');
%                         if segmentSec ~= dataSegmentSec
%                             hShade = fill([x_axis fliplr(x_axis)],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.7);
%                             LG = legend([hShade(1) hPlot(1)],'IQR','Median');set(LG,'fontsize',12);
%                         end
%                         
%                         if ~obj.enable_auto_x_axis, xlim([obj.x_min obj.x_max]);end
%                         if ~obj.enable_auto_y_axis, ylim([obj.y_min obj.y_max]);end
%                         grid on;
%                         title([nstr 'PSD(multitaper) / Sync. Avg. (' num2str(segmentSec) ' sec)' char(10) titleComp ' / CCA result(r = ' num2str(r(1,1),'%0.2f') ')'],...
%                             'fontsize',12,'interpreter','none');
%                         xlabel('Frequency (Hz)','fontsize',14);
%                         ylabel('Power (dB/Hz)','fontsize',14);
%                         
%                         try
%                             oDir = obj.getHierarchyDir(ALLEEG(i).filepath);
%                         catch
%                             error('Cannot get hierarchy directory!');
%                         end;
%                         fileDir = [oDir '\' nstr 'PSD (' num2str(segmentSec) 'sec segment, ' num2str(obj.overlap_fraction*100) '% overlap)\' ALLEEG(i).filename '(' titleComp ')\'];
%                         fileName = 'CCA result';
%                         if ~exist(fileDir,'dir'), mkdir(fileDir);end
%                         obj.logMessage('plotPSD', 3, [fileDir fileName]);
%                         print(fHandler, '-dtiff','-r200',[fileDir fileName '.tiff']);
%                         saveas(fHandler,[fileDir fileName '.fig']);
%                         close(fHandler)
%                     catch
%                         obj.logMessage('plotPSD', 5, [ALLEEG(i).filename ' / CCA result failure ...']);
%                     end
                end                
                
                
                % skip plotting if it's oddball recording
                try
                    if ~isempty(regexp(ALLEEG(i).meta.session.title, '([oO][dD][dD].*[bB][aA][lL][lL])', 'once'))
                        warning('Skip plotting since it is an oddball recording!');
                        obj.logMessage('plotPSD', 4, ['Skip plotting since it is an oddball recording, file:' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                        continue;
                    end
                catch
                    warning('Cannot find session.title in metadata!');
                    continue
                end
                
                titleComp = obj.getTitleComp(ALLEEG(i), obj.title_comp);
                segmentSec = obj.sync_avg_segment_sec;
                overlapFct = obj.overlap_fraction;
                dataSegmentSec = ALLEEG(i).pnts/ALLEEG(i).srate;
                if segmentSec == 0; segmentSec = dataSegmentSec; end;
                if segmentSec*obj.srate > ALLEEG(i).pnts
                    obj.logMessage('plotPSD', 5, [ALLEEG(i).filename ' length < Sync. Avg. segment(' num2str(segmentSec) 'sec)']);
                    continue;
                end
                % if use normalized data
                EEGdata = ALLEEG(i).data;
                if obj.normalize_data
                    EEGdata =zscore(EEGdata')';
                    nstr = 'Zscored data';
                else
                    nstr = [];
                end
                clear pxx f
                for j = 1:ALLEEG(i).nbchan
                    data = EEGdata(j,:);
                    try
                        for idx = uint32(1:segmentSec*(1-overlapFct)*obj.srate:(dataSegmentSec-segmentSec)*obj.srate)
                            segment_num = ceil((idx-1)/(segmentSec*(1-overlapFct)*obj.srate)+1);
                            [pxx(segment_num,:),f(segment_num,:)] = pmtm(data(idx:idx+segmentSec*obj.srate-1),obj.NW,segmentSec*obj.srate,obj.srate);
                        end
                        pxxdb=10*log10(pxx);
                        Q2 = median(pxxdb,1);
                        Q3 = prctile(pxxdb,75);
                        Q1 = prctile(pxxdb,25);
                        
                        fHandler = figure(5566);set(fHandler,'visible','off');;hold on
                        freq_reso = 1/segmentSec;
                        x_axis = 0:freq_reso:obj.srate/2;
                        hPlot = plot(f,Q2,'k');
                        if segmentSec ~= dataSegmentSec
                            hShade = fill([x_axis fliplr(x_axis)],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.7);
                            LG = legend([hShade(1) hPlot(1)],'IQR','Median');set(LG,'fontsize',12);
                        end
                        
                        if ~obj.enable_auto_x_axis, xlim([obj.x_min obj.x_max]);end
                        if ~obj.enable_auto_y_axis, ylim([obj.y_min obj.y_max]);end
                        if isempty(ALLEEG(i).chanlocs), chLabel = ['Ch' int2str(j)]; else chLabel = ALLEEG(i).chanlocs(j).labels; end
                        grid on;
                        title([nstr ' PSD(multitaper) / Sync. Avg. (' num2str(segmentSec) ' sec)' char(10) titleComp ' / ' chLabel ],...
                            'fontsize',12,'interpreter','none');
                        xlabel('Frequency (Hz)','fontsize',14);
                        ylabel('Power (dB/Hz)','fontsize',14);
                        
                        try
                            oDir = obj.getHierarchyDir(ALLEEG(i).filepath);
                        catch
                            error('Cannot get hierarchy directory!');
                        end;
                        fileDir = [oDir '\' nstr 'PSD (' num2str(segmentSec) 'sec segment, ' num2str(obj.overlap_fraction*100) '% overlap)\' ALLEEG(i).filename '(' titleComp ')\'];
                        fileName = chLabel;
                        if ~exist(fileDir,'dir'), mkdir(fileDir);end
                        obj.logMessage('plotPSD', 3, [fileDir fileName]);
                        print(fHandler,'-dtiff','-r200',[fileDir fileName '.tiff']);
                        saveas(fHandler,[fileDir fileName '.fig']);
                        close(fHandler)
                    catch
                        obj.logMessage('plotPSD', 5, [ALLEEG(i).filename ' / ' chLabel ' failure ...']);
                    end
                end
                

            end
            obj.logMessage('plotPSD', 3, 'Plotting PSD finished.');
        end
        
        function plotWaveform(obj, ALLEEG, varargin)
            if ~isArgPaired(obj, varargin{:})
                return;
            end
            options = finputcheck( varargin, { ...
                'title_comp'   {'cell' 'string'} []    obj.DEFAULT_title_comp; ...
                'window_sec'      'integer'      []      obj.DEFAULT_window_sec; ...
                'normalized_output' 'boolean'     [0 1]   obj.DEFAULT_normalized_output; ...
                'normalized_offset' 'real'       []       obj.DEFAULT_normalized_offset});
            if options.window_sec == 0; error('Display window length = 0!'); end
            if isstr(options), error(options); end;
            obj.setProperties(options);
            obj.logMessage('plotWaveform', 3, ['*** Data segment length(sec): ' num2str(obj.window_sec)]);
            obj.logMessage('plotWaveform', 3, ['*** Normalized output: ' num2str(obj.normalized_output)]);
            obj.logMessage('plotWaveform', 3, ['*** Normalized offset: ' num2str(obj.normalized_offset)]);
            obj.logMessage('plotWaveform', 3, 'Plotting waveform figures ...');
            % plot
            for i = 1:length(ALLEEG)
                % skip plotting if it's oddball recording
                try
                    if ~isempty(regexp(ALLEEG(i).meta.session.title, '([oO][dD][dD].*[bB][aA][lL][lL])', 'once'))
                        warning('Skip plotting since it is an oddball recording!');
                        obj.logMessage('plotPSD', 4, ['Skip plotting since it is an oddball recording, file:' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                        continue;
                    end
                catch
                    warning('Cannot find session.title in metadata!');
                    continue
                end
                
                try
                    titleComp = obj.getTitleComp(ALLEEG(i), obj.title_comp);
                    segmentSec = obj.window_sec;
                    dataSegmentSec = ALLEEG(i).pnts/ALLEEG(i).srate;
                    if segmentSec == 0 segmentSec = dataSegmentSec;end
                    if isempty(ALLEEG(i).chanlocs), chLabel = strcat('Ch', cellstr(num2str([1:ALLEEG(i).nbchan]'))); else chLabel = {ALLEEG(i).chanlocs.labels}; end
                    for idx = 1:segmentSec*obj.srate:dataSegmentSec*obj.srate
                        segment_num = (idx-1)/(segmentSec*obj.srate)+1;
                        if idx+segmentSec*obj.srate-1 > ALLEEG(i).pnts
                            data = [ALLEEG(i).data(:,idx:end) nan(ALLEEG(i).nbchan, segmentSec*obj.srate-size(ALLEEG(i).data(:,idx:end),2))];
                        else
                            data = ALLEEG(i).data(:,idx:idx+segmentSec*obj.srate-1);
                        end
                        % distance between channels
                        offset = mean(max(ALLEEG(i).data, [], 2)-min(ALLEEG(i).data, [], 2));
                        offsetMat = repmat(fliplr(0:ALLEEG(i).nbchan-1)'*offset, 1, segmentSec*obj.srate);
                        xData = repmat((1:segmentSec*obj.srate)/obj.srate, ALLEEG(i).nbchan, 1)';
                        if obj.normalized_output
                            offset = obj.normalized_offset;
                            offsetMat = repmat(fliplr(0:offset:offset*(ALLEEG(i).nbchan-1))', 1, segmentSec*obj.srate);
                            data = zscore(data')';
                            normalized_str = 'Normalized ';
                        else
                            normalized_str = '';
                        end
                        yData = [data + double(offsetMat)]';
                        fHandler = figure(7788);set(fHandler,'visible','off');
                        plot(xData, yData)
                        grid on
                        text(xData(end,:), double(offsetMat(:,1)), chLabel,'fontsize',14)
                        
                        title([normalized_str 'Waveform / ' num2str((segment_num-1)*segmentSec) '~' num2str(segment_num*segmentSec) 'sec' ,...
                            char(10) titleComp],'fontsize',12,'interpreter','none');
                        xlim([0 segmentSec]);
                        set(gca,'xticklabel',get(gca,'xtick')+(segment_num-1)*segmentSec)
                        xlabel('Time (sec)','fontsize',12);
                        ylabel('Amplitude (uV)','fontsize',12);
                        ylim([-offset ALLEEG(i).nbchan*offset])
                        
                        try
                            oDir = obj.getHierarchyDir(ALLEEG(i).filepath);
                        catch
                            error('Cannot get hierarchy directory!');
                        end;
                        fileDir = [oDir '\' normalized_str 'Waveform (' num2str(segmentSec) 'sec segment)\' ALLEEG(i).filename '(' titleComp ')\'];
                        fileName = [num2str((segment_num-1)*segmentSec) '~' num2str(segment_num*segmentSec) 'sec'];
                        if ~exist(fileDir,'dir') mkdir(fileDir);end
                        obj.logMessage('plotWaveform', 3, [fileDir fileName]);
                        print(fHandler, '-dtiff','-r200',[fileDir fileName '.tiff']);
                        saveas(fHandler,[fileDir fileName '.fig']);
                        close(fHandler)
                    end
                catch
                    obj.logMessage('plotWaveform', 5, [ALLEEG(i).filename ' / segment' num2str(segment_num) ' failure ...']);
                end
            end
            obj.logMessage('plotWaveform', 3, 'Plotting waveform finished.');
        end
        
        function plotOddball(obj, ALLEEG, varargin)
            if ~isArgPaired(obj, varargin{:})
                return;
            end
            options = finputcheck( varargin, { ...
                'plot_each_channel_TvsNT'     'boolean'  [0 1]    obj.DEFAULT_plot_each_channel_TvsNT; ...
                'plot_ERP_image'             'boolean'  [0 1]    obj.DEFAULT_plot_ERP_image; ...
                'plot_ERP_amplitude_min'      'real'      []     obj.DEFAULT_plot_ERP_amplitude_min; ...
                'plot_ERP_amplitude_max'      'real'      []     obj.DEFAULT_plot_ERP_amplitude_max; ...
                'plot_ERP_smoothing'          'real'      []     obj.DEFAULT_plot_ERP_smoothing; ...
                'oddball_x_min'             'real'      []     obj.DEFAULT_oddball_x_min; ...
                'oddball_x_max'             'real'      []     obj.DEFAULT_oddball_x_max; ...
                'oddball_y_min'             'real'      []     obj.DEFAULT_oddball_y_min; ...
                'oddball_y_max'             'real'      []     obj.DEFAULT_oddball_y_max; ...
                'plot_ERP_caxis_min'         'real'      []     obj.DEFAULT_plot_ERP_caxis_min; ...
                'plot_ERP_caxis_max'         'real'      []     obj.DEFAULT_plot_ERP_caxis_max});
            if isstr(options), error(options); end;
            obj.setProperties(options);
            if obj.plot_each_channel_TvsNT
                obj.logMessage('plotOddball', 3, 'Enable plot each channel target v.s non-target.'); end
            if obj.plot_ERP_image
                obj.logMessage('plotOddball', 3, 'Enable plot ERP image.');
                obj.logMessage('plotOddball', 3, ['*** Trial amplitude is limited within [' num2str(obj.plot_ERP_amplitude_min) ' ' num2str(obj.plot_ERP_amplitude_max) ']uV']);
                obj.logMessage('plotOddball', 3, ['*** Smoothing trials with ' num2str(obj.plot_ERP_smoothing) 'Hz']);
                obj.logMessage('plotOddball', 3, ['*** Time range of ERP image: ' num2str(obj.oddball_x_min) 'ms ~ ' num2str(obj.oddball_x_max) 'ms']);
                obj.logMessage('plotOddball', 3, ['*** Y-axis of ERP image: ' num2str(obj.oddball_y_min) 'uV ~ ' num2str(obj.oddball_y_max) 'uV']);
                obj.logMessage('plotOddball', 3, ['*** Color bar range of ERP image: [' num2str(obj.plot_ERP_caxis_min) ' ' num2str(obj.plot_ERP_caxis_max) ']']);
            end
            
            % collect oddball dataset
            ODDBALL_EEG = []; TOTAL_TRIAL = []; TOTAL_SEQ = [];
            for i = 1:length(ALLEEG)
                % skip any non-oddball dataset
                try
                    if isempty(regexp(ALLEEG(i).meta.session.title, '([oO][dD][dD].*[bB][aA][lL][lL])', 'once'))
                        warning('Not an oddball recording!');
                        continue;
                    else
                        obj.logMessage('plotOddball', 4, ['Oddball recording file fected:' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    end
                catch
                    warning(['Cannot find session.title from metadata in ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    obj.logMessage('plotOddball', 4, ['Cannot find session.title from metadata in ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    continue;
                end
                % fetch the sequence file
                [path, name, ~] = fileparts(fullfile(ALLEEG(i).filepath, ALLEEG(i).filename));
                fileID = fopen(fullfile(path, [name '_seq.txt']));
                if fileID < 0
                    warning(['Cannot reach the oddball sequence file of ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    obj.logMessage('plotOddball', 4, ['Cannot reach the oddball sequence file of ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    continue;
                end
                % read the sequences
                temp = textscan(fileID, '%s');
                temp = cell2mat(temp{1});
                temp = strsplit(temp, ',');
                seq = str2double(temp(find(~ismember(temp, {''}))))';
                ALLEEG(i).meta.oddball_seq = seq;
%                 %% MAKE UP PHOTOSENSOR DATA
%                 ALLEEG(i).frame_transition = zeros(size(ALLEEG(i).frame_transition));
%                 ALLEEG(i).frame_transition(500:550:27990) = 1;

                % obtain onset index
%                 onset_index = find(ALLEEG(i).frame_transition==1);
                [peak,onset_index]=findpeaks(ALLEEG(i).frame_transition,'MinPeakDistance',2);
                
                if length(onset_index) ~= length(seq)
                    warning(['The number of onset marker count does not match the number of oddball sequence in ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    obj.logMessage('plotOddball', 4, ['The number of onset marker count does not match the number of oddball sequence in ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    continue;
                end
                % extract continuous data into trails based on the onset index
                Xaxis_pnts = [obj.oddball_x_min obj.oddball_x_max]/1000*ALLEEG(i).srate;
                Xaxis_time = Xaxis_pnts/ALLEEG(i).srate;
                try
                    trials = nan(ALLEEG(i).nbchan, diff(Xaxis_pnts), length(onset_index));
                    for j = 1:length(onset_index)
                        trials(:, :, j) = ALLEEG(i).data(:, onset_index(j)+Xaxis_pnts(1):onset_index(j)+Xaxis_pnts(2)-1);
                    end
                catch
                    warning(['Accessing index out of boundry when extracting trials from ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    obj.logMessage('plotOddball', 4, ['Accessing index out of boundry when extracting trials from ' ALLEEG(i).filepath filesep ALLEEG(i).filename]);
                    continue;
                end
                % create a new EEG struct which contains trial data instead
                % of continuous data, and inherite metadata from the old one.
                EEG_TRIAL = pop_importdata('dataformat','array','nbchan',ALLEEG(i).nbchan,'data',trials,'srate',ALLEEG(i).srate,'pnts',diff(Xaxis_pnts),'xmin',Xaxis_time(1),'chanlocs',ALLEEG(i).chanlocs);
                EEG_TRIAL.meta = ALLEEG(i).meta;
                EEG_TRIAL.setname = ALLEEG(i).setname;
                EEG_TRIAL.filename = ALLEEG(i).filename;
                EEG_TRIAL.filepath = ALLEEG(i).filepath;
                EEG_TRIAL.subject = ALLEEG(i).subject;
                EEG_TRIAL.comments = ALLEEG(i).comments;
                
                [ODDBALL_EEG, ~, ~] = eeg_store( ODDBALL_EEG, EEG_TRIAL, 0);
                TOTAL_TRIAL = cat(3, TOTAL_TRIAL, EEG_TRIAL.data);
                TOTAL_SEQ = [TOTAL_SEQ EEG_TRIAL.meta.oddball_seq];
            end
            if isempty(TOTAL_TRIAL) || isempty(TOTAL_SEQ)
                warning('The value of Oddball trial/sequence equals to zero!')
                return;
            end
            ODDBALL = pop_importdata('dataformat','array','nbchan',ODDBALL_EEG(end).nbchan,'data',TOTAL_TRIAL,'srate',ODDBALL_EEG(end).srate,'pnts',diff(Xaxis_pnts),'xmin',Xaxis_time(1),'chanlocs',ODDBALL_EEG(end).chanlocs);
            % use baseline (points before the marker) to remove DC offsets
            if Xaxis_time(1) == 0
                ODDBALL = pop_rmbase( ODDBALL, []);
            else
                ODDBALL = pop_rmbase( ODDBALL, [Xaxis_time(1)*1000 0]);
            end

            target = TOTAL_TRIAL(:, :, TOTAL_SEQ==1);
            nontarget = TOTAL_TRIAL(:, :, TOTAL_SEQ==0);
            % remove extreme amplitude trials
            TBD = [];
            for t = 1:size(target, 3)
                if any(any(target(:,:,t) < obj.plot_ERP_amplitude_min)) || any(any(target(:,:,t) > obj.plot_ERP_amplitude_max))
                    TBD = [TBD t]; end;
            end;
            target_left_percentage = (1-length(TBD)/size(target, 3))*100;
            target(:,:,TBD) = []; 
            TBD = [];
            for t = 1:size(nontarget, 3)
                if any(any(nontarget(:,:,t) < obj.plot_ERP_amplitude_min)) || any(any(nontarget(:,:,t) > obj.plot_ERP_amplitude_max))
                    TBD = [TBD t]; end;
            end;
            nontarget_left_percentage = (1-length(TBD)/size(nontarget, 3))*100;
            nontarget(:,:,TBD) = [];
            % validation check
            if isempty(target) || isempty(nontarget)
                warning('Too aggresive for removing extreme value trials. Please adjust the amplitude value and retry.');
                obj.logMessage('plotOddball', 4, 'Too aggresive for removing extreme value trials. Please adjust the amplitude value and retry.');
                return;
            end
            TARGET = pop_importdata('dataformat','array','nbchan',ODDBALL.nbchan,'data',target,'srate',ODDBALL.srate,'pnts',diff(Xaxis_pnts),'xmin',Xaxis_time(1),'chanlocs',ODDBALL.chanlocs);
            NONTARGET = pop_importdata('dataformat','array','nbchan',ODDBALL.nbchan,'data',nontarget,'srate',ODDBALL.srate,'pnts',diff(Xaxis_pnts),'xmin',Xaxis_time(1),'chanlocs',ODDBALL.chanlocs);
            % plot T vs NT
            if obj.plot_each_channel_TvsNT
                for ch = 1:ODDBALL.nbchan
                    h = figure(5566); set(h,'visible', 'off'); hold on;
                    Xaxis = [Xaxis_time(1):1/ODDBALL.srate:Xaxis_time(2)-1/ODDBALL.srate]*1000;
                    plot(Xaxis, mean(target(ch,:,:),3))
                    plot(Xaxis, mean(nontarget(ch,:,:),3), 'r')
                    
                    grid on;
                    set(gca, 'xlim', [Xaxis(1) Xaxis(end)], 'ylim', [obj.oddball_y_min obj.oddball_y_max], 'fontsize', 12);
                    xlabel('Time (ms)'); ylabel('\muV');
                    title(['Oddball Exp. / Sync. Avg. of ' ODDBALL.chanlocs(ch).labels], 'fontsize',12);
                    LG = legend('Target', 'Non-target'); set(LG, 'Location', 'SouthEast', 'fontsize',12);
                    
                    fileDir = [obj.outputDir '\Oddball plot - amplitude [' num2str(obj.plot_ERP_amplitude_min) '~' num2str(obj.plot_ERP_amplitude_max) ']uV, T(' num2str(target_left_percentage, '%2.0f') '%) v.s NT(' num2str(nontarget_left_percentage, '%2.0f') '%)\'];
                    fileName = ODDBALL.chanlocs(ch).labels;
                    if ~exist(fileDir,'dir') mkdir(fileDir);end
                    obj.logMessage('plotOddball', 3, ['Oddball plot: ' fileDir fileName]);
                    print(h, '-dtiff','-r200',[fileDir fileName '.tiff']);
                    saveas(h,[fileDir fileName '.fig']);
                    close(h)
                end
            end
            % plot ERP image
            if obj.plot_ERP_image
                for ch = 1:ODDBALL.nbchan
                    % target
                    h = figure(7788);
                    pop_erpimage(TARGET,1, [ch],[[]],['ERP / Target / ' ODDBALL.chanlocs(ch).labels ],obj.plot_ERP_smoothing,1,{},[],'' ...
                        ,'yerplabel','\muV','erp','on','limits',[obj.oddball_x_min obj.oddball_x_max obj.oddball_y_min obj.oddball_y_max ...
                        NaN NaN NaN NaN] ,'cbar','on','caxis',[obj.plot_ERP_caxis_min obj.plot_ERP_caxis_max] );
                    
                    fileDir = [obj.outputDir '\Oddball ERP - amplitude [' num2str(obj.plot_ERP_amplitude_min) '~' num2str(obj.plot_ERP_amplitude_max) ']uV, T(' num2str(target_left_percentage, '%2.0f') '%) v.s NT(' num2str(nontarget_left_percentage, '%2.0f') '%)\'];
                    fileName = ['Target_' ODDBALL.chanlocs(ch).labels];
                    if ~exist(fileDir,'dir') mkdir(fileDir);end
                    obj.logMessage('plotOddball', 3, ['Oddball ERP: ' fileDir fileName]);
                    print(h, '-dtiff','-r200',[fileDir fileName '.tiff']);
                    saveas(h,[fileDir fileName '.fig']);
                    close(h)
                    % nontarget
                    h = figure(7788);
                    pop_erpimage(NONTARGET,1, [ch],[[]],['ERP / Non-target / ' ODDBALL.chanlocs(ch).labels ],obj.plot_ERP_smoothing,1,{},[],'' ...
                        ,'yerplabel','\muV','erp','on','limits',[obj.oddball_x_min obj.oddball_x_max obj.oddball_y_min obj.oddball_y_max ...
                        NaN NaN NaN NaN] ,'cbar','on','caxis',[obj.plot_ERP_caxis_min obj.plot_ERP_caxis_max] );
                    
                    fileDir = [obj.outputDir '\Oddball ERP - amplitude [' num2str(obj.plot_ERP_amplitude_min) '~' num2str(obj.plot_ERP_amplitude_max) ']uV, T(' num2str(target_left_percentage, '%2.0f') '%) v.s NT(' num2str(nontarget_left_percentage, '%2.0f') '%)\'];
                    fileName = ['Non-target_' ODDBALL.chanlocs(ch).labels];
                    if ~exist(fileDir,'dir') mkdir(fileDir);end
                    obj.logMessage('plotOddball', 3, ['Oddball ERP: ' fileDir fileName]);
                    print(h, '-dtiff','-r200',[fileDir fileName '.tiff']);
                    saveas(h,[fileDir fileName '.fig']);
                    close(h)
                end
            end
            obj.logMessage('plotOddball', 3, 'Plotting Oddball finished.');
        end
    end
    
    methods (Access = private)
        function obj = init(obj, varargin)
            if ~isArgPaired(obj, varargin{:})
                return;
            end
            options = finputcheck( varargin, { ...
                'srate'                 {'real' 'integer'}       []      obj.DEFAULT_srate; ...
                'outputDir'             'string'                []      obj.DEFAULT_outputDir; ...
                'outputOrg'             'string'      {'hierarchy' 'flat'} obj.DEFAULT_outputOrg; ...
                'log_level'             {'real' 'integer'}      [0 7]    obj.DEFAULT_log_level; ...
                'command_window_level'    {'real' 'integer'}     [0 7]     obj.DEFAULT_command_window_level});
            if isstr(options), error(options); end;
            obj.setProperties(options);
            %% initial logging tool
            time_now_str = num2str(datestr(now, 'yyyy-mm-dd HHMMSS'));
            obj.outputDir = fullfile(obj.outputDir,['Analysis(' time_now_str ')']);
            if ~exist(obj.outputDir, 'dir'), mkdir(obj.outputDir); end;
            
            obj.log_obj = log4m.getLogger([obj.outputDir '\log.txt']);
            obj.log_obj.setCommandWindowLevel(obj.command_window_level);
            obj.log_obj.setLogLevel(obj.log_level);
            
            obj.logMessage('Init', 3, ['Logger setup completed! Log level:' num2str(obj.log_level)]);
        end
        
        function TF = isArgPaired(obj, varargin)
            nArgs = length(varargin);
            if round(nArgs/2) ~= nArgs/2
                error('Arguments needs propertyName/propertyValue pairs!');
            else
                TF = true;
            end
        end
        
        function obj = setProperties(obj, options)
            optionNames = fieldnames(options);
            for idx = 1:length(optionNames)
                try
                    eval(['obj.' optionNames{idx} '= options.' optionNames{idx} ';']);
                catch
                    error('Failure at setting property:%s', optionNames{idx})
                end
            end
        end
        
        function logMessage(obj, tag, level, msg)
            LOG_LEVEL = {'ALL', 'TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'OFF'};
            try
                eval(['obj.log_obj.' char(lower(LOG_LEVEL(level+1))) '(''' tag ''', ''' msg ''');'])
            catch
                error('''%s'' cannot write (%s)log message: ''%s''.',tag, char(lower(LOG_LEVEL(level+1))), msg)
            end
        end
        
        function titleStr = getTitleComp(obj, EEGStruct, varargin)
            comp = varargin{:};
            if ischar(comp), comp = {comp}; end
            titleStr = [];
            for i = 1:length(comp)
                if strcmpi(comp{i}, 'subject._id')
                    comp{i} = 'subject.x0x5F_id';
                end
                try
                    eval(['field = EEGStruct.meta.' comp{i} ';']);
                catch
                    field = 'NULL';
                    %                     error('Reference to non-existent field ''%s''.', comp{i});
                end
                
                if i == 1
                    titleStr = field;
                else
                    titleStr = strcat(titleStr, '-', field);
                end
            end
        end
        
        function hDir = getHierarchyDir(obj, dataPath)
            if strcmpi(obj.outputOrg, 'flat')
                hDir = obj.outputDir;
                return;
            end
            [oDir, ~, ~] = fileparts(obj.outputDir);
            if strncmp(dataPath, oDir, length(oDir))
                rDir =  dataPath(length(oDir)+1:end);
            else
                rDir = dataPath;
            end
            hDir = fullfile(obj.outputDir,rDir);
            % remove colon case like C:\X\Y\D:\ -> C:\X\Y\D\
            hDir(find(ismember(hDir(3:end),':'))+2) = [];
        end
        
    end
end
