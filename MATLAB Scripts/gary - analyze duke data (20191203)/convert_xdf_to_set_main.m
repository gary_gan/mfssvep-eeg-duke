% filepath = 'E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\98. convertsample\DK0015-05_30_1949-mfssvep-left-1.xdf'
% 
% filepath='E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\98. convertsample\DK0015-05_30_1949-alpha.xdf'
% 
% filepath = 'E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\3. severe - handpick\DK0004\20181219\DK0004-05_20_1953-mfssvep-left-1.xdf';


FILEPATH = 'e:\_MFSSVEP DUKE\_category\3. Severe'

FILE_LIST = get_all_files(FILEPATH);

invalid_filepath = {};

for i =1:length(FILE_LIST)
    try
        filepath = FILE_LIST{i};
        [pathstr,filename,ext] = fileparts(filepath);   
        expression = '.*.xdf'; % only process file with mfssvep inside name

        if regexp(FILE_LIST{i},expression)    
            convert_xdf_to_set(filepath);   
        end
    catch
        disp(filepath)
        invalid_filepath{end+1} = filepath;
        disp(getReport(exception))
    end
    
end
