
color = ['r', 'g', 'b', 'm'];
% pattern_type_list = ['a', 'b'];
pattern_type_list = {'11', '12'};


% selected pattern type
% pattern_type = pattern_type_list(2)

new = struct;
new.p11 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p11.mat');
new.p12 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p12.mat');

% max_all = [];
% max_all{end+1} = max(cell2mat(struct2cell(new.p11.cca_rel_avg_struct.p11)))
% max_all{end+1} = max(cell2mat(struct2cell(new.p12.cca_rel_avg_struct.p12)))
% max = max(cell2mat(max_all))
% 
% min_all = [];
% min_all{end+1} = min(cell2mat(struct2cell(new.p11.cca_rel_avg_struct.p11)))
% min_all{end+1} = min(cell2mat(struct2cell(new.p12.cca_rel_avg_struct.p12)))
% min = min(cell2mat(min_all))

% convert struct to cell array
% cca_rel_avg_cell = struct2cell(cca_rel_avg_struct);
% evalute this in the if statement
% cca_rel_avg_cell = struct2cell(new.p12.cca_rel_avg_struct.p12);




for i=1:length(pattern_type_list)
    pattern_type = pattern_type_list{i};
    fprintf('Pattern %s\n', pattern_type);
    % pattern_type = pattern
    if (pattern_type == '11')
        disp('aaaa');
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 10];
        outermost_ring_freq = [0, 0, 0, 0, 9, 0, 0, 0];
        middle_ring_freq = [0, 0, 8, 0, 0, 0, 0, 0];
        inner_ring_freq = [11, 0, 0, 0];
        
        outer_outermost_ring_freq_struct_mapping = [3];
        outermost_ring_freq_struct_mapping = [2];
        middle_ring_freq_struct_mapping = [1];
        inner_ring_freq_struct_mapping = [4];   
        
        cca_rel_avg_cell = struct2cell(new.p11.cca_rel_avg_struct.p11);
    
    elseif (pattern_type == '12')
        disp('bbbb');
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 0];
        outermost_ring_freq = [0, 10, 0, 0, 0, 11, 0, 0];
        middle_ring_freq = [0, 0, 0, 9, 0, 0, 0, 0];
        inner_ring_freq = [0, 0, 0, 8];
        
        outer_outermost_ring_freq_struct_mapping = [];
        outermost_ring_freq_struct_mapping = [3, 4];
        middle_ring_freq_struct_mapping = [2];
        inner_ring_freq_struct_mapping = [1];     
        
        cca_rel_avg_cell = struct2cell(new.p12.cca_rel_avg_struct.p12);
    
    
    elseif (pattern_type == '13')
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 0];
        outermost_ring_freq = [0, 0, 11, 0, 0, 0, 0, 0];
        middle_ring_freq = [0, 9, 0, 0, 0, 0, 10, 0];
        inner_ring_freq = [0, 0, 8, 0];
        
        outer_outermost_ring_freq_struct_mapping = [];
        outermost_ring_freq_struct_mapping = [4];
        middle_ring_freq_struct_mapping = [2, 3];
        inner_ring_freq_struct_mapping = [1];     
        
        cca_rel_avg_cell = struct2cell(new.p13.cca_rel_avg_struct.p13);
    end
    
figure(i);



%% ORI
% t = linspace(90,360)/180*pi;
% x = [0 cos(t) 0];
% y = [0 sin(t) 0];
% figure
% hold on
% patch( x, y, 'r' )

%% Outer-Outermost circle
num_sector = 8
sector_start_angle = 0;
outer_outermost_ring_freq_struct_mapping_idx = 1;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (outer_outermost_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{outer_outermost_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        outer_outermost_ring_freq_struct_mapping_idx = outer_outermost_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x, y, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end

%% Outermost circle
num_sector = 8
sector_start_angle = 0;
outermost_ring_freq_struct_mapping_idx = outer_outermost_ring_freq_struct_mapping_idx;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (outermost_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{outermost_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        outermost_ring_freq_struct_mapping_idx = outermost_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/2, y/2, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end

%% Middle Circle
num_sector = 8
sector_start_angle = 0;
middle_ring_freq_struct_mapping_idx = outermost_ring_freq_struct_mapping_idx;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (middle_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{middle_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        middle_ring_freq_struct_mapping_idx = middle_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/4, y/4, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end


%% Inner Circle
num_sector = 4
sector_start_angle = 0;
inner_ring_freq_struct_mapping_idx = middle_ring_freq_struct_mapping_idx;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector);
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (inner_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{inner_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        inner_ring_freq_struct_mapping_idx = inner_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/8, y/8, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector);
end


end

% t = linspace(-pi,0.5*pi,128);
% x = [0 cos(t) 0];
% y = [0 sin(t) 0];
% patch ( x, y, 'r')





    

