% sync average cca signal from 2 sessions

TAG = '20191129 - normal_handpick_session_all_cca_preproc_masaki_noconcat_run2'
TAG = '20191130 - normal_handpick_session_all_cca_preproc_masaki_noconcat-nw1_5-4'
SAVE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output'

SAVE_PATH_ROOT = fullfile(SAVE_PATH, TAG)

PATTERN = {'11', '12', '13', '14', '15', '16'};


% FILE_LIST = get_all_files(SAVE_PATH_ROOT);
% 
% length(FILE_LIST)
% for i =1:length(FILE_LIST)
%     filepath = FILE_LIST{i};
%     [pathstr,filename,ext] = fileparts(filepath);   
%     
%     expression = '.*_cca_output.mat'; % only process file with mfssvep inside name
%     if regexp(filepath,expression) 
%     end
% end


s.srate = 500;
s.epoch_range = [0.2 5.2]
s.SAVE_PATH_ROOT = SAVE_PATH_ROOT;
ref_freq = 8;
nw = 2;
mean_avg_r = 0;
xlim_range = [5 15];
tag_ = 'tag';

% filepath = 'E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\20191129 - normal_handpick_session_all_cca_preproc_masaki_noconcat_run2\DK0006-10_12_1947-mfssvep-left-all';

filenames = { {'DK0006-10_12_1947-mfssvep-left-1', 'DK0006-10_12_1947-mfssvep-left-2'} }
%      {'DK0006-10_12_1947-mfssvep-left-1', 'DK0006-10_12_1947-mfssvep-left-2'} }

FREQUENCIES = [8:1:11];

%% f8
% output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];

N_SEGMENT = 3;
ONSET_LENGTH = 5*500;

%% for concatenated 
for file_idx=1:length(filenames)
    fileset = filenames{file_idx};
    filepath = fullfile(SAVE_PATH_ROOT, [ fileset{1} '-session=2' ]);
    for p_idx=1:length(PATTERN)
        s.pattern = PATTERN{p_idx};
        for freq_idx=1:length(FREQUENCIES)
            ref_freq = FREQUENCIES(freq_idx);
            cca_out_session_1_filename = [ fileset{1} '_p' s.pattern '_f' num2str(ref_freq) '_cca_output.mat' ];
            cca_out_session_1_filepath = fullfile(SAVE_PATH_ROOT, cca_out_session_1_filename);
            cca_out_session_1_struct = load(cca_out_session_1_filepath);

            cca_out_session_2_filename = [ fileset{2} '_p' s.pattern '_f' num2str(ref_freq) '_cca_output.mat' ];
            cca_out_session_2_filepath = fullfile(SAVE_PATH_ROOT, cca_out_session_2_filename);
            cca_out_session_2_struct = load(cca_out_session_2_filepath);  
            
            if size(cca_out_session_1_struct.cca_signals,3) == 1 % this is the concatenated signals
                % split the 15s for session 1 in concatenated output into 1 5s
                offset = 0;
                cca_out_session_1_segments = zeros(1, ONSET_LENGTH, N_SEGMENT); % channel x data x trial            
                for i=1:N_SEGMENT
                    start = offset+1;
                    e = start+ONSET_LENGTH-1;    
                    cca_out_session_1_segments(:,:,i) = cca_out_session_1_struct.cca_signals(start:e);  
                    offset = start + ONSET_LENGTH-1;
                end

                % split the 15s for session 2 in concatenated output into 1 5s
                offset = 0;
                cca_out_session_2_segments = zeros(1, ONSET_LENGTH, N_SEGMENT); % channel x data x trial            
                for i=1:N_SEGMENT
                    start = offset+1;
                    e = start+ONSET_LENGTH-1;    
                    cca_out_session_2_segments(:,:,i) = cca_out_session_2_struct.cca_signals(start:e);  
                    offset = start + ONSET_LENGTH-1;
                end  
                
            else
                cca_out_session_1_segments = cca_out_session_1_struct.cca_signals;
                cca_out_session_2_segments = cca_out_session_2_struct.cca_signals;
            end

            % the combined cca = session 1 + session 2
            data = cca_out_session_1_segments;
            combined_cca = zeros(size(data,1), size(data,2), 6); % ch x segmentlen x total trials

            for i=1:size(cca_out_session_1_segments,3)
                combined_cca(:,:,i) = cca_out_session_1_segments(:,:,i);
            end

            offset = 3;
            for i=1:size(cca_out_session_2_segments,3)
                offset = offset + 1;
                combined_cca(:,:,offset) = cca_out_session_2_segments(:,:,i);
            end

            xlim_range = [5 15];
            tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca-session=2' ];
            plot_psd (combined_cca, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);        
        end
    end
end

%% for not concatenated 
for file_idx=1:length(filenames)
    fileset = filenames{file_idx};
    for p_idx=1:length(PATTERN)
        s.pattern = PATTERN{p_idx};
        for freq_idx=1:length(FREQUENCIES)
            ref_freq = FREQUENCIES(freq_idx);
            cca_out_session_1_filename = [ fileset{1} '_p' s.pattern '_f' num2str(ref_freq) '_cca_output.mat' ];
            cca_out_session_1_filepath = fullfile(SAVE_PATH_ROOT, cca_out_session_1_filename);
            cca_out_session_1_struct = load(cca_out_session_1_filepath);

            cca_out_session_2_filename = [ fileset{2} '_p' s.pattern '_f' num2str(ref_freq) '_cca_output.mat' ];
            cca_out_session_2_filepath = fullfile(SAVE_PATH_ROOT, cca_out_session_2_filename);
            cca_out_session_2_struct = load(cca_out_session_2_filepath);  

            data = cca_out_session_1_struct.cca_signals;
            combined_cca = zeros(size(data,1), size(data,2), 6);

            for i=1:size(cca_out_session_1_struct.cca_signals,3)
                combined_cca(:,:,i) = cca_out_session_1_struct.cca_signals(:,:,i);
            end

            offset = 3;
            for i=1:size(cca_out_session_2_struct.cca_signals,3)
                offset = offset + 1;
                combined_cca(:,:,offset) = cca_out_session_2_struct.cca_signals(:,:,i);
            end

            xlim_range = [5 15];
            tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca-session=2' ];
            plot_psd (combined_cca, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);        
        end
    end
end

%% dd
% 
% for p_idx=1:length(PATTERN)
%     s.pattern = PATTERN{p_idx};
%     for freq_idx=1:length(FREQUENCIES)
%         ref_freq = FREQUENCIES(freq_idx);
%         cca_out_session_1_filename = [ 'DK0006-10_12_1947-mfssvep-left-1_p' s.pattern '_f' num2str(ref_freq) '_cca_output.mat' ];
%         cca_out_session_1_filepath = fullfile(SAVE_PATH_ROOT, cca_out_session_1_filename);
%         cca_out_session_1_struct = load(cca_out_session_1_filepath);
%         
%         cca_out_session_2_filename = [ 'DK0006-10_12_1947-mfssvep-left-2_p' s.pattern '_f' num2str(ref_freq) '_cca_output.mat' ];
%         cca_out_session_2_filepath = fullfile(SAVE_PATH_ROOT, cca_out_session_2_filename);
%         cca_out_session_2_struct = load(cca_out_session_2_filepath);  
%         
%         data = cca_out_session_1_struct.cca_signals;
%         combined_cca = zeros(size(data,1), size(data,2), 6);
% 
%         for i=1:size(cca_out_session_1_struct.cca_signals,3)
%             combined_cca(:,:,i) = cca_out_session_1_struct.cca_signals(:,:,i);
%         end
% 
%         offset = 3;
%         for i=1:size(cca_out_session_2_struct.cca_signals,3)
%             offset = offset + 1;
%             combined_cca(:,:,offset) = cca_out_session_2_struct.cca_signals(:,:,i);
%         end
% 
%         xlim_range = [5 15];
%         tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca-session=2' ];
%         plot_psd (combined_cca, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);        
%     end
% end
% 
% 
% session_1_p11_f8 = load('E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\20191129 - normal_handpick_session_all_cca_preproc_masaki_noconcat_run2\DK0006-10_12_1947-mfssvep-left-1_p11_f8_cca_output.mat');
% session_2_p11_f8 = load('E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\20191129 - normal_handpick_session_all_cca_preproc_masaki_noconcat_run2\DK0006-10_12_1947-mfssvep-left-2_p11_f8_cca_output.mat');
% 
% data = session_1_p11_f8.cca_signals;
% combined_cca = zeros(size(data,1), size(data,2), 6);
% 
% for i=1:size(session_1_p11_f8.cca_signals,3)
%     combined_cca(:,:,i) = session_1_p11_f8.cca_signals(:,:,i);
% end
% 
% offset = 3;
% for i=1:size(session_2_p11_f8.cca_signals,3)
%     offset = offset + 1;
%     combined_cca(:,:,offset) = session_2_p11_f8.cca_signals(:,:,i);
% end
% 
% xlim_range = [5 15];
% tag_ = [ 'xlim[' num2str(xlim_range(1)) '-' num2str(xlim_range(2)) ']-Avg Sync-aftercca-session=2' ];
% plot_psd (combined_cca, s, filepath, ref_freq, nw, mean_avg_r, xlim_range, tag_);
% 
% 
% session_1_p11_f8 = load('E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\20191129 - normal_handpick_session_all_cca_preproc_masaki_noconcat_run2\DK0006-10_12_1947-mfssvep-left-1_p11_f8_cca_output.mat');
% session_1_p11_f8 = load('E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\20191129 - normal_handpick_session_all_cca_preproc_masaki_noconcat_run2\DK0006-10_12_1947-mfssvep-left-1_p11_f8_cca_output.mat');