function [snr_area, snr_db] = plot_psd_with_baseline_per_epoch (data, data_baseline, s, filepath, ref_freq, nw, cca_cor, xlim_range, tag)

    snr = []; % return this to the caller

    [pathstr,name,ext] = fileparts(filepath);

    N_SEGMENT = size(data, 3);
    NW = nw;
    SRATE = s.srate;
    SEGLEN = (size(data,2)/SRATE);

    NW_baseline = 5;
    
    %% pmtm baseline
    N_DS_FACTOR = 3;
%     data_baseline_ = data_baseline(:,1:end-1);
        data_baseline_ = data_baseline(:,1:end);
    data_baseline_ds = zeros(size(data_baseline_,1), size(data_baseline_,2)/N_DS_FACTOR);
%     data_baseline_ds = zeros(size(data_baseline,1), 2500);
    
    for i=1:size(data_baseline,1)
        data_baseline_ds(i,:) = downsample(data_baseline_(i,:),N_DS_FACTOR);
    end
    t = squeeze(data_baseline_ds(1,:,:))
    [pxx_baseline, f_baseline] = pmtm(t,NW_baseline,length(t),SRATE);  
    pxx_baseline = pxx_baseline';    
    Q2_baseline = median(pxx_baseline,1);    

    %% iterate segment
    for i=1:N_SEGMENT
    %     temp = squeeze(data(ch,:,i));
        temp_ = squeeze(data(1,:,i));
        [pxx_(i,:),f] = pmtm(temp_,NW,length(temp_),SRATE); 
        
        % also do cca
            [A, B, r, U, V, awx, ~] = CCA_normal(data(:, :, i)', ref_freq);
%             betas = cat(1, betas, B(:, 1)');
%             rel = cat(1, rel, r(1));
%             kkk = get_snr(awx(:, 1), f, FS);
%             SNR_ = cat(1, SNR_, kkk);
%             new_result(:, j) = awx(:, 1);  
%                         
       cca_cor = r(1,1);
        
        
        pxx_zscore = zscore(pxx_(i,:));
        pxx_baseline_zscore = zscore(pxx_baseline);
        

        %% plot
        figure;set(gcf,'visible','off')
        % plot(f,Q2)
        marker_indices = 1:length(f);

        % plot the * if xlim range < 20
        if xlim_range < 20
        plot(f,pxx_zscore,'-*','MarkerIndices',marker_indices, ...
                  'MarkerFaceColor','blue', ...
                  'MarkerEdgeColor','blue',...
                  'MarkerSize',5);
        else
        plot(f,pxx_zscore,'-','MarkerIndices',marker_indices, ...
                  'MarkerFaceColor','blue', ...
                  'MarkerEdgeColor','blue',...
                  'MarkerSize',5);
        end
        hold on;
        marker_indices_target_freq = find(f==ref_freq);
        plot(f(marker_indices_target_freq),pxx_zscore(marker_indices_target_freq),...
                  '-*', ...
                  'MarkerFaceColor','red', ...
                  'MarkerEdgeColor','red',...
                  'MarkerSize',13);

        xlim( xlim_range );
        grid on;grid minor; set(gca,'fontsize',14)
    %     set(gca,'XMinorTick','on');
        xticks([1:1:100]);

    %     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
        if cca_cor > 0
            title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ', Trial=' num2str(i) ],'fontsize',12,'interpreter','none');
        else
            title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
        end

        xlabel('Frequency (Hz)','fontsize',14);
        ylabel('Power','fontsize',14);


        %% pmtm the baseline
        hold on;
    %     plot(f,Q2_baseline, 'r');
        plot(f,pxx_baseline_zscore,'-*','MarkerIndices',marker_indices, ...
                  'MarkerFaceColor','red', ...
                  'MarkerEdgeColor','red',...
                  'MarkerSize',5);

        legend('CCA output', 'Baseline (Eyes Open)');

    %     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
        SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (Z-score with Baseline) (Linear) (Per Epoch) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

        if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
        output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) '_t' num2str(i) ];
        % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

        disp(['Saving figure=' SAVE_PATH '\' output_filename]);
        print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
        saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
        close;       

        end
    
    
    return
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    %% Plot power (linear) (not db)
    Q2 = median(pxx_,1);

    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power','fontsize',14);
    
    
    %% pmtm the baseline
    hold on;
    % downsample
    N_DS_FACTOR = 3;
%     data_baseline_ = data_baseline(:,1:end-1);
        data_baseline_ = data_baseline(:,1:end);
    data_baseline_ds = zeros(size(data_baseline_,1), size(data_baseline_,2)/N_DS_FACTOR);
%     data_baseline_ds = zeros(size(data_baseline,1), 2500);
    
    for i=1:size(data_baseline,1)
        data_baseline_ds(i,:) = downsample(data_baseline_(i,:),N_DS_FACTOR);
    end
    t = squeeze(data_baseline_ds(1,:,:))
    [pxx_baseline, f_baseline] = pmtm(t,NW_baseline,length(t),SRATE);  
    pxx_baseline = pxx_baseline';    
    Q2_baseline = median(pxx_baseline,1);
    
%     plot(f,Q2_baseline, 'r');
    plot(f,Q2_baseline,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',5);
    
%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (with Baseline) (Linear) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;  
    
    
    
    %% ------------------------------------ Z-SCORE --------------------
    
    %% Plot power (linear) (not db)
    Q2 = median(pxx_,1);
    Q2_zscore = zscore(Q2);

    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2_zscore,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2_zscore,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_zscore(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power','fontsize',14);
    
    
    %% pmtm the baseline
    hold on;
       
    Q2_baseline = median(pxx_baseline,1);
    Q2_baseline_zscore = zscore(Q2_baseline);
    
%     plot(f,Q2_baseline, 'r');
    plot(f,Q2_baseline_zscore,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',5);
          
    legend('CCA output', 'Baseline (Eyes Open)');
    
%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (Z-score with Baseline) (Linear) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;       
    
    
    
    
    
    %% Plot in DB
       pxxdb = 10*log10(pxx_);
        Q2_db = median(pxxdb,1);

    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2_db,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2_db,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_db(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, Avg. CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);
    
    %% pmtm the baseline
    hold on;   
   pxx_baseline_db = 10*log10(pxx_baseline);
    Q2_baseline_db = median(pxx_baseline_db,1);
    
%     plot(f,Q2_baseline, 'r');    
    plot(f,Q2_baseline_db,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',5);    
    

%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (with Baseline) (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;
    
    
    %% ------------------------------------ Z-SCORE --------------------
    Q2_db_zscore = zscore(Q2_db);

    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2_db_zscore,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2_db_zscore,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_db_zscore(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);
    
    
    %% pmtm the baseline
    hold on;
    Q2_baseline_db_zscore = zscore(Q2_baseline_db);
    
%     plot(f,Q2_baseline, 'r');
    plot(f,Q2_baseline_db_zscore,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',5);
          
    legend('CCA output', 'Baseline (Eyes Open)');
    
%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (Z-score with Baseline) (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;        
    
       
    %% Calculate Area using linear scale
    Q2_zscore;
    Q2_baseline_zscore;
    
    N_POINTS = 5;
    power_cca_out = [];
    power_baseline = [];
    
    indices_target_freq = find(f==ref_freq);
    
%     for idx_pts=1:N_POINTS
    power_cca_out =  Q2_zscore(indices_target_freq-2)*0.2 + ...
                            Q2_zscore(indices_target_freq-1)*0.2 + ...
                            Q2_zscore(indices_target_freq)*0.2 + ...
                            Q2_zscore(indices_target_freq+1)*0.2 + ...
                            Q2_zscore(indices_target_freq+2)*0.2;
%     end
    
  
    power_baseline =  Q2_baseline_zscore(indices_target_freq-2)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq-1)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq+1)*0.2 + ...
                            Q2_baseline_zscore(indices_target_freq+2)*0.2;    
    
   
    snr_area = power_cca_out / power_baseline; 
    disp([ 'SNR (Z-Score) CCA Out / Baseline (Rest) Area = ' num2str(snr_area)]);    
    
    %% Plot psd dif (dB)
    Q2_diff = Q2_db-Q2_baseline_db;
    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2_diff,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2_diff,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, Avg. CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);    
    
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD Diff (with Baseline) (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;    
    
    %% Plot psd dif zscore (Linear)
    Q2_diff = Q2./Q2_baseline;
    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2_diff,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2_diff,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, Avg. CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);    
    
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD Diff (Z-Score with Baseline) (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;       
    

    %% Plot psd dif zscore (dB)
    Q2_diff = Q2_db_zscore-Q2_baseline_db_zscore;
    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2_diff,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2_diff,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, Avg. CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);    
    
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD Diff (Z-Score with Baseline) (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;   
    
    snr_db = Q2_diff(find(f==ref_freq)); % return the snr at target freq
    disp([ 'SNR (Z-Score dB) CCA Out / Baseline (Rest) = ' num2str(snr_db) 'dB']);
    
    
    %% Plot psd dif zscore (Linear)
    Q2_diff = Q2_zscore./Q2_baseline_zscore;
    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2_diff,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    else
    plot(f,Q2_diff,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',5);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2_diff(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, Avg. CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD Diff.(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power','fontsize',14);    
    
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD Diff (Z-Score with Baseline) (Linear) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;    
    
    
    

end
