color = ['r', 'g', 'b', 'm'];
pattern_type_list = ['a', 'b'];

% selected pattern type
pattern_type = pattern_type_list(2)



% convert struct to cell array
cca_rel_avg_cell = struct2cell(cca_rel_avg_struct);

%% Pattern A
if (pattern_type == 'a')
    outermost_ring_freq = [0, 10.4, 0, 11.6, 0, 11.2, 0, 10.8];
    middle_ring_freq = [9, 0, 10.2, 0, 9.8, 0, 9.4, 0];
    inner_ring_freq = [0, 8.6, 0, 8.2];    
    
    % mapping of struct to freq, e.g. outermost_ring_freq(10.4) map to struct
    % cell index 7
    outermost_ring_freq_struct_mapping = [7, 10, 9, 8];
    middle_ring_freq_struct_mapping = [3, 6, 5, 4];
    inner_ring_freq_struct_mapping = [2, 1];    

%% Pattern B
elseif (pattern_type == 'b')
    outermost_ring_freq = [10.6, 0, 11.8, 0, 11.4, 0, 11, 0];
    middle_ring_freq = [0, 8.8, 0, 10, 0, 9.6, 0, 9.2];
    inner_ring_freq = [8, 0, 8.4, 0];
    
    % mapping of struct to freq, e.g. outermost_ring_freq(10.4) map to struct
    % cell index 7
    outermost_ring_freq_struct_mapping = [7, 10, 9, 8];
    middle_ring_freq_struct_mapping = [3, 6, 5, 4];
    inner_ring_freq_struct_mapping = [1, 2];     
end;    


figure
%% ORI
% t = linspace(90,360)/180*pi;
% x = [0 cos(t) 0];
% y = [0 sin(t) 0];
% figure
% hold on
% patch( x, y, 'r' )

%% Outermost circle
num_sector = 8
sector_start_angle = 0;
outermost_ring_freq_struct_mapping_idx = 1;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (outermost_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{outermost_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        outermost_ring_freq_struct_mapping_idx = outermost_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x, y, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end

%% Middle Circle
num_sector = 8
sector_start_angle = 0;
middle_ring_freq_struct_mapping_idx = 1;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (middle_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{middle_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        middle_ring_freq_struct_mapping_idx = middle_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/2, y/2, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end


%% Inner Circle
num_sector = 4
sector_start_angle = 0;
inner_ring_freq_struct_mapping_idx = 1;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector);
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (inner_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{inner_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        inner_ring_freq_struct_mapping_idx = inner_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/4, y/4, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector);
end

% t = linspace(-pi,0.5*pi,128);
% x = [0 cos(t) 0];
% y = [0 sin(t) 0];
% patch ( x, y, 'r')


    

