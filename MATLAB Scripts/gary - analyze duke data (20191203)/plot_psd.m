function plot_psd (data, s, filepath, ref_freq, nw, cca_cor, xlim_range, tag)

[pathstr,name,ext] = fileparts(filepath);

N_SEGMENT = size(data, 3);
NW = nw;
SRATE = s.srate;
SEGLEN = (size(data,2)/SRATE);

    % iterate segment
    for i=1:N_SEGMENT
    %     temp = squeeze(data(ch,:,i));
        temp_ = squeeze(data(1,:,i));
        [pxx_(i,:),f] = pmtm(temp_,NW,length(temp_),SRATE);    
    end
    
    %% Plot power (not db)
        Q2 = median(pxx_,1);

    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',10);
    else
    plot(f,Q2,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',10);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ', Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power','fontsize',14);

%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (Linear) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;    
    
    
    %% Plot in DB
       pxxdb = 10*log10(pxx_);
        Q2 = median(pxxdb,1);

    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',10);
    else
    plot(f,Q2,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',10);
    end
    hold on;
    marker_indices_target_freq = find(f==ref_freq);
    plot(f(marker_indices_target_freq),Q2(marker_indices_target_freq),...
              '-*', ...
              'MarkerFaceColor','red', ...
              'MarkerEdgeColor','red',...
              'MarkerSize',13);

    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
%     title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) 'x 5s segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d')],'fontsize',12,'interpreter','none');
    if cca_cor > 0
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments, Avg. CCA Correlation: ' num2str(cca_cor, '%0.2f') char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    else
        title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', NW=' num2str(NW) ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);

%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;

end
