function eeg = bandpass(s,eeg)

% s = setting;
LOW_EDGE = s.BPLE;
HIGH_EDGE = s.BPHE;
SRATE = s.srate;
[CH,PNT] = size(eeg);

EEG = pop_importdata('dataformat','array','nbchan',CH,'data',eeg,'setname','temp','srate',SRATE,'pnts',0,'xmin',0);
EEG = eeg_checkset( EEG );
EEG = pop_eegfiltnew( EEG, LOW_EDGE, HIGH_EDGE);
eeg = EEG.data;