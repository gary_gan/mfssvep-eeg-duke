clear all; clc;
color = ['r', 'g', 'b', 'm'];
% pattern_type_list = ['a', 'b'];
pattern_type_list = {'11', '12', '13', '14', '15', '16'};
%pattern_type_list = { '11' };


% selected pattern type
% pattern_type = pattern_type_list(2)

new = struct;
new.p11 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p11.mat');
new.p12 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p12.mat');
new.p13 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p13.mat');
new.p14 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p14.mat');
new.p15 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p15.mat');
new.p16 = load('C:\Recordings\duke 2019\_sorted_by_subject_id\_filtered\DK0029\test\Figures\cca_avg_fstruct_pattern_p16.mat');    
        
        
% max_all = [];
% max_all{end+1} = max(cell2mat(struct2cell(new.p11.cca_rel_avg_struct.p11)))
% max_all{end+1} = max(cell2mat(struct2cell(new.p12.cca_rel_avg_struct.p12)))
% max = max(cell2mat(max_all))
% 
% min_all = [];
% min_all{end+1} = min(cell2mat(struct2cell(new.p11.cca_rel_avg_struct.p11)))
% min_all{end+1} = min(cell2mat(struct2cell(new.p12.cca_rel_avg_struct.p12)))
% min = min(cell2mat(min_all))

% convert struct to cell array
% cca_rel_avg_cell = struct2cell(cca_rel_avg_struct);
% evalute this in the if statement
% cca_rel_avg_cell = struct2cell(new.p12.cca_rel_avg_struct.p12);



%%
for i=1:length(pattern_type_list)
    pattern_type = pattern_type_list{i};
    fprintf('Pattern %s\n', pattern_type);
    % pattern_type = pattern
    if (pattern_type == '11')
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 10];
        outermost_ring_freq = [0, 0, 0, 0, 9, 0, 0, 0];
        middle_ring_freq = [0, 0, 8, 0, 0, 0, 0, 0];
        inner_ring_freq = [11, 0, 0, 0];
        
        outer_outermost_ring_freq_struct_mapping = [3];
        outermost_ring_freq_struct_mapping = [2];
        middle_ring_freq_struct_mapping = [1];
        inner_ring_freq_struct_mapping = [4];   
        
        cca_rel_avg_cell = struct2cell(new.p11.cca_rel_avg_struct.p11);
    
    elseif (pattern_type == '12')
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 0];
        outermost_ring_freq = [0, 10, 0, 0, 0, 11, 0, 0];
        middle_ring_freq = [0, 0, 0, 9, 0, 0, 0, 0];
        inner_ring_freq = [0, 0, 0, 8];
        
        outer_outermost_ring_freq_struct_mapping = [];
        outermost_ring_freq_struct_mapping = [3, 4];
        middle_ring_freq_struct_mapping = [2];
        inner_ring_freq_struct_mapping = [1];     
        
        cca_rel_avg_cell = struct2cell(new.p12.cca_rel_avg_struct.p12);
    
    
    elseif (pattern_type == '13')
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 0];
        outermost_ring_freq = [0, 0, 11, 0, 0, 0, 0, 0];
        middle_ring_freq = [0, 9, 0, 0, 0, 0, 10, 0];
        inner_ring_freq = [0, 0, 8, 0];
        
        outer_outermost_ring_freq_struct_mapping = [];
        outermost_ring_freq_struct_mapping = [4];
        middle_ring_freq_struct_mapping = [2, 3];
        inner_ring_freq_struct_mapping = [1];     
        
        cca_rel_avg_cell = struct2cell(new.p13.cca_rel_avg_struct.p13);
        
    elseif (pattern_type == '14')
        outer_outermost_ring_freq = [9, 0, 0, 0, 0, 0, 0, 0];
        outermost_ring_freq = [0, 0, 0, 10, 0, 0, 0, 11];
        middle_ring_freq = [0, 0, 0, 0, 8, 0, 0, 0];
        inner_ring_freq = [0, 0, 0, 0];
        
        outer_outermost_ring_freq_struct_mapping = [4];
        outermost_ring_freq_struct_mapping = [2, 3];
        middle_ring_freq_struct_mapping = [1];
        inner_ring_freq_struct_mapping = [];     
        
        cca_rel_avg_cell = struct2cell(new.p14.cca_rel_avg_struct.p14);    
        
    elseif (pattern_type == '15')
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 0];
        outermost_ring_freq = [0, 0, 0, 0, 11, 0, 10, 0];
        middle_ring_freq = [9, 0, 0, 0, 0, 0, 0, 0];
        inner_ring_freq = [0, 8, 0, 0];
        
        outer_outermost_ring_freq_struct_mapping = [];
        outermost_ring_freq_struct_mapping = [4, 3];
        middle_ring_freq_struct_mapping = [2];
        inner_ring_freq_struct_mapping = [1];     
        
        cca_rel_avg_cell = struct2cell(new.p15.cca_rel_avg_struct.p15); 
        
    elseif (pattern_type == '16')
        outer_outermost_ring_freq = [0, 0, 0, 0, 0, 0, 0, 0];
        outermost_ring_freq = [11, 0, 0, 0, 0, 0, 0, 0];
        middle_ring_freq = [0, 0, 0, 0, 0, 10, 0, 9];
        inner_ring_freq = [0, 8, 0, 0];
        
        outer_outermost_ring_freq_struct_mapping = [];
        outermost_ring_freq_struct_mapping = [4];
        middle_ring_freq_struct_mapping = [3, 2];
        inner_ring_freq_struct_mapping = [1];     
        
        cca_rel_avg_cell = struct2cell(new.p16.cca_rel_avg_struct.p16);         
    end     
    

%% Pattern A
% if (pattern_type == 'a')
%     outermost_ring_freq = [0, 10.4, 0, 11.6, 0, 11.2, 0, 10.8];
%     middle_ring_freq = [9, 0, 10.2, 0, 9.8, 0, 9.4, 0];
%     inner_ring_freq = [0, 8.6, 0, 8.2];    
%     
%     % mapping of struct to freq, e.g. outermost_ring_freq(10.4) map to struct
%     % cell index 7
%     outermost_ring_freq_struct_mapping = [7, 10, 9, 8];
%     middle_ring_freq_struct_mapping = [3, 6, 5, 4];
%     inner_ring_freq_struct_mapping = [2, 1];    
% 
% %% Pattern B
% elseif (pattern_type == 'b')
%     outermost_ring_freq = [10.6, 0, 11.8, 0, 11.4, 0, 11, 0];
%     middle_ring_freq = [0, 8.8, 0, 10, 0, 9.6, 0, 9.2];
%     inner_ring_freq = [8, 0, 8.4, 0];
%     
%     % mapping of struct to freq, e.g. outermost_ring_freq(10.4) map to struct
%     % cell index 7
%     outermost_ring_freq_struct_mapping = [7, 10, 9, 8];
%     middle_ring_freq_struct_mapping = [3, 6, 5, 4];
%     inner_ring_freq_struct_mapping = [1, 2];     
% end;    

figure(i);

%% ORI
% t = linspace(90,360)/180*pi;
% x = [0 cos(t) 0];
% y = [0 sin(t) 0];
% figure
% hold on
% patch( x, y, 'r' )

%% Outer-Outermost circle
num_sector = 8
sector_start_angle = 0;
outer_outermost_ring_freq_struct_mapping_idx = 1;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (outer_outermost_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{outer_outermost_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        outer_outermost_ring_freq_struct_mapping_idx = outer_outermost_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x, y, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end

%% Outermost circle
num_sector = 8
sector_start_angle = 0;
outermost_ring_freq_struct_mapping_idx = outer_outermost_ring_freq_struct_mapping_idx;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (outermost_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{outermost_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        outermost_ring_freq_struct_mapping_idx = outermost_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/2, y/2, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end

%% Middle Circle
num_sector = 8
sector_start_angle = 0;
middle_ring_freq_struct_mapping_idx = outermost_ring_freq_struct_mapping_idx;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector)
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (middle_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{middle_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        middle_ring_freq_struct_mapping_idx = middle_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/4, y/4, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector)
end


%% Inner Circle
num_sector = 4
sector_start_angle = 0;
inner_ring_freq_struct_mapping_idx = middle_ring_freq_struct_mapping_idx;
for i = 1:num_sector
    sector_end_angle = sector_start_angle + (360/num_sector);
    t = linspace(sector_start_angle,sector_end_angle)/180*pi;
    x = [0 cos(t) 0];
    y = [0 sin(t) 0];
    hold on
    if (inner_ring_freq(i) == 0)
        color = 'k'; % black color for 0f
    else
        color = get_sector_color(cca_rel_avg_cell{inner_ring_freq_struct_mapping_idx}); % access cell element using braces {}
        inner_ring_freq_struct_mapping_idx = inner_ring_freq_struct_mapping_idx+1;
        % color = 'w';
    end
    patch( x/8, y/8, color);
    
    sector_start_angle = sector_start_angle + (360/num_sector);
end

% t = linspace(-pi,0.5*pi,128);
% x = [0 cos(t) 0];
% y = [0 sin(t) 0];
% patch ( x, y, 'r')

end


% autoArrangeFigures(2,3,1)

% 
% c=hgload(figure(1));
% k=hgload(figure(2));
% 
% 
% % Prepare subplots
% figure
% h(1)=subplot(1,2,1);
% h(2)=subplot(1,2,2);
% % Paste figures on the subplots
% copyobj(allchild(get(c,'CurrentAxes')),h(1));
% copyobj(allchild(get(k,'CurrentAxes')),h(2));
% % Add legends
% l(1)=legend(h(1),'LegendForFirstFigure')
% l(2)=legend(h(2),'LegendForSecondFigure')




    

