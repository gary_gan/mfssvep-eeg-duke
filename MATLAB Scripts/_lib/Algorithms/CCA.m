%% project the input and template on another space that maximizies the correlation
%input: data (time*ch)
%frequency: data fundamental frequency (in vector)
%harmonics: number of harmonics
function [corrVector, U, V, A, B] = CCA(input, resolution, frequency, harmonics)
%column major
template = zeros(size(input,1),harmonics*2);
time = size(input,1);
T = 1/time * resolution;                     % Sample time
L = size(input,1);                     % Length of signal
t = (0:L-1)*T;                % Time vector
index=1;
corrVector = zeros(1,length(frequency));
for f = 1:length(frequency)
    for harm=1:(harmonics)
        template(:,index) = sin(2*harm*pi*frequency(f)*t);
        template(:,index+1) = cos(2*harm*pi*frequency(f)*t);
        index= index+2;
    end
    [A,B,r,U,V] = canoncorr(input,template);
    corrVector(f) = r(1);
    index=1;
    template=[];
end
end