% Simplified test script using Makoto's preprocessing then CCA.
clear all; clc;
% EEG_SET_FILEPATH = 'E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\98. convertsample\DK0015-05_30_1949-mfssvep-left-1.set';
EEG_SET_FILEPATH = 'DK0015-05_30_1949-mfssvep-left-1.set';

EEG = pop_loadset(EEG_SET_FILEPATH);

%% mfSSVEP Recording param
ref_freq = [8:11];
n_channels = 6; % ngoggle has 6 electrodes only

ONSET_LENGTH = 5*500; % 5 seconds, 500 sampling rate
N_TRIAL = 3; % number of epochs onset

%% Makoto preprocessing
asr_burst_criterion = 20;

EEG = clean_rawdata(EEG, -1, [1, 50], -1,-1, asr_burst_criterion, -1);  % FIR -> ASR
EEG = pop_runica(EEG, 'icatype', 'runica', 'extended',1); % offline ICA
EEG = pop_iclabel(EEG,'default'); % ICLabel

% find which IC has the highest value in 'Brain' label
[val, idx] = max(EEG.etc.ic_classification.ICLabel.classifications(:, 1));
ic_max_brain = EEG.icaact(idx,:);

% convert/project the IC into channel?
% ???

%% extract epoch 
event_id = '11';
epoch_range = [0.2 5.2]; % 5 seconds
fprintf('Extracting epoch with eventid=%s\n', event_id);
try

    epoch_all = pop_rmdat( EEG, {event_id}, epoch_range, 0);
    epoch_all = eeg_checkset (epoch_all);
    
catch exception
    disp(EEG_SET_FILEPATH)
    disp(getReport(exception))
    return;
end    

%% split extracted epoch into each epoch
offset = 0;
epoch = zeros(n_channels,ONSET_LENGTH,N_TRIAL); % channel x data x trial
for i = 1:N_TRIAL % select all epochs
    start = offset + 1;
    e = start+ONSET_LENGTH-1;
    test = epoch_all.data(:,start:e);
    epoch(:,:,i) = test;
    offset = start + ONSET_LENGTH-1;
end

[CH,PNT,TRIAL] = size(epoch);

for i=1:length(ref_freq) % run CCA for each freq
    for epoch_idx= 1:size(epoch,3)    
        [A, B, r, U, V, awx, ~] = CCA_normal(epoch(:, :, epoch_idx)', ref_freq(i));
        disp(r); % just show the correlation value
        
    end
end


disp('done!');