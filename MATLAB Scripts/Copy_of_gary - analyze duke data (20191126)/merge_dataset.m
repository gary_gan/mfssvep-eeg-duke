% merged session 1 and 2 together as -all.set

EEG_DATASET_ROOT = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal - handpick\'

FILE_LIST = get_all_files(EEG_DATASET_ROOT);
for i =1:length(FILE_LIST)
    filepath = FILE_LIST{i};
    disp(['Reading file: ' filepath]);
    
    expression = '.*mfssvep-.*[1].set'; % only process file with mfssvep inside name
    if regexp(filepath,expression)  
        [pathstr,name,ext] = fileparts(filepath);     
        
        % check if second file exist
        C = strsplit(name,'-');        
        set_2_name = [ C{1} '-' C{2} '-' C{3} '-' C{4} '-2.set' ];
        set_2_filepath = fullfile(pathstr, set_2_name);
        
        if exist(set_2_filepath)
            disp( ['Second session exist!: ', set_2_filepath ]);
            disp( 'Merging dataset 1 and 2' );
            
            EEG_1 = pop_loadset(filepath);
            EEG_2 = pop_loadset(set_2_filepath);
            EEG_merged = pop_mergeset( EEG_1, EEG_2);     
            
            set_merged_name = cell2mat([ C(1) '-' C(2) '-' C(3) '-' C(4) '-all.set']);
            pop_saveset( EEG_merged, ...
                        'filename', [ set_merged_name ], ...
                        'filepath', pathstr );                
            disp(['Merged into: ' set_merged_name]);            
        end
        
    end
end

disp('all done!');