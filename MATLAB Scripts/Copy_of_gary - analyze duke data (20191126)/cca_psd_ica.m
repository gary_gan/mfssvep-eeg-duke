function [avg_r] = cca_psd_ica(s,filepath,epoch,ref_freq,nw,ic_class_name,ic_class_prob,tag)

    SAVE_FIGURE = s.SAVE_FIGURE;

    % SAVE_PATH_ROOT = ['e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\Figures\' s.TAG];

    % s=setting;
    % [A,B,r,U,V] = canoncorr(X,Y)
    [pathstr,name,ext] = fileparts(filepath);
    meta_filename = name;
    C = strsplit(name,'-');
    % name = cell2mat([C(1) '-' C(4)]);
    name = cell2mat([C(1) '-' C(2) '-' C(3) '-' C(4) '-' C(5)]);
    name = strcat(name, ['-p' num2str(s.pattern,'%d') '-f' num2str(ref_freq,'%d')]);



    CH_LABEL = s.chlabel;
    SRATE = s.srate;
    SEG_LEN = s.seg_len;
    OVERLAP = s.overlap;
    NW = nw;

    data = epoch;

    color = {'b','r','r','g','k','m','c','c',[1 0.55 0]};
    figure;set(gcf,'visible','off')

    cca_signals = [];

    avg_r = [];

    %% Plot IC
    % for i = 1:size(data,1)
    for i = 1:size(data,3)

        [A, B, r, U, V, awx, ~] = CCA_normal(data', ref_freq);
        %             betas = cat(1, betas, B(:, 1)');
        %             rel = cat(1, rel, r(1));
        %             kkk = get_snr(awx(:, 1), f, FS);
        %             SNR_ = cat(1, SNR_, kkk);
        %             new_result(:, j) = awx(:, 1);  
        %                         
        avg_r = [avg_r r(1,1)];
        %             [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
        [pxx(i,:),f] = pmtm(U(:,1),NW,length(U(:,1)),SRATE);


        cca_signals = U(:,1);

        %% draw figure for CCA signal
        pxxdb = 10*log10(pxx);
        Q2 = median(pxxdb,1);
        figure;set(gcf,'visible','off')
        plot(f,Q2);
    %     marker_indices = 1:length(f);
    %     plot(f,Q2,'-*','MarkerIndices',marker_indices, ...
    %               'MarkerFaceColor','blue', ...
    %               'MarkerEdgeColor','blue',...
    %               'MarkerSize',10);
        hold on;
        marker_indices_target_freq = [];
        marker_indices_target_freq(end+1) = find(f==ref_freq);
        marker_indices_target_freq(end+1) = find(f==ref_freq*2);    
        marker_indices_target_freq(end+1) = find(f==ref_freq*3);        
        plot(f(marker_indices_target_freq),Q2(marker_indices_target_freq),...
                  '*', ...
                  'MarkerFaceColor','red', ...
                  'MarkerEdgeColor','red',...
                  'MarkerSize',13);

        xlim([ 5 35] );
        grid on;set(gca,'fontsize',14)
        title(['PSD(multitaper) / ' name char(10) 'Segment length:' num2str(size(U(:,1),1)/SRATE) 's, NW=' num2str(nw) char(10) 'CCA Ref. Freq:' num2str(ref_freq,'%d') ', Avg. CCA correlation:' num2str(avg_r,'%0.2f') ', IC:' char(ic_class_name) ',Prob=' num2str(ic_class_prob)],'fontsize',12,'interpreter','none');
        xlabel('Frequency (Hz)','fontsize',14);
        ylabel('Power (dB/Hz)','fontsize',14);

        % folder name
        SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-' num2str(size(U(:,1),1)/SRATE) 's segment-ic=' char(ic_class_name) '-' tag];

        if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
        output_filename = [ meta_filename '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
        % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

        disp(['Saving figure=' SAVE_PATH '\' output_filename]);
        print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
        saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
        close;   
    end

end

