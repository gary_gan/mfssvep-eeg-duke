%% example code for running simulated online REST
% buffer blocksize = srate/10 without overlapping
% restoredefaultpath
% RESTpath = '/home/yuan/Documents/Tool/REST-git/';
clear all; clc;
RESTpath = 'E:\_MFSSVEP DUKE\Matlab Libraries\REST-master';
current_path = pwd;
cd(RESTpath);

EEG_FILEPATH = 'E:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\0. normal\DK0006\20181205\DK0006-10_12_1947-mfssvep-left-1.xdf';

% pipeline
% FIR -> ASR -> ORICA -> eyeCatch (IC classification) -> Reproject non-eye
% ICs
pipeline_desc = { ...
'fir', {'fspec', [0.5000 1 50 55], 'stopripple', -40, ...
    'normalize_amplitude', 0}, ...
'repair_bursts', {'stddev_cutoff', 20, 'window_len', 0.5}, ...
'orica', {'onlineWhitening', {'blockSize', 16}, 'adaptiveFF', {'constant', 'tau_const', 3},... 
    'options', {'blockSize', 16}, 'evalConvergence', 0.01}, ...
'eyecatch', {'cutoff', 0.8} ...
'ica_reproject', {'ProjectionMatrix', '.icawinv', 'ComponentSubset', '.reject'}, ...
};
pipe = [{'FilterOrdering', strcat('flt_', pipeline_desc(1:2:end))}, pipeline_desc];
% REST
% eeg_path = 'eeg_data_path/';

%% load eeg data
eeg_path = pop_loadxdf(EEG_FILEPATH);

calib = 'calibration_data_path/'; % or you can select time range in eeg data for calibration
buffer = REST_offline(eeg_path, calib, pipe);

cd(current_path);
disp('Done')