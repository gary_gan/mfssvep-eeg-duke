clear all; clc;

TAG = 'normal_asr'
EYE_DIRECTION = 'left'


%%
TYPE = 'CCA MEAN OVERLAP';
FIG_ROOT_DIR = [ 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\' TAG ]
SAVE_PATH = ['e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\' TAG '\report\' TYPE ]

if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;

CCA_MEAN_OVERLAP_DIR_NAME = 'CCA MEAN OVERLAP';


% subject_id = 'DK0015';
pattern = { 'p11', 'p12', 'p13', 'p14', 'p15', 'p16' };

% left eye
GROUP_NORMAL = { 'DK0006', 'DK0015', 'DK0043', 'DK0046', 'DK0048' };
% GROUP_NORMAL = { 'DK0015' };
GROUP_MILD = { 'DK0011', 'DK0012', 'DK0021' };

GROUP_SEVERE = { 'DK0004', 'DK0014', 'DK0036', 'DK0039', 'DK0005' };

%% user set flag
if strcmp(TAG, 'normal_asr')
    group = GROUP_NORMAL;
elseif strcmp(TAG, 'severe')
    group = GROUP_SEVERE;
end

% select a list of subjects belongings
%  subjects = [group.left; group.right];
subjects = group;
% subplot(4,2,1)

% Plot format
% LEFT               
% CCA_MEAN_OVERLAP [2x3]
% p11 p12 p13
% p14 p15 p16

% PSD [6x4]
% p11
% f8 f9 f10 f11
% p12
% f8 f9 f10 f11
% ...
% p16
% f8 f9 f10 f11

% [8x4]

%% Plot CCA MEAN OVERLAP
% get list of files and filter
DIR = [ FIG_ROOT_DIR '\' CCA_MEAN_OVERLAP_DIR_NAME ];
files_ = dir(DIR);

record = [];


for subject_idx=1 : length(subjects)
    subject_id = subjects{subject_idx};

    % get list of filename that matches the subject id
    for f_idx=1 : length(files_)
        filename = files_(f_idx).name;

        expression = char(strcat(subject_id, '.*'));  % only process file with mfssvep inside name
        if regexp(filename, expression) % if the filename matches the subject      
            record{end+1} = filename;
        end
    end

    eye_direction = [ {'left_1'}, {'left_2'}, {'right_1'}, {'right_2'} ];

    for e_idx=1 : length(eye_direction)
        % create single figure with p11-p16
        for f_idx=1 : length(record)        
            for p_idx=1 : length(pattern)
                % expression = char(strcat(subject_id, '.*mfssvep.*left_1.*', pattern{p_idx}, '.*')); % only process file with mfssvep inside name
                expression = char(strcat(subject_id, '.*mfssvep.*', eye_direction{e_idx}, '.*', pattern{p_idx}, '.*')); % only process file with mfssvep inside name        
                if regexp(record{f_idx}, expression) 
                    filename = record{f_idx};  

                    % DK0004_05_20_1953_mfssvep_right_2_p15_f8-11_cca_mean_overlap.png
                    disp(filename);

                    filepath = [ DIR '\' filename ]

                    s = subplot(2,3,p_idx);

                    fig = image(imread(filepath));
                    set(gcf, 'visible', 'off');
                    set(gcf, 'Position', [0 0 2000 1000]);
                    
                    % set axes labels
                    set(gca,'xtick',[]) % hide all x labels
                    set(gca,'ytick',[]) % hide all x labels
%                     if (freq_idx == 1) 
%                         ylabel(pattern{p_idx});                            
%                     end                        
                    xlabel(pattern{p_idx});

                    % add title for first top left figure
                    if p_idx == 1 title([subject_id '-' TAG], 'Interpreter', 'none'); end                    
                    
                    
                    copyobj(fig,s);
                end
            end
        end

%         h = suptitle({ TAG, filename, 'CCA MEAN OVERLAP' });
%         h.Interpreter = 'none';

        output_filename = [ filename '_cca_mean_overlap_report.png' ];
        output_filepath = [ SAVE_PATH '\' output_filename ];
        print(gcf, '-dpng', '-r200', output_filepath)     
    end
    
end


disp('All done!');


%% Plot PSD