%% Plot mean and stdev in bar

clear all; clc;
STRUCT_DIR = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\_summary'


groups = { 'normal', 'severe' };
% groups = { 'normal' };

%% DO NOT MODIFY
r_all = [];

PATTERN_LIST = {'p11', 'p12', 'p13', 'p14', 'p15', 'p16'};
epoch = { 'e200' };
% columns index
f8 = [1:3];
f9 = [4:6];
f10 = [7:9];
f11 = [10:12];

freq = { 8, 9, 10, 11 };
freq_cols = { f8, f9, f10, f11 };

struct_all = {};

group_mean = [];
group_stddev = [];

for group_idx=1:length(groups)
    groups{group_idx}
    % load the struct
    struct_filepath = fullfile(STRUCT_DIR, [ 'cca_avg_fstruct_' groups{group_idx} '.mat'] )
    cca_rel_avg_struct = load(struct_filepath);
    struct_all{end+1} = cca_rel_avg_struct;
    
    cca_rel_avg_struct = cca_rel_avg_struct.cca_rel_avg_struct;
    records = fieldnames(cca_rel_avg_struct);

    for k=1:numel(records)
        row_idx = k;
        % get filename
        filename = records{k}

        % get subject name
        fields = strsplit(filename,'-');

        % get eye direction
        if (isfield(cca_rel_avg_struct.(records{k}), 'left'))
            eye_direction = 'left';
        else
            eye_direction = 'right';
        end

        % iterate each pattern

        for p_idx=1:length(PATTERN_LIST)
            for e_idx=1:length(epoch)

                for freq_idx=1:length(freq_cols)                         
                    r = cca_rel_avg_struct.(records{k}).(eye_direction).(PATTERN_LIST{p_idx}).(epoch{e_idx})(:, freq_cols{freq_idx});

                    arr = num2cell(r);
                    for i=1:length(arr)
                        r_all(end+1) = arr{i};                      
                    end               
                end
            end

        end
    end
    
    group_mean(end+1) = mean(r_all);
    group_stddev(end+1) = std(r_all);

    % next group ...

end

%% Plot bar
figure;
hold on;
x=[1:length(groups)];
y=group_mean;
bar(x,y)
ylim([0.1 0.25]);
errorbar(y,group_stddev)


