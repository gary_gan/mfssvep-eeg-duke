% Calculate sideband snr
function [snr_sideband] = cca_snr(s,filepath,epoch,ref_freq,sideband_points, nw)

    N_SIDEBAND_PTS = sideband_points; % how many points in f in left/right sideband,
                                      % with 500sps, f would be 0.2Hz apart,
                                      % setting this to 3 will take 

    % SAVE_PATH_ROOT = ['e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output\Figures\' s.TAG];

    % s=setting;
    % [A,B,r,U,V] = canoncorr(X,Y)
    [pathstr,name,ext] = fileparts(filepath);
    meta_filename = name;
    C = strsplit(name,'-');
    % name = cell2mat([C(1) '-' C(4)]);
    name = cell2mat([C(1) '-' C(2) '-' C(3) '-' C(4) '-' C(5)]);
    name = strcat(name, ['-f' num2str(ref_freq,'%d')]);



    CH_LABEL = s.chlabel;
    SRATE = s.srate;
    SEG_LEN = s.seg_len;
    OVERLAP = s.overlap;
    NW = s.NW;

    data = epoch;

    color = {'b','r','r','g','k','m','c','c',[1 0.55 0]};
    figure;set(gcf,'visible','off')

    clear pxx f
    snr_sideband = [];
    for i = 1:size(data,3) % for each epoch
        [A, B, r, U, V, awx, ~] = CCA_normal(data(:, :, i)', ref_freq);
        %             betas = cat(1, betas, B(:, 1)');
        %             rel = cat(1, rel, r(1));
        %             kkk = get_snr(awx(:, 1), f, FS);
        %             SNR_ = cat(1, SNR_, kkk);
        %             new_result(:, j) = awx(:, 1);  
        %                         
        [pxx(i,:),f] = pmtm(U(:,1), nw ,length(U(:,1)),SRATE);

        %% calculate SNR
        % ...    
        t_freq_idx = find(f==ref_freq); % target freq index
        Psig = pxx(1, t_freq_idx);

        Pside = [];
        offset = t_freq_idx-1;
        for i=1:N_SIDEBAND_PTS % scan left
            offset = offset - 1;
           Pside(end+1) = pxx(1, offset);
        end

        offset = t_freq_idx;
        for i=1:N_SIDEBAND_PTS % scan right
            offset = offset + 1;
           Pside(end+1) = pxx(1, offset);
        end         

        PsideAvg = mean(Pside); 

        SNR = 10*log10(Psig/PsideAvg); % SNR of PSig / Pnoise

        fprintf('SNR=%.4f\n', SNR);
        disp('snr');

        snr_sideband(end+1) = SNR; 


    end

end



      
   


