function eeg = preproc(s,EEG,filepath,preproc,concat,ica)
% function [EEG_before_ica, EEG_after_ica] = preproc(s,EEG,filepath,preproc,concat)

    [pathstr,name,ext] = fileparts(filepath);
    filename = name;
    C = strsplit(name,'-');



    %% use the buffer .mat form REST offline
    if strcmp(preproc, 'rest_offline')
        buffer_mat_filename = [ filename '.mat' ];
        buffer_mat_filepath = fullfile(pathstr, buffer_mat_filename)

        load(buffer_mat_filepath);

        % onset_new.data = cell2mat(buffer.data(3))
        EEG.data = cell2mat(buffer.data(3)); % the 3rd output
        
        %% extract epcoh
        event_id = s.pattern;        
        disp(['Extracting event id=' event_id ' for ' filename]);
         try

            % extract epoch, onset_id = 50, select just 5s
            % onset = pop_rmdat( eeg_, {'50'}, [0 5], 0);
            % onset = pop_rmdat( eeg_, {event_id}, [0 5], 0);
%             EEG = pop_rmdat( EEG, {event_id}, setting.epoch_range, 0);
%             EEG = eeg_checkset (EEG);         
            
            EEG = pop_epoch( EEG, {event_id}, [0.2 5.2], 'epochinfo', 'yes');
            EEG = eeg_checkset (EEG); 
            EEG = pop_rmbase( EEG, [],[]);  
            
%             pop_eegplot(EEG)

        catch exception
            disp(filepath)
            disp(getReport(exception))
            return;
         end         
         
        % concatenate trials   
        if concat
            EEG_before_concat = EEG;
            EEG = eeg_epoch2continuous(EEG);    
        end        

    %% Load dataset for Makoto preprocessing
    elseif strcmp(preproc, 'masaki')
        %% Masaki's preprocessing
        
        if s.RMBASE_flag 
            EEG = pop_rmbase( EEG,[], []);
        end
        % bandpass
        if s.BP_flag
            EEG = pop_eegfiltnew( EEG, s.BPLE, s.BPHE);   
        end

    %     ASR algorithm
        if s.ASR_flag EEG.data = asr_algo(s, EEG.data);end          
%                     EEG = pop_epoch( EEG, {'11', '12', '13', '14', '15', '16'}, [0.2 5.2], 'epochinfo', 'yes');

        
%         EEG = clean_rawdata(EEG, -1, [1, 50], -1,-1, 20, -1);  % FIR -> ASR
        
        %% extract epcoh
        event_id = s.pattern;        
        disp(['Extracting event id=' event_id ' for ' filename]);
         try

            % extract epoch, onset_id = 50, select just 5s
            % onset = pop_rmdat( eeg_, {'50'}, [0 5], 0);
            % onset = pop_rmdat( eeg_, {event_id}, [0 5], 0);
%             EEG = pop_rmdat( EEG, {event_id}, setting.epoch_range, 0);
%             EEG = eeg_checkset (EEG);         
            
            EEG = pop_epoch( EEG, {event_id}, [0.2 5.2], 'epochinfo', 'yes');
            EEG = eeg_checkset (EEG); 
            EEG = pop_rmbase( EEG, [],[]);  
            
%             pop_eegplot(EEG)

        catch exception
            disp(filepath)
            disp(getReport(exception))
            return;
         end         
         
        % concatenate trials   
        if concat
            EEG_before_concat = EEG;
            EEG = eeg_epoch2continuous(EEG);    
        end
        
    
        if ica
            EEG = pop_runica(EEG, 'icatype', 'runica', 'extended',1); % offline ICA
            EEG = pop_iclabel(EEG,'default'); % ICLabel
        end
        
    elseif strcmp(preproc, 'gary')
        % remove baseline
        if s.RMBASE_flag 
            EEG = pop_rmbase( EEG,[], []);
        end
        % bandpass
        if s.BP_flag
            EEG = pop_eegfiltnew( EEG, s.BPLE, s.BPHE);   
        end

    %     ASR algorithm
        if s.ASR_flag EEG.data = asr_algo(s, EEG.data);end    
    end

    eeg = EEG;
end

