function pxx_ = plot_psd_generic (data, s, filepath, nw, xlim_range, tag)

[pathstr,name,ext] = fileparts(filepath);

N_SEGMENT = size(data, 3);
NW = nw;
SRATE = s.srate;
SEGLEN = (size(data,2)/SRATE);

    % iterate segment
    for i=1:N_SEGMENT
    %     temp = squeeze(data(ch,:,i));
        temp_ = squeeze(data(1,:,i));
        [pxx_(i,:),f] = pmtm(temp_,NW,length(temp_),SRATE);    
    end
    
    % save the signals
    output_name = [s.SAVE_PATH_ROOT '\' name '_p' s.pattern '_beforecca.mat']
    save(output_name,'pxx_'); 
    
    
    %% Plot power (not db)
        Q2 = median(pxx_,1);

    figure;set(gcf,'visible','off')
    % plot(f,Q2)
    marker_indices = 1:length(f);
    
    % plot the * if xlim range < 20
    if xlim_range < 20
    plot(f,Q2,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',3);
    else
    plot(f,Q2,'-','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',10);
    end
  
    xlim( xlim_range );
    grid on;grid minor; set(gca,'fontsize',14)
%     set(gca,'XMinorTick','on');
    xticks([1:1:100]);
    
    if N_SEGMENT > 1 % not sync avg. if segment is 1
      title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ],'fontsize',12,'interpreter','none');
    else
      title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ],'fontsize',12,'interpreter','none');
    end
        
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power','fontsize',14);


%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (Linear) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']' ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close; 
    
    
    %% Plot dB
       pxxdb = 10*log10(pxx_);
        Q2 = median(pxxdb,1);

    figure;set(gcf,'visible','off')
    plot(f,Q2)
    marker_indices = 1:length(f);
    plot(f,Q2,'-*','MarkerIndices',marker_indices, ...
              'MarkerFaceColor','blue', ...
              'MarkerEdgeColor','blue',...
              'MarkerSize',3);
%     hold on;
%     marker_indices_target_freq = find(f==ref_freq);
%     plot(f(marker_indices_target_freq),Q2(marker_indices_target_freq),...
%               '-*', ...
%               'MarkerFaceColor','red', ...
%               'MarkerEdgeColor','red',...
%               'MarkerSize',13);

    xlim( xlim_range );
    grid on; grid minor; set(gca,'fontsize',14)
   xticks([1:1:100]);
 
    if N_SEGMENT > 1 % not sync avg. if segment is 1
      title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of ' num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ],'fontsize',12,'interpreter','none');
    else
      title(['PSD(multitaper) / ' name char(10) num2str(N_SEGMENT) ' x ' num2str(SEGLEN) 's segments' char(10) 'p' s.pattern ],'fontsize',12,'interpreter','none');
    end
    
    xlabel('Frequency (Hz)','fontsize',14);
    ylabel('Power (dB/Hz)','fontsize',14);

%     SAVE_PATH = [ s.SAVE_PATH_ROOT '\CCA+PSD ( NW=' num2str(NW) ')-Avg Sync (' num2str(N_SEGMENT) ' Segments)-' tag];
    SAVE_PATH = [ s.SAVE_PATH_ROOT '\PSD (dB) ( NW=' num2str(NW) ')-' num2str(N_SEGMENT) ' Segments ( ' num2str(SEGLEN) 's )-' tag];

    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    output_filename = [ name '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']' ];
    % print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])

    disp(['Saving figure=' SAVE_PATH '\' output_filename]);
    print('-dtiff','-r200',[SAVE_PATH '\' output_filename '.tiff'])
    saveas(gcf,[SAVE_PATH '\' output_filename '.fig'])
    close;

end
