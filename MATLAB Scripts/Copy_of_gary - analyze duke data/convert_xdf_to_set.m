function EEG = convert_xdf_to_set(filepath)

% EEG = pop_loadxdf('C:\Users\AE\Documents\Bitbucket\mfssvep-eeg-duke\dataset\mfssvep-v2\sample\DK0006-10_12_1947-alpha.xdf' , 'streamtype', 'EEG', 'exclude_markerstreams', {});
% [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off'); 
% EEG = eeg_checkset( EEG );
% EEG = pop_select( EEG, 'channel',{'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'});
% [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 3,'savenew','C:\\Users\\AE\\Documents\\Bitbucket\\mfssvep-eeg-duke\\dataset\\mfssvep-v2\\sample\\DK0006-10_12_1947-alpha-ch6.set','gui','off'); 



    try
        % read xdf file
        eeg_ = pop_loadxdf(filepath, 'streamtype', 'EEG', 'exclude_markerstreams', {});
%         [ALLEEG EEG_ CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off'); 
        
       
        
        eeg_ = eeg_checkset( eeg_ );
        
        % pop_eegplot(eeg, 1, 1, 1);
        % eeg = eeg.data;

        %% convert to set if .set not exist (REST offline need this)
        [pathstr,name,ext] = fileparts(filepath);
        filename = name;
        set_filepath = fullfile(pathstr, [ name '.set' ]);
        if ~isfile(set_filepath) == 1
            fprintf('Converting .xdf to .set and .fdt\n');
            
            eeg_ = pop_select( eeg_,'channel',{'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'});
%         [ALLEEG EEG_ CURRENTSET] = pop_newset(ALLEEG, EEG_, 3,'savenew',set_filepath,'gui','off');             
            
%% add channel location (my custom method). Replaced by eeglab tool instead
%         rest_path = 'c:\Users\AE\Documents\Bitbucket\mfssvep-eeg-duke\REST-master';
%         chanlocs_cog64 = load(fullfile(rest_path, 'data/chanlocs/Cognionics_64.mat'));
%         chanlocs_q30 = load(fullfile(rest_path, 'data/chanlocs/Quick30.mat'));        
% 
%         % cog64: PO1, PO2, Oz
%         % q30: O1, O2, Pz
%         
%        CHANNELS = { 'PO1', 'PO2', 'Oz', 'O1', 'O2', 'Pz'};
% %         CHANNELS = { 'PO1', 'PO2', 'Oz' };
%        src_ch_idx = 0;
%        dest_ch_idx = 0;
%        for ch_idx=1:length(CHANNELS)
%            
%            % source channel info
%            if ch_idx < 4
%                chanlocs = chanlocs_cog64.chanlocs;
%                
%            else
%             chanlocs = chanlocs_q30.chanlocs;
%                
%            end
%            
%            
%             for k=1:numel(chanlocs)
%                 ch_label = chanlocs(k).labels;
% 
%                 if strcmp(ch_label, CHANNELS{ch_idx})
%                    src_ch_idx = k;
%                 end
%             end
%             
%             % target channel info
%             for i=1:numel(eeg_.chanlocs)
%                 ch_label_2 = eeg_.chanlocs(i).labels;
%                 if strcmp(ch_label_2, CHANNELS{ch_idx})
%                     dest_ch_idx = i;
%                 end
%             end    
%             
%             disp([src_ch_idx dest_ch_idx]);
%             
%             eeg_.chanlocs(dest_ch_idx) = chanlocs(src_ch_idx);
%             eeg_.chanlocs(dest_ch_idx).urchan = ch_idx;
%             
%             src_ch_idx=0;
%             dest_ch_idx=0;
%        end

%% EEGLAB way of adding channel location
            eeglab_path = which('eeglab');
            [pathstr_eeglab,name,ext] = fileparts(eeglab_path); % just get the folder path            
            chan_loc_filepath = fullfile(pathstr_eeglab, 'plugins', 'dipfit', 'standard_BESA', 'standard-10-5-cap385.elp');
            eeg_ = pop_chanedit(eeg_, 'lookup', chan_loc_filepath);

            eeg_ = eeg_checkset(eeg_);
            pop_saveset( eeg_, ...
                        'filename', [ filename '.set' ], ...
                        'filepath', pathstr );
            disp(['Saved as: ' set_filepath]);
        else

        end

    catch exception
        disp(filepath)
        disp(getReport(exception))
        return;
    end
end