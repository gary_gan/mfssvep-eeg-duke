function cca_rel_avg = proc_vep_concat(s,filepath)

SAVE_PATH = 'e:\_MFSSVEP DUKE\_filtered_complete_mfssvep1and2_bygroup\output'

% s=setting;
% filepath=[FILE_PATH '\' char(FILE_LIST(i))];

SEGI = 2;% ignored segment in seconds
SEGD = 10;% data segement in seconds
SEGR = 2.5; % minimal rest segment in seconds: 2.5~3.5

[pathstr,name,ext] = fileparts(filepath);
filename = name;
C = strsplit(name,'-');
name = cell2mat([C(1) '-' C(4)]);

% extract fields from filename for cca correlation struct
meta_eye_direction = char(C(4));
meta_pattern = s.pattern;
meta_filename = filename;

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

%% My Own loading XDF
try
    % filepath='C:\Users\Gary\Desktop\_____________mfssvepduketest\DK0006-10_12_1947-mfssvep-left-1.xdf';
    % read xdf file
    eeg_ = pop_loadxdf(filepath);
    eeg_ = eeg_checkset(eeg_);
    eeg_ = pop_select( eeg_,'channel',{'PO1' 'O1' 'Pz' 'O2' 'Oz' 'PO2'});

    % pop_eegplot(eeg, 1, 1, 1);
    % eeg = eeg.data;

    eeg = eeg_.data;
catch exception
    disp(filepath)
    disp(getReport(exception))
    return;
end

%% convert to set if .set not exist (REST offline need this)
% [pathstr,name,ext] = fileparts(filepath);
% set_filepath = fullfile(pathstr, [ name '.set' ]);
% if ~isfile(set_filepath)
%     fprintf('Converting .xdf to .set and .fdt\n');
%     pop_saveset( eeg_, ...
%                 'filename', [ filename '.sett' ], ...
%                 'filepath', pathstr );
% else
%     
% end
% 
% eeg_ = pop_loadset( set_filepath );
% eeg = eeg_.data;

%% REST offline
% proc_rest_offline( set_filepath );

% eeg = data(2:9,:);
% remove baseline
% if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% bandpass
% if s.BP_flag eeg = bandpass(s,eeg);end
% ASR algorithm
% if s.ASR_flag eeg = asr_algo(s,eeg);end

%% use the buffer .mat form REST offline
buffer_mat_filename = [ filename '.mat' ];
buffer_mat_filepath = fullfile(pathstr, buffer_mat_filename)

load(buffer_mat_filepath);

% onset_new.data = cell2mat(buffer.data(3))
eeg_.data = cell2mat(buffer.data(3)); % the 3rd output

%% Makoto's preprocessing
eeg_ = clean_rawdata(eeg_, -1, [1, 50], -1,-1, 20, -1);  % FIR -> ASR
eeg_ = pop_runica(eeg_, 'icatype', 'runica', 'extended',1); % offline ICA
eeg_ = pop_iclabel(eeg_,'default'); % ICLabel

%% Extract epoch
event_id = s.pattern;
fprintf('Extracting epoch with eventid=%s\n', event_id);
%% pipeline replaced by REST offline
try
    % extract epoch, onset_id = 50, select just 5s
    % onset = pop_rmdat( eeg_, {'50'}, [0 5], 0);
    % onset = pop_rmdat( eeg_, {event_id}, [0 5], 0);
    onset = pop_rmdat( eeg_, {event_id}, s.epoch_range, 0);
    onset = eeg_checkset (onset);
    
%     % remove baseline
%     if s.RMBASE_flag 
%         onset = pop_rmbase( onset,[]);
%     end
%     % bandpass
%     if s.BP_flag
%         onset = pop_eegfiltnew( onset, s.BPLE, s.BPHE);   
%     end
%     
%     % ASR algorithm
%     if s.ASR_flag onset.data = asr_algo(s, onset.data);end
    
%     pop_eegplot(onset, 1, 1, 1);
catch exception
    disp(filepath)
    disp(getReport(exception))
    return;
end

% %% use the buffer .mat form REST offline
% buffer_mat_filename = [ filename '.mat' ];
% buffer_mat_filepath = fullfile(pathstr, buffer_mat_filename)
% 
% load(buffer_mat_filepath);
% 
% % onset_new.data = cell2mat(buffer.data(3))
% onset.data = cell2mat(buffer.data(3)); % the 3rd output


%% extract epochs
ONSET_LENGTH = 5*500; % 5 seconds, 500 sampling rate
% N_TRIAL = 15; % number of epochs onset
N_TRIAL = 3; % number of epochs onset
offset = 0;
epoch = zeros(6,ONSET_LENGTH,N_TRIAL); % channel x data x trial
for i = 1:N_TRIAL % select all epochs
    start = offset + 1;
    e = start+ONSET_LENGTH-1;
    test = onset.data(:,start:e);
    epoch(:,:,i) = test;
%     epoch(:,:,i) = onset.data(:,offset:500);
    offset = start + ONSET_LENGTH-1;
end

[CH,PNT,TRIAL] = size(epoch);

%% Concatenate epochs
epoch_concat = zeros(6, PNT*TRIAL, 1);

ONSET_LENGTH = 5*500;
offset = 0;
for i = 1:TRIAL
    start = offset+1;
    e = start + ONSET_LENGTH-1;
    epoch_concat(:, start:e, 1) = epoch(:, :, i);
    offset = start + ONSET_LENGTH-1;
end


%% CCA
%FREQUENCY_LIST = [8:1:11]; % ORI
%FREQUENCY_LIST = [8.2:0.4:10.2 10.4:0.4:11.6]; % pattern B
FREQUENCY_LIST = s.frequency_list; % get from setting instead depending on pattern

cca_rel_avg = [];
% cca_rel_avg_struct_ = struct;
% cca_rel_avg_struct.(strcat('p', s.pattern)) = struct;

% rename - to _ coz struct fieldname cannot have -
meta_filename = strrep(meta_filename, '-', '_');

% cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).cca_max(1) = 0.11
% cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).cca_max(2) = 0.12



for f = 1:length(FREQUENCY_LIST)
    fprintf('CCA, ref signal=%dHz\n', FREQUENCY_LIST(f));
    meta_frequency = FREQUENCY_LIST(f);

    % cca_rel_avg(end+1) = cca_psd_of_all_ch(s,filepath,epoch,FREQUENCY_LIST(f));
    cca_rel_avg = [cca_rel_avg cca_psd_of_all_ch(s,filepath,epoch_concat,FREQUENCY_LIST(f))];
    struct_field_name = strcat(filename, '-f', num2str(FREQUENCY_LIST(f)));
    struct_field_name = strrep(struct_field_name, '.', '_'); % struct field name cannot contain '.'
    struct_field_name = strrep(struct_field_name, '-', '_'); % struct field name cannot contain '-'
    % cca_rel_avg_struct.(s.pattern).(struct_field_name) = cca_rel_avg(end);
    % cca_rel_avg_struct.(strcat('p', s.pattern)).(struct_field_name) = cca_rel_avg(end);
    cca_rel_avg_struct.(meta_filename).(meta_eye_direction).(strcat('p', meta_pattern)).cca_max(f) = cca_rel_avg(end);
       
    % Plot CCA Waveform
%     cca_waveform(s,filepath,epoch_concat,FREQUENCY_LIST(f));
end

% output_name = 'C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_avg_f'
% save(output_name,'cca_rel_avg');

% [pathstr,name,ext] = fileparts(filepath);
% SAVE_PATH = [ s.SAVE_PATH_ROOT ];

% SAVE_PATH = [pathstr '\Figures'];
% if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;

% output_name = strcat('C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_avg_fstruct_pattern_', s.pattern);
% output_name = [SAVE_PATH '\cca_avg_fstruct_pattern_p' s.pattern];
% output_filename = [ meta_filename '_p' s.pattern '_e[' num2str(s.epoch_range(1)) '-' num2str(s.epoch_range(2)) ']_f' num2str(ref_freq) ];
% 
% output_name = [SAVE_PATH '\cca_avg_fstruct'];
% save(output_name,'cca_rel_avg_struct');


%% Plot multitaper
% NW = 4;
% SRATE = 500;
% % plot PSD
% for j = 1:8
%     pxx = [];f = [];
%     for k = 1:TRIAL
%         seg = epoch(j,:,k);
%         [pxx(end+1,:),f] = pmtm(seg,NW,length(seg),SRATE);
%     end
%     pxxdb = 10*log10(pxx);
%     Q1 = prctile(pxxdb,25);
%     Q2 = median(pxxdb,1);
%     Q3 = prctile(pxxdb,75);
%     
%     figure;hold on;set(gcf,'visible','off');
%     fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
%     plot(f,Q2,'k')
%     axis([0 50 -40 60]);grid on;
%     title([ name char(10) char(CH_LABEL(j)) ' \ ' num2str(SEG_LEN) 'sec segments \ Multitaper PSD'],'fontsize',12,'interpreter','none');
%     xlabel('Frequency (Hz)','fontsize',12);
%     ylabel('Power/Frequency (dB/Hz)','fontsize',12);
%     
%     SAVE_PATH = [pathstr '\Figures\p' num2str(s.pattern) '\PSD(multitaper)'];
%     if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
%     print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(j)) '.tiff'])
%     saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(j)) '.fig'])
%     close;
%     
% end



% 
% %% Ori
% data = csvread(filepath)';
% ps = data(12,:);
% % get onset index
% onset = [];
% i = 1;
% while( i < length(ps))
%     if ps(i) == 1 && i+7 <= length(ps) && ~any(~ps(i:i+7))
%         onset(end+1) = i;
%         i = i+(SEGI+SEGD+SEGR)*SRATE;
%     else
%         i = i+1;
%     end
% end
% 
% 
% eeg = data(2:9,:);
% % remove baseline
% if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% % bandpass
% if s.BP_flag eeg = bandpass(s,eeg);end
% % ASR algorithm
% if s.ASR_flag eeg = asr_algo(s,eeg);end
% 
% for i = 1:length(onset)
%     epoch(:,:,i) = eeg(:,onset(i)+SEGI*SRATE:onset(i)+(SEGI+SEGD)*SRATE-1);   
% end
% [CH,PNT,TRIAL] = size(epoch);
% % CCA
% cca_cor(s,filepath,epoch);
% cca_psd_of_all_ch(s,filepath,epoch);
% cca_waveform(s,filepath,epoch);
% 
% 
% % plot PSD
% for j = 1:CH
%     pxx = [];f = [];
%     for k = 1:TRIAL
%         seg = epoch(j,:,k);
%         [pxx(end+1,:),f] = pmtm(seg,NW,length(seg),SRATE);
%     end
%     pxxdb = 10*log10(pxx);
%     Q1 = prctile(pxxdb,25);
%     Q2 = median(pxxdb,1);
%     Q3 = prctile(pxxdb,75);
%     
%     figure;hold on;set(gcf,'visible','off');
%     fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
%     plot(f,Q2,'k')
%     axis([0 50 -40 60]);grid on;
%     title([ name char(10) char(CH_LABEL(j)) ' \ ' num2str(SEG_LEN) 'sec segments \ Multitaper PSD'],'fontsize',12,'interpreter','none');
%     xlabel('Frequency (Hz)','fontsize',12);
%     ylabel('Power/Frequency (dB/Hz)','fontsize',12);
%     
%     SAVE_PATH = [pathstr '\Figures\PSD(multitaper, ' num2str(s.seg_len) 's seg)\' name];
%     if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
%     print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(j)) '.tiff'])
%     saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(j)) '.fig'])
%     close;
%     
% end