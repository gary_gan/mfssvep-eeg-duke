function eeg = asr_algo(s,eeg)

% s = setting;
SRATE = s.srate;
[CH,PNT] = size(eeg);

EEG = pop_importdata('dataformat','array','nbchan',CH,'data',eeg,'setname','temp','srate',SRATE,'pnts',0,'xmin',0);

%% Christian's Preprocessing
% Arguments
channel_crit=0.45;
burst_crit=3;
window_crit=0.1;
highpass=[0.5 1];
channel_crit_excluded=0.1;
channel_crit_maxbad_time=0.5;
burst_crit_refmaxbadchns=0.075;
burst_crit_reftolerances=[-5 3];
window_crit_tolerances=[];
flatline_crit=5;
%% remove flat-line channels
% EEG = clean_flatlines(EEG,flatline_crit);
%% high-pass filter the data
EEG = clean_drifts(EEG,highpass);
%% remove hopeless channels
% EEG = clean_channels(EEG,channel_crit,channel_crit_excluded,[],channel_crit_maxbad_time);
%% repair bursts (It seems like that this function needs data exceeds 10s, or it cannot proceed)
% if length(EEG.data) >= 15*EEG.srate % tried 10s, but must exceeds 15s
    EEG = repair_bursts(EEG,burst_crit,[],[],[],burst_crit_refmaxbadchns,burst_crit_reftolerances,[]);
% end
%% remove hopeless time windows
%   EEG = clean_windows(EEG,window_crit,window_crit_tolerances);
eeg = EEG.data;