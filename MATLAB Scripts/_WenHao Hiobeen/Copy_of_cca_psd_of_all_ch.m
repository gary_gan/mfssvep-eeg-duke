function cca_psd_of_all_ch(s,filepath,epoch)
% s=setting;
% [A,B,r,U,V] = canoncorr(X,Y)
[pathstr,name,ext] = fileparts(filepath);
C = strsplit(name,'-');
name = cell2mat([C(1) '-' C(4)]);


CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

data = epoch;

expression = '\d+Hz';
[startIndex,endIndex] = regexp(filepath,expression) ;
freq=str2num(filepath(startIndex:endIndex-2));

t = (1:length(data))/SRATE;
ref1 = sin(2*pi*freq*t);
ref2 = cos(2*pi*freq*t);
REF = [ref1; ref2];
color = {'b','','r','g','k','m','c','',[1 0.55 0]};
figure;set(gcf,'visible','off')
for ch = [1 3:7 9]
    clear pxx f
    if ch == 9
        avg_r = [];
        for i = 1:size(data,3)
            temp = squeeze(data([1 3:7],:,i));
            clear A B r U V
            [A,B,r,U,V] = canoncorr(temp',REF');
            avg_r = [avg_r r(1,1)];
            [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
        end
        avg_r = mean(avg_r);
    else
        for i = 1:size(data,3)
            temp = squeeze(data(ch,:,i));
            [pxx(i,:),f] = pmtm(temp,4,length(temp),SRATE);
        end
    end
    
    pxxdb = 10*log10(pxx);
    Q2 = median(pxxdb,1);
    
    hold on;
    plot(f,Q2,'color',color{ch})
end
axis([5 50 -30 20]);
grid on;set(gca,'fontsize',14)
title(['PSD(multitaper) / ' name char(10) 'Sync. Avg. of 10 epochs, Avg. CCA correlation:' num2str(avg_r,'%0.2f')],'fontsize',12,'interpreter','none');
xlabel('Frequency (Hz)','fontsize',14);
ylabel('Power (dB/Hz)','fontsize',14);

LG=legend(CH_LABEL([1 3:7]),'CCA result');
set(LG,'location','northeastoutside')

SAVE_PATH = [pathstr '\Figures\CCA+PSD all7ch.( ' num2str(s.seg_len) 's seg)\'];
if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])
saveas(gcf,[SAVE_PATH '\' name '.fig'])
close;

