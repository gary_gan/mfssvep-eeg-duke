function cca_waveform(s,filepath,epoch);
% s=setting;
% [A,B,r,U,V] = canoncorr(X,Y)
[pathstr,name,ext] = fileparts(filepath);
C = strsplit(name,'-');
name = cell2mat([C(1) '-' C(4)]);

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

data = epoch;

expression = '\d+Hz';
[startIndex,endIndex] = regexp(filepath,expression) ;
freq=str2num(filepath(startIndex:endIndex-2));

t = (1:length(data))/SRATE;
ref1 = sin(2*pi*freq*t);
ref2 = cos(2*pi*freq*t);
REF = [ref1; ref2];
color = {'b','','r','g','k','m','c','',[1 0.55 0]};
clear pxx f
corvalue = [];
for i = 1:size(data,3)
    temp = squeeze(data([1 3:7],:,i));
    clear A B r U V
    [A,B,r,U,V] = canoncorr(temp',REF');
    corvalue = [corvalue r(1,1)];
    [pxx(i,:),f] = pmtm(U(:,1),4,length(U(:,1)),SRATE);
    allU(i,:) = U(:,1);
end

pxxdb = 10*log10(pxx);
max_idx = find(corvalue == max(corvalue));
min_idx = find(corvalue == min(corvalue));

SEGLEN = 0.5;
maxcor_epoch = reshape(allU(max_idx,:),SEGLEN*SRATE,[]);
maxcor_seg = mean(maxcor_epoch,2);

mincor_epoch = reshape(allU(min_idx,:),SEGLEN*SRATE,[]);
mincor_seg = mean(mincor_epoch,2);
figure;set(gcf,'visible','off')
plot(maxcor_seg,'b')
hold on
plot(mincor_seg,'r')

grid on;set(gca,'xticklabel',get(gca,'xtick')/SRATE,'fontsize',14)
title(['Waveform of CCA result / ' name char(10) 'Sync. Avg. of ' num2str(SEG_LEN/SEGLEN) ' segments in each epoch'],'fontsize',12,'interpreter','none');
xlabel('Time (sec)','fontsize',14);
ylabel('Amplitude (\muV)','fontsize',14);
LG=legend(['Max Corr.(' num2str(max(corvalue),'%0.2f') ') epoch'],['Min Corr.(' num2str(min(corvalue),'%0.2f') ') epoch']);
ylim([-1.5 1.5])

SAVE_PATH = [pathstr '\Figures\Waveform of CCA( ' num2str(SEGLEN) 's seg)\'];
if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
print('-dtiff','-r200',[SAVE_PATH '\' name '.tiff'])
saveas(gcf,[SAVE_PATH '\' name '.fig'])
close;
