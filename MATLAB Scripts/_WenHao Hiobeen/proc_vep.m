function proc_vep(s,filepath)
% s=setting;
% filepath=[FILE_PATH '\' char(FILE_LIST(i))];

SEGI = 2;% ignored segment in seconds
SEGD = 10;% data segement in seconds
SEGR = 2.5; % minimal rest segment in seconds: 2.5~3.5

[pathstr,name,ext] = fileparts(filepath);
C = strsplit(name,'-');
name = cell2mat([C(1) '-' C(4)]);

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

%% My Own loading XDF
try
    % filepath='C:\Users\Gary\Desktop\_____________mfssvepduketest\DK0006-10_12_1947-mfssvep-left-1.xdf';
    % read xdf file
    eeg_ = pop_loadxdf(filepath);
    eeg_ = eeg_checkset(eeg_);

    % pop_eegplot(eeg, 1, 1, 1);
    % eeg = eeg.data;

    eeg = eeg_.data;
catch exception
    disp(filepath)
    disp(getReport(exception))
    return;
end

% eeg = data(2:9,:);
% remove baseline
if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% bandpass
if s.BP_flag eeg = bandpass(s,eeg);end
% ASR algorithm
% if s.ASR_flag eeg = asr_algo(s,eeg);end

%% Extract epoch
try
    % extract epoch, onset_id = 50, select just 5s
    onset = pop_rmdat( eeg_, {'50'}, [0 5], 0);
    onset = eeg_checkset (onset);
    
    % remove baseline
    if s.RMBASE_flag 
        onset = pop_rmbase( onset,[]);
    end
    % bandpass
    if s.BP_flag
        onset = pop_eegfiltnew( onset, s.BPLE, s.BPHE);   
    end
    
    % pop_eegplot(epoch_onset, 1, 1, 1);
catch exception
    disp(filepath)
    disp(getReport(exception))
    return;
end

ONSET_LENGTH = 5*500; % 5 seconds, 500 sampling rate
N_TRIAL = 15; % number of epochs onset
offset = 0;
epoch = zeros(16,ONSET_LENGTH,N_TRIAL); % channel x data x trial
for i = 1:N_TRIAL % select all epochs
    start = offset + 1;
    e = start+ONSET_LENGTH-1;
    test = onset.data(:,start:e);
    epoch(:,:,i) = test;
%     epoch(:,:,i) = onset.data(:,offset:500);
    offset = start + ONSET_LENGTH-1;
end

[CH,PNT,TRIAL] = size(epoch);

%% CCA
%FREQUENCY_LIST = [8:1:11]; % ORI
%FREQUENCY_LIST = [8.2:0.4:10.2 10.4:0.4:11.6]; % pattern B
FREQUENCY_LIST = s.frequency_list; % get from setting instead depending on pattern
cca_rel_avg = [];
cca_rel_avg_struct = struct;
for f = 1:length(FREQUENCY_LIST)
    fprintf('CCA, ref signal=%dHz\n', FREQUENCY_LIST(f));
    cca_rel_avg(end+1) = cca_psd_of_all_ch(s,filepath,epoch,FREQUENCY_LIST(f));
    
    struct_field_name = strcat('f', num2str(FREQUENCY_LIST(f)));
    struct_field_name = strrep(struct_field_name, '.', '_'); % struct field name cannot contain '.'
    cca_rel_avg_struct.(struct_field_name) = cca_rel_avg(end);
    cca_waveform(s,filepath,epoch,FREQUENCY_LIST(f));
end

% output_name = 'C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_avg_f'
% save(output_name,'cca_rel_avg');

[pathstr,name,ext] = fileparts(filepath);
SAVE_PATH = [pathstr '\Figures'];
if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;

% output_name = strcat('C:\Users\Gary\Desktop\_____________mfssvepduketest\test single\Figures\cca_avg_fstruct_pattern_', s.pattern);
output_name = [SAVE_PATH '\cca_avg_fstruct_pattern_' s.pattern];
save(output_name,'cca_rel_avg_struct');


%%
NW = 4;
SRATE = 500;
% plot PSD
for j = 1:8
    pxx = [];f = [];
    for k = 1:TRIAL
        seg = epoch(j,:,k);
        [pxx(end+1,:),f] = pmtm(seg,NW,length(seg),SRATE);
    end
    pxxdb = 10*log10(pxx);
    Q1 = prctile(pxxdb,25);
    Q2 = median(pxxdb,1);
    Q3 = prctile(pxxdb,75);
    
    figure;hold on;set(gcf,'visible','off');
    fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
    plot(f,Q2,'k')
    axis([0 50 -40 60]);grid on;
    title([ name char(10) char(CH_LABEL(j)) ' \ ' num2str(SEG_LEN) 'sec segments \ Multitaper PSD'],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',12);
    ylabel('Power/Frequency (dB/Hz)','fontsize',12);
    
    SAVE_PATH = [pathstr '\Figures\PSD(multitaper)'];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(j)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(j)) '.fig'])
    close;
    
end



% 
% %% Ori
% data = csvread(filepath)';
% ps = data(12,:);
% % get onset index
% onset = [];
% i = 1;
% while( i < length(ps))
%     if ps(i) == 1 && i+7 <= length(ps) && ~any(~ps(i:i+7))
%         onset(end+1) = i;
%         i = i+(SEGI+SEGD+SEGR)*SRATE;
%     else
%         i = i+1;
%     end
% end
% 
% 
% eeg = data(2:9,:);
% % remove baseline
% if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% % bandpass
% if s.BP_flag eeg = bandpass(s,eeg);end
% % ASR algorithm
% if s.ASR_flag eeg = asr_algo(s,eeg);end
% 
% for i = 1:length(onset)
%     epoch(:,:,i) = eeg(:,onset(i)+SEGI*SRATE:onset(i)+(SEGI+SEGD)*SRATE-1);   
% end
% [CH,PNT,TRIAL] = size(epoch);
% % CCA
% cca_cor(s,filepath,epoch);
% cca_psd_of_all_ch(s,filepath,epoch);
% cca_waveform(s,filepath,epoch);
% 
% 
% % plot PSD
% for j = 1:CH
%     pxx = [];f = [];
%     for k = 1:TRIAL
%         seg = epoch(j,:,k);
%         [pxx(end+1,:),f] = pmtm(seg,NW,length(seg),SRATE);
%     end
%     pxxdb = 10*log10(pxx);
%     Q1 = prctile(pxxdb,25);
%     Q2 = median(pxxdb,1);
%     Q3 = prctile(pxxdb,75);
%     
%     figure;hold on;set(gcf,'visible','off');
%     fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
%     plot(f,Q2,'k')
%     axis([0 50 -40 60]);grid on;
%     title([ name char(10) char(CH_LABEL(j)) ' \ ' num2str(SEG_LEN) 'sec segments \ Multitaper PSD'],'fontsize',12,'interpreter','none');
%     xlabel('Frequency (Hz)','fontsize',12);
%     ylabel('Power/Frequency (dB/Hz)','fontsize',12);
%     
%     SAVE_PATH = [pathstr '\Figures\PSD(multitaper, ' num2str(s.seg_len) 's seg)\' name];
%     if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
%     print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(j)) '.tiff'])
%     saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(j)) '.fig'])
%     close;
%     
% end