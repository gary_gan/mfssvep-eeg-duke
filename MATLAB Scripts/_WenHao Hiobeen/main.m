clear all;close all;clc;
% cd('E:\_CerebraTek\_ngoggle kist\_Script')
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(process)\';
FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\test single\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch(Healthy)\'
% FILE_PATH = 'c:\Users\Gary\Desktop\_____________mfssvepduketest\batch (with problem)\'

pattern_list = ['a', 'b'];
pattern = pattern_list(2);
setting.pattern = pattern;

setting.seg_len = 10;
setting.overlap = 0;
setting.srate = 500;
setting.NW = 4;
setting.RMBASE_flag = true;   % apply remove baseline filter or not
setting.BP_flag = true;      % apply bandpass filter or not
setting.ASR_flag = true;     % apply ASR algorithm or not
setting.BPLE = 1;           % bandpass low edge
setting.BPHE = 50;          % bandpass high edge
% setting.chlabel = {'EOG1', 'O1', 'P5', 'POz', 'P6', 'CPz', 'EOG2', 'O2'};
% Gear VR Set
% setting.chlabel = {'O1', 'EOG-L', 'POz', 'PO5', 'CPz', 'PO6', 'O2', 'EOG-R'};

% duke eye center OSVR set
setting.chlabel = {'CH1', 'CH2', 'PO1', 'O1', 'Pz', 'O2', 'Oz', 'PO2'};

% FILE_LIST = dir([FILE_PATH '\*.csv']);
% FILE_LIST = {FILE_LIST.name};

% load XDF
FILE_LIST = dir([FILE_PATH '\*.xdf']);
FILE_LIST = {FILE_LIST.name};

% pattern freuqnecy list
if (pattern == 'a')
    FREQUENCY_LIST = [8.2:0.4:10.2 10.4:0.4:11.6]; % pattern A
elseif (pattern == 'b')
    FREQUENCY_LIST = [8:0.4:10 10.6:0.4:11.8]; % pattern B
end    

setting.frequency_list = FREQUENCY_LIST;

for i =1:length(FILE_LIST)
    expression = '.*mfssvep.*'; % only process file with mfssvep inside name
    if regexp(char(FILE_LIST(i)),expression)    
        fprintf(['Analyzing file=' char(FILE_LIST(i)) '\n']);
        proc_vep(setting,[FILE_PATH '\' char(FILE_LIST(i))]);%vep10s
    end
end







% for i =1:length(FILE_LIST)
%     if regexp(char(FILE_LIST(i)),'frameonset')
%         continue
%     end
%     if regexp(char(FILE_LIST(i)),'Alpha')
%         proc_alpha(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     end
%     
%     expression = '.*VEP.*';
%     if regexp(char(FILE_LIST(i)),expression)
%         proc_vep(setting,[FILE_PATH '\' char(FILE_LIST(i))]);%vep10s
%         setting.NW = 3;
%         proc_vep2s(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%         
%     elseif regexp(char(FILE_LIST(i)),'.*Supervised.*')
%         proc_caseSUP(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     else
%         proc_other(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
%     end
% end
set(gcf,'visible','on');close;

