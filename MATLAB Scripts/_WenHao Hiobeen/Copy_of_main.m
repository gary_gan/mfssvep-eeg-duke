clear all;close all;clc;
cd('c:\Users\Gary\Desktop\___20181130\_Script')
FILE_PATH = 'c:\Users\Gary\Desktop\___20181130\Hiobeen-subset\';

setting.seg_len = 10;
setting.overlap = 0;
setting.srate = 500;
setting.NW = 4;
setting.RMBASE_flag = true;   % apply remove baseline filter or not
setting.BP_flag = true;      % apply bandpass filter or not
setting.ASR_flag = true;     % apply ASR algorithm or not
setting.BPLE = 1;           % bandpass low edge
setting.BPHE = 50;          % bandpass high edge
% setting.chlabel = {'EOG1', 'O1', 'P5', 'POz', 'P6', 'CPz', 'EOG2', 'O2'};
setting.chlabel = {'O1', 'EOG-L', 'POz', 'PO5', 'CPz', 'PO6', 'O2', 'EOG-R'};
FILE_LIST = dir([FILE_PATH '\*.csv']);
FILE_LIST = {FILE_LIST.name};

for i =1:length(FILE_LIST)
    if regexp(char(FILE_LIST(i)),'frameonset')
        continue
    end
    if regexp(char(FILE_LIST(i)),'Alpha')
        proc_alpha(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
    end
    
    expression = '.*VEP.*';
    if regexp(char(FILE_LIST(i)),expression)
        proc_vep(setting,[FILE_PATH '\' char(FILE_LIST(i))]);%vep10s
        setting.NW = 3;
        proc_vep2s(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
        
    elseif regexp(char(FILE_LIST(i)),'.*Supervised.*')
        proc_caseSUP(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
    else
        proc_other(setting,[FILE_PATH '\' char(FILE_LIST(i))]);
    end
end
set(gcf,'visible','on');close;