function proc_vep(s,filepath)
% s=setting;
% filepath=[FILE_PATH '\' char(FILE_LIST(i))];

SEGI = 2;% ignored segment in seconds
SEGD = 10;% data segement in seconds
SEGR = 2.5; % minimal rest segment in seconds: 2.5~3.5

[pathstr,name,ext] = fileparts(filepath);
C = strsplit(name,'-');
name = cell2mat([C(1) '-' C(4)]);

CH_LABEL = s.chlabel;
SRATE = s.srate;
SEG_LEN = s.seg_len;
OVERLAP = s.overlap;
NW = s.NW;

data = csvread(filepath)';
ps = data(12,:);
% get onset index
onset = [];
i = 1;
while( i < length(ps))
    if ps(i) == 1 && i+7 <= length(ps) && ~any(~ps(i:i+7))
        onset(end+1) = i;
        i = i+(SEGI+SEGD+SEGR)*SRATE;
    else
        i = i+1;
    end
end


eeg = data(2:9,:);
% remove baseline
if s.RMBASE_flag eeg = rmbaseline(s,eeg);end
% bandpass
if s.BP_flag eeg = bandpass(s,eeg);end
% ASR algorithm
if s.ASR_flag eeg = asr_algo(s,eeg);end

for i = 1:length(onset)
    epoch(:,:,i) = eeg(:,onset(i)+SEGI*SRATE:onset(i)+(SEGI+SEGD)*SRATE-1);   
end
[CH,PNT,TRIAL] = size(epoch);
% CCA
cca_cor(s,filepath,epoch);
cca_psd_of_all_ch(s,filepath,epoch);
cca_waveform(s,filepath,epoch);


% plot PSD
for j = 1:CH
    pxx = [];f = [];
    for k = 1:TRIAL
        seg = epoch(j,:,k);
        [pxx(end+1,:),f] = pmtm(seg,NW,length(seg),SRATE);
    end
    pxxdb = 10*log10(pxx);
    Q1 = prctile(pxxdb,25);
    Q2 = median(pxxdb,1);
    Q3 = prctile(pxxdb,75);
    
    figure;hold on;set(gcf,'visible','off');
    fill([f' fliplr(f')],[Q3 fliplr(Q1)],[0.702 0.855 1.0],'LineStyle','none', 'FaceAlpha', 0.5)
    plot(f,Q2,'k')
    axis([0 50 -40 60]);grid on;
    title([ name char(10) char(CH_LABEL(j)) ' \ ' num2str(SEG_LEN) 'sec segments \ Multitaper PSD'],'fontsize',12,'interpreter','none');
    xlabel('Frequency (Hz)','fontsize',12);
    ylabel('Power/Frequency (dB/Hz)','fontsize',12);
    
    SAVE_PATH = [pathstr '\Figures\PSD(multitaper, ' num2str(s.seg_len) 's seg)\' name];
    if ~exist(SAVE_PATH,'dir') mkdir(SAVE_PATH); end;
    print('-dtiff','-r200',[SAVE_PATH '\' char(CH_LABEL(j)) '.tiff'])
    saveas(gcf,[SAVE_PATH '\' char(CH_LABEL(j)) '.fig'])
    close;
    
end