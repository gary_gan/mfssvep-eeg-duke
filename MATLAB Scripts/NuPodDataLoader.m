classdef NuPodDataLoader < handle
    properties (Access = private)
        srate = 500
        decoder
        loaderpath
    end
    
    methods
        %% constructor
        function obj = NuPodDataLoader()
            % check matlab dynamic class path
            jpath = javaclasspath('-dynamic');
            [obj.loaderpath,~,~] = fileparts(which('NuPodDataLoader'));
            if isempty(obj.loaderpath); error('Cannot find ''NuPodDataLoader'''); end;
            % Java protobuf decorder jar file
            lib = {'matlabngfiledecoder_ch8-16.jar','protobuf-java-2.6.1.jar'};
            lib_path = strcat(fullfile(obj.loaderpath,'_lib','Protobuf Java decorder'),filesep,lib);
            % add decorder library to path
            if sum(ismember(jpath, lib_path)) ~= length(lib)
                for n = lib_path
                    javaaddpath([n{1}]);end;
            end
            % Java object
            obj.decoder = javaObject('com.cerebratek.matlab.data.NgDataFileDecoder');
            % add eeglab function & required library to path
            [eeglabpath, ~,~] = fileparts(which('eeglab'));
            if isempty(eeglabpath); error('EEGLAB not found!'); end
            
            try
                addpath(genpath(fullfile(obj.loaderpath,'_lib')));
                addpath(genpath(fullfile(eeglabpath,'functions')));
                addpath(genpath(fullfile(eeglabpath,'plugins')));
            catch
            end
        end
        %% protobuf to matrix (mat)
        function result = toMatFile(obj, InputPath, OutputPath)
            % check input file
            if ~obj.valid_pb_file(InputPath)
                fprintf('Input file not found!\n');
                return;
            end
            % manage output path
            [ipath,iname,~] = fileparts(InputPath);
            oext = '.mat';
            switch nargin
                case 3
                    [opath,oname,~] = fileparts(OutputPath);
                    if isempty(oname), oname = iname; end
                    OutputPath = fullfile(opath,[oname oext]);
                case 2
                    OutputPath = fullfile(ipath,[iname oext]);
                otherwise
                    error('Not enough input arguments!');
            end
            fprintf('Converting to .mat file ...\n');
            % decode protobuf data
            statusMessage = obj.decoder.generateMatFile(InputPath, OutputPath);
            fprintf('%s\n', char(statusMessage));
            if strcmpi(statusMessage, 'DECODE_SUCCESS')
                result = true;
            else
                result = false;
                return;
            end
            fprintf('Finished in converting to .mat file: %s\n', OutputPath);
        end
        %% protobuf to comma-separated values (csv)
        function result = toCsvFile(obj, InputPath, OutputPath)
            % check input file
            if ~obj.valid_pb_file(InputPath)
                fprintf('Input file not found!\n');
                return;
            end
            % manage output path
            [ipath,iname,~] = fileparts(InputPath);
            oext = '.csv';
            switch nargin
                case 3
                    [opath,oname,~] = fileparts(OutputPath);
                    if isempty(oname), oname = iname; end
                    OutputPath = fullfile(opath,[oname oext]);
                case 2
                    OutputPath = fullfile(ipath,[iname oext]);
                otherwise
                    error('Not enough input arguments!');
            end
            fprintf('Converting to .csv file ...\n');
            % write protobuf data to tempfile.mat
            tempfilepath = fullfile(obj.loaderpath,'tempfile.mat');
            if ~obj.toMatFile(InputPath, tempfilepath)
                error('Failure in converting to .mat file!');
            end
            % load tempfile.mat
            load(tempfilepath); % variable:eeg
            data = [];
            field = fieldnames(eeg);
            for i = 1:length(field)
                eval(['[pnt, ch] = size(eeg.' field{i} ');']);
                eval(['data(:,end+1:end+ch) = eeg.' field{i} ';']);
            end
            try
                % write to csv file
                dlmwrite(OutputPath, data, 'precision', 16);
%                 csvwrite(OutputPath, data); % precision lost!!!
                
                % delete tempfile.mat
                try delete(tempfilepath);catch, end
                result = true;
                fprintf('Finished in converting to .csv file: %s\n', OutputPath);
            catch
                result = false;
                error('Failure in creating .csv file: %s', OutputPath);
            end
        end
        %% load protobuf data into eeglab
        function EEG = loadToEEGLAB(obj, InputPath)
            EEG = [];
            % check input file
            if ~obj.valid_pb_file(InputPath)
                fprintf('Cannot find input file!\n');
                return;
            end
            fprintf('Loading NuPod file into EEGLAB ...\n');
%             global EEG;		% current dataset
            global ALLEEG;		% all datasets
            global CURRENTSET;	% current set index
%             global ALLCOM;		% all commands (history)
%             global STUDY;
%             global CURRENTSTUDY;
%             global PLUGINLIST;

            % load EEGLAB variables structure if exists in base workspace
            if obj.fnBaseExist('ALLEEG'), ALLEEG = evalin('base','ALLEEG');
            else ALLEEG = [];end
%             if obj.fnBaseExist('ALLCOM'), ALLCOM = evalin('base','ALLCOM');
%             else ALLCOM = [];end
%             if obj.fnBaseExist('CURRENTSTUDY'), CURRENTSTUDY = evalin('base','CURRENTSTUDY');
%             else CURRENTSTUDY = [];end
%             if obj.fnBaseExist('PLUGINLIST'), PLUGINLIST = evalin('base','PLUGINLIST');
%             else PLUGINLIST = [];end
%             if obj.fnBaseExist('STUDY'), STUDY = evalin('base','STUDY');
%             else STUDY = [];end

            % protobuf to EEG structure
            EEG = obj.toEEGStruct(InputPath);
            [ALLEEG, EEG, CURRENTSET] = eeg_store( ALLEEG, EEG, 0 );
            
            % write back EEGLAB variables structure to USER workspace
            assignin('base','EEG',EEG);
            assignin('base','ALLEEG',ALLEEG);
            assignin('base','CURRENTSET',CURRENTSET);
%             assignin('base','STUDY',STUDY);
%             assignin('base','CURRENTSTUDY',CURRENTSTUDY);
%             assignin('base','PLUGINLIST',PLUGINLIST);
%             assignin('base','ALLCOM',ALLCOM);
            eeglab redraw;

            fprintf('Finished in loading NuPod file into EEGLAB!\n');
        end
        
        function EEG = toEEGStruct(obj, pbPath)
            if ~exist(pbPath, 'file'), error('Cant find protobuf file!'); end
            [filepath, filename, ext] = fileparts(pbPath);
            if ~strcmpi(ext, '.pb'), error('Not a protobuf file!'); end
            
            OutputPath = fullfile(filepath, 'temp.mat');
            isMatDone = obj.toMatFile(pbPath, OutputPath);
            if isMatDone
                Mat = load(OutputPath);
                fields = fieldnames(Mat.eeg);
                channel_status = [];
                
                % TODO: read sampling rate from meta data file
                meta_path = fullfile(filepath, [filename '.ngmeta']);
                disp(['Reading .ngmeta file... ' meta_path]);
                
                if exist(meta_path, 'file')
                    try
                        ngmeta_json_body = loadjson(meta_path);% json format
                        nupod_srate = ngmeta_json_body.record.sampling_rate;
                        disp(['Recording sampling rate: ' num2str(nupod_srate)]);
                    catch 
                        warning('Format error in %s!', meta_path);
                        warning('Using default sampling rate: %d', obj.srate);
                        nupod_srate = obj.srate;
                    end
                else
                    warning('Cannot find %s !', meta_path);
                end
                
           
                
                for fidx = 1:length(fields)
                    eval(['[pnt, ch] = size(Mat.eeg.' fields{fidx} ');']);
                    eval(['tempdata = double(Mat.eeg.' fields{fidx} ')'';']);
                    if strcmpi(fields(fidx), 'channel_data')
                        EEG = pop_importdata('dataformat','array','nbchan',ch,'data',tempdata,'srate',nupod_srate,'pnts',pnt,'xmin',0);
                        EEG = eeg_checkset( EEG );
                    elseif regexp(fields{fidx}, 'lead_off_detection')
                        channel_status(end+1:end+ch, 1:pnt) = tempdata;
                    elseif strcmpi(fields(fidx), 'ntp_timestamp')
                        EEG.timestamp = tempdata;
                    elseif strcmpi(fields(fidx), 'photosensor_state')
                        EEG.frame_transition = tempdata;
                    else
                        eval(['EEG.' fields{fidx} ' = tempdata;']);
                    end
                end
                EEG.channel_status = channel_status;
 
                % loading meta data
                meta_path = fullfile(filepath, [filename '.ngmeta']);
                if exist(meta_path, 'file')
                    try
                        EEG.meta = loadjson(meta_path);% json format
                        try
                            EEG.subject = EEG.meta.subject.x0x5F_id; % name
                        catch
                            warning('Cannot find subject.id metadata;');
                        end
                        EEG.setname = ['NuPod raw data (' EEG.meta.activity.type ', ' EEG.meta.session.title ', ' EEG.meta.subject.x0x5F_id ')'];
                        EEG.filename = [filename ext];
                        EEG.filepath = filepath;
                        
                        %% channel location
                        % filling in NuPod electrodes parameters from BESA standard
                        Standard_elecs = readlocs(fullfile(obj.loaderpath,'\_lib\Sculp channel infomation\standard-10-5-cap385.elp'),'filetype','besa');
                        NuPod_elecs = EEG.meta.record.record_channel_spec.channel_num_used;
                        NuPod_elecs_index = [];
                        for idx = 1:length(NuPod_elecs)
                            NuPod_elecs_index(end+1) = find(ismember(lower({Standard_elecs.labels}),lower(NuPod_elecs(idx))));
                        end
                        % The number of NuPod data channels must equal to the number of used channels recorded in metedata and the channels,
                        % and as well as the number of NuPod-matched channels in standard sculp channels
                        if length(NuPod_elecs) == EEG.nbchan && length(NuPod_elecs_index) == EEG.nbchan
                            fields = {'labels','type','theta','radius','X','Y','Z','sph_theta','sph_phi','sph_radius','urchan','ref'};
                            for i = 1:length(NuPod_elecs_index)
                                for j = 1:length(fields)
                                    try
                                        if strcmpi(fields{j},'urchan') % origin channel index
                                            eval(['EEG.chanlocs(i).' fields{j} ' = ' num2str(i) ';']);
                                        else
                                            eval(['EEG.chanlocs(i).' fields{j} ' = Standard_elecs(' num2str(NuPod_elecs_index(i)) ').' fields{j} ';']);
                                        end
                                    catch
                                        eval(['EEG.chanlocs(i).' fields{j} ' = [];'])
                                    end
                                end
                            end
                        end
                        % comment
                        EEG.comments = pop_comments('','',['Original file: ' pbPath],0);
                    catch
                        warning('Format error in %s!', meta_path);
                    end
                else
                    warning('Cannot find %s !', meta_path);
                end
                % delete temp.mat
                try
                    delete(OutputPath);
                catch
                    % error('Failure in deleting temp.mat!');
                end
            end 
        end
        
        % add XDF files 
        function ALLEEG = allXDFToEEGStruct(obj, inputDir)
            allxdf = rdir([inputDir '\**\*.xdf']);
            allxdf = {allxdf.name}';
            disp([num2str(length(allxdf)) ' files found: ']);
            disp(allxdf);
            if obj.fnBaseExist('ALLEEG'), ALLEEG = evalin('base','ALLEEG');
            else ALLEEG = [];end  
            
            for idx = 1:length(allxdf)
                EEG = pop_loadxdf( allxdf{idx} );
                [ALLEEG, ~, ~] = eeg_store(ALLEEG, EEG, 0);
            end
        end            
        
        function ALLEEG = allToEEGStruct(obj, inputDir)
            % Recursively find out all protobuf files under inputDir, and convert
            % these files to ALLEEG (EEGLAB structure)
            allpb = rdir([inputDir '\**\*.pb']);
            allpb = {allpb.name}';
            disp([num2str(length(allpb)) ' files found: ']);
            disp(allpb);
            if obj.fnBaseExist('ALLEEG'), ALLEEG = evalin('base','ALLEEG');
            else ALLEEG = [];end
            
            for idx = 1:length(allpb)
                EEG = obj.toEEGStruct(allpb{idx});
                [ALLEEG, ~, ~] = eeg_store(ALLEEG, EEG, 0);
            end
        end
    end

    methods (Access = private)
        % check if input file exists
        function result = valid_pb_file(obj, filepath)
            [~, ~, ext] = fileparts(filepath);
            if exist(filepath, 'file') && strcmpi(ext, '.pb')
                result = true;
            else
                result = false;
            end
        end
        % check if variable exists in base workspace
        function InBase = fnBaseExist(obj, var)
            W = evalin('base', 'who');
            InBase=0;
            for ii = 1:length(W)
                nm1 = W{ii};
                InBase = strcmp(nm1, var)+InBase;
            end
            InBase(InBase>0) = 1;
        end
        
    end
end