function eeg_processing(dirpath, tag)
FS = 500;
FREQUENCY_LIST = [8:0.2:11.8];
LENGTH = 2568;
CHANNEL = 6;
cd(dirpath);
file_list = dir('*.csv');
data = [];
for i = 1 : length(file_list)
    filename = file_list(i).name;
    new_data = zeros(LENGTH, CHANNEL);
    temp = bp_w(csvread(filename), FS);
    new_data(1:size(temp,1), 1:size(temp,2)) = temp;
    data = cat(3, data, zscore(new_data));
end
result = zeros(size(data, 3), size(data, 1), length(FREQUENCY_LIST));
epoch_betas = zeros(size(data, 3), length(FREQUENCY_LIST), 6);
correlation = zeros(size(data, 3), length(FREQUENCY_LIST));
SNR = zeros(size(data, 3), length(FREQUENCY_LIST));
for i = 1 : size(data, 3)
    betas = [];
    rel = [];
    SNR_ = [];
    new_result = zeros(size(data, 1), length(FREQUENCY_LIST));
    for j = 1 : length(FREQUENCY_LIST)
        f = FREQUENCY_LIST(j);
        [~, B, r, ~, ~, awx, ~] = CCA_normal(data(:, :, i), f);
        betas = cat(1, betas, B(:, 1)');
        rel = cat(1, rel, r(1));
        kkk = get_snr(awx(:, 1), f, FS);
        SNR_ = cat(1, SNR_, kkk);
        new_result(:, j) = awx(:, 1);  
    end
    result(i, :, :) = bp(new_result, FS);
    epoch_betas(i, :, :) = betas;
    correlation(i, :) = rel;
    SNR(i, :) = SNR_;
end
cd(strcat('C:\Users\User\Desktop\cca_out_', tag));
parts = strsplit(dirpath, '\');
output_name = parts{end};
save(output_name, 'result', 'epoch_betas', 'correlation', 'SNR');
end