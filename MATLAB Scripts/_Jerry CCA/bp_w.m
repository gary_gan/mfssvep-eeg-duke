function y = bp_w(x, fs)
fs = fs/2; 
Wp = [5/fs 40/fs];
Ws = [2/fs 50/fs];
Wn = 0;
[N,Wn] = cheb1ord(Wp, Ws, 3, 40);
[B,A] = cheby1(N, 0.5, Wn);
y = filtfilt(B, A, x);
end
