function batch_eeg_processing(dirpath, tag)
cd(dirpath);
file_list = dir('sbj*');
for i = 1 : length(file_list)
    try
        eeg_processing([file_list(i).folder, '\', file_list(i).name], tag);
    catch exception
        disp(file_list(i).name)
        disp(getReport(exception))
    end
end
end