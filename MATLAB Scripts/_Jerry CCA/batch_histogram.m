function batch_histogram()
mkdir('fig')
mkdir('csv')
file_list = dir('*.csv');
data = [];
edges = [0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.60, 0.65, 0.70, 0.75, 0.8, 0.85, 0.9, 0.95, 1];
for i = 1 : length(file_list)
    new_data = csvread(file_list(i).name);
    data = cat(1, data, new_data);
    movefile(file_list(i).name, 'csv');
    t = histogram(new_data, edges, 'Normalization', 'probability');
    xlim([0, 1]);
    ylim([0, 1]);
    savefig(['fig\', file_list(i).name, '.fig']);
    saveas(t, [file_list(i).name, '.png'])
    close;
end
t = histogram(data, edges, 'Normalization', 'probability');
xlim([0, 1]);
ylim([0, 1]);
savefig('fig\all.fig');
saveas(t, 'all.png')
close;
end