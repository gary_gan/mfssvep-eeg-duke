% % %% 
function [A, B, r, U, V, awx, bwx] = CCA_normal(x, frequency)
X = x; 
Fs = 500;  % Sampling frequency
T = 1/Fs;  % Sample time
L = max(size(x));  % Length of signal
t = (0:L-1)*T;  % Time vector

y1 = sin(2*pi*frequency*t);
y2 = cos(2*pi*frequency*t);
y3 = sin(4*pi*frequency*t);
y4 = cos(4*pi*frequency*t);
y5 = sin(6*pi*frequency*t);
y6 = cos(6*pi*frequency*t);
% 4th harmonic
% y7 = sin(8*pi*frequency*t);
% y8 = sin(8*pi*frequency*t);

% Y = [y1; y2; y3; y4; y5; y6; y7; y8;]';
Y = [y1; y2; y3; y4; y5; y6;]';

% size(X)
% size(Y)
[A, B, r, U, V] = canoncorr(X, Y);
awx = X*A;
bwx = Y*B;
end