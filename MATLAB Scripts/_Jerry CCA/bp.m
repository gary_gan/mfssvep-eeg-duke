function y = bp(x, fs)
fs = fs/2; 
Wp = [5/fs 15/fs];
Ws = [2/fs 25/fs];
Wn = 0;
[N,Wn] = cheb1ord(Wp, Ws, 3, 40);
[B,A] = cheby1(N, 0.5, Wn);
y = filtfilt(B, A, x);
end
